#include "TObjectStreamer.h"

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

bool TObjectStreamer::bReady = false;
bool TObjectStreamer::IsReady()
{
    return this->IsValid() && TObjectStreamer::bReady;
}

Long_t TObjectStreamer::Ticks()
{
    return this->ticks;
}

Long_t TObjectStreamer::LastUpdate()
{
    this->ticks++;
    return (Long_t) 1000*(TTimeStamp().AsDouble() - this->lastUpdate);
}

int TObjectStreamer::Initialize(const char *token)
{
    if (!TServerSocket::IsValid()) {
        TPrint::Error(__METHOD_NAME__, "Failed to start socket application");
        return FAILURE;
    }

    this->fSockets = new TList;
    this->fObjects = new TList;

    this->fMonitor  = new TMonitor;
    this->fMonitor->Add(this);

    TPrint::Message(__METHOD_NAME__, "Listening for connections on %s:%d starting from %s", TServerSocket::GetLocalInetAddress().GetHostName(), TServerSocket::GetLocalPort(), gClock->Local().Data());
    
    this->token = token;
    if(!TString(this->token).EqualTo(""))
        TPrint::Message(__METHOD_NAME__, "Expected token `%s` for new connections", this->token);
    
    TObjectStreamer::bReady = true;
    return SUCCESS;
}

void TObjectStreamer::ErrorHandler(int level, Bool_t abort, const char *location, const char *msg)
{
   if (level == ::kSysError) throw std::runtime_error(msg);
   DefaultErrorHandler(level, abort, location, msg);
}


void TObjectStreamer::DisableSignalHandler() {

    TPrint::Debug(10, __METHOD_NAME__, "Disable Ctrl+C catcher..");
    signal(SIGINT, SIG_DFL);
}

void TObjectStreamer::EnableSignalHandler(){

	TPrint::Debug(10, __METHOD_NAME__, "Enable Ctrl+C catcher..");
	struct sigaction sigIntHandler;
	sigIntHandler.sa_handler = TObjectStreamer::HandlerCTRL_C;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;
	sigaction(SIGINT, &sigIntHandler, NULL);
}

void TObjectStreamer::HandlerCTRL_C(int s){

	std::cerr << std::endl;
	std::cerr << "** Caught signal " << s << std::endl;
	std::cerr << "** You stopped processing pressing CTRL-C.."<< std::endl;
	std::cerr << "** Now, going to finish processing last file." << std::endl;
	std::cerr << "** Please press CTRL-C again, if you want to exit." << std::endl;

	TObjectStreamer::DisableSignalHandler();
	TObjectStreamer::bReady = false;
}


int TObjectStreamer::Open(TPServerSocket *socket)
{
    if(nClients >= maxClients && maxClients > -1) {

        TPrint::Warning(__METHOD_NAME__, "Too many connections.. (max connections: %d) at %s", this->GetInetAddress().GetHostName(), maxClients, gClock->Local().Data());
        return TObjectStreamer::FAILURE;
    }

    TSocket *server = socket->Accept();
    fMonitor->Add(server);

    if(!TString(token).EqualTo("")) TPrint::Message(__METHOD_NAME__, "New connection received, waiting for token from `%s` at %s", server->GetInetAddress().GetHostName(), gClock->Local().Data());
    nClients++;

    return TObjectStreamer::SUCCESS;
}

int TObjectStreamer::Close(TSocket *socket)
{
    if(!fSockets->FindObject(socket)) 
        return TObjectStreamer::FAILURE;

    fMonitor->Remove(socket);
    fSockets->Remove(socket);        

    TPrint::Message(__METHOD_NAME__, "Closed connection from `%s` at %s", socket->GetInetAddress().GetHostName(), gClock->Local().Data());
    delete socket;

    nClients--;

    return TObjectStreamer::SUCCESS;
}

int TObjectStreamer::SocketHandler()
{
    TSocket *socket = NULL;

    if ( (socket = fMonitor->Select(this->timeout)) == (TSocket*) -1 ) {
        return TObjectStreamer::PASSIVE;
    }

    if (socket->InheritsFrom("TServerSocket"))
        return this->Open((TPServerSocket *) socket);
    
    int ret;
    char name[256];   
    try {

        auto errorHandler = SetErrorHandler(TObjectStreamer::ErrorHandler);
        ret = socket->Recv(name, sizeof(name));
        SetErrorHandler(errorHandler);

    } catch(const std::runtime_error& e) { ret = 0; } 

    //
    // Check if connection is closed
    if(ret <= 0) return this->Close(socket);

    //
    // Check auth
    if(fSockets->FindObject(socket) == NULL) {
    
        TMessage auth;
        if(TString(name).EqualTo(this->token) || TString(this->token).EqualTo("")) {

            TPrint::Message(__METHOD_NAME__, "Accepted connection from `%s` on %s", socket->GetInetAddress().GetHostName(), gClock->Local().Data());
            auth.WriteBool(true);
            socket->Send(auth);

            fSockets->Add(socket);
            return TObjectStreamer::SUCCESS;
        }
        
        TPrint::Warning(__METHOD_NAME__, "Wrong credentials provided from `%s` on %s", socket->GetInetAddress().GetHostName(), gClock->Local().Data());
        auth.WriteBool(false);
        socket->Send(auth);

        return TObjectStreamer::FAILURE;
    }

    //
    // Send requested object back
    TIter Next(this->fObjects);

    TObject *fObject = this->FindObject(name);
    if( fObject != NULL ) {
    
        TMessage message(kMESS_OBJECT);
                 message.WriteObject(fObject);
        
        socket->Send(message);
        return  TObjectStreamer::SUCCESS;
    } 
    
    TPrint::Error(__METHOD_NAME__, "An unregistered object `%s` has been requested.", name);
    return TObjectStreamer::FAILURE;
}

TObject *TObjectStreamer::FindObject(TObject *obj)
{
    if(obj == NULL) return NULL;
    
    TObject *fObject = this->fObjects->FindObject(obj);
    if(fObject) return fObject;

    return this->FindObject(obj->GetName());
}

TObject *TObjectStreamer::FindObject(TString objName)
{
    if(objName.EqualTo("")) return NULL;

    TObject *fObject = this->fObjects->FindObject(objName);
    if(fObject) return fObject;

    TString objDir = TFileReader::DirName(objName);
    std::vector<TString> array = ROOT::IOPlus::Helpers::Explode("/", objName);
    objName = array[array.size()-1];

    TIter Next(this->fObjects);
    while( (fObject = Next()) ) {
    
        if(fObject == NULL) continue;
        if(fObject->InheritsFrom("TDirectory")) {

            TDirectory *dir = (TDirectory *) fObject;
            if(dir == NULL) continue;

            if(!objDir.EqualTo("")) dir = dir->GetDirectory(objDir);
            if(dir == NULL) continue;

            fObject = dir->FindObject(objName);
            if(fObject) return fObject;

        } else if(TString(fObject->GetName()).EqualTo(objName)) {

            return fObject;
        }
    }

    return NULL;
}

void TObjectStreamer::Print(Option_t *opt)
{
    TServerSocket::Print(opt);
    this->fObjects->Print();
}

void TObjectStreamer::Add(TObject *fObject)
{
    if(fObject == NULL) return;
    
    if(fObject->InheritsFrom("TDirectory")) TPrint::Debug(1, __METHOD_NAME__, "Object in `%s` directory are now broadcasted.", fObject->GetName());
    else TPrint::Debug(1, __METHOD_NAME__, "Object `%s` is now broadcasted.", fObject->GetName());

    this->fObjects->Add(fObject);
}

void TObjectStreamer::Remove(TObject *fObject)
{
    if(this->fObjects->FindObject(fObject)) {

        TPrint::Debug(1, __METHOD_NAME__, "Object `%s` is not broadcasted anymore.", fObject->GetName());
        this->fObjects->Remove(fObject);
    }
}

int TObjectStreamer::Listen()
{
    this->lastUpdate = TTimeStamp().AsDouble();
    this->ticks = 0;

    return SocketHandler();
}

void TObjectStreamer::DeleteIfFound(TObject *object) 
{
    if(object == NULL) return;

    object = this->FindObject(object->GetName());
    if(object == NULL) return;

    this->Remove(object);

    object->Delete();
    object = NULL;
}

void TObjectStreamer::DeleteIfFound(TString objName)
{
    TObject *object = this->FindObject(objName);    
    if(object == NULL) return;

    this->Remove(object);

    object->Delete();
    object = NULL;
}

ClassImp(TObjectStreamer)
