
//
// @TODO: Implement advanced plotting as needed.

#include "TPicasso.h"
ClassImp(TPicasso)

#include <TGClient.h>
TPicasso *gPicasso = new TPicasso();

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

int TPicasso::GetDisplayWidth() const
{
        return gClient->GetDisplayWidth();
}

int TPicasso::GetDisplayHeight() const
{
        return gClient->GetDisplayHeight();
}

TCanvas *TPicasso::MakeCornerPlot(int Nx, int Ny)
{
        return NULL;
}

TCanvas *TPicasso::DrawCornerPlot(TTree *t, const char *varexp, const char *selection, Option_t *option, Long64_t nentries, Long64_t firstentry)
{
        int Nx = 1;
        int Ny = 1;

        TCanvas *c = MakeCornerPlot(Nx, Ny);
        return NULL;
}

TCanvas *TPicasso::MakeMatrixPlot(int Nx, int Ny)
{
        return NULL;
}

TCanvas *TPicasso::DrawMatrixPlot(const std::vector<std::vector<TH1*>> &matrix)
{
        int Nx = 1;
        int Ny = 1;

        TCanvas *c = MakeMatrixPlot(Nx, Ny);
        return NULL;
}

TCanvas *TPicasso::DrawMatrixPlot(TTree *t, const char *varexp, const char *selection, Option_t *option, Long64_t nentries, Long64_t firstentry)
{
        TCanvas *c = NULL;
        return NULL;
}

TCanvas *TPicasso::MakeRatioPlot()
{
        return NULL;
}

TCanvas *TPicasso::DrawRatioPlot(TList *l)
{
        TCanvas *c = MakeRatioPlot();
        return NULL;
}

TCanvas *TPicasso::MakeSkymap()
{
        return NULL;
}
TCanvas *TPicasso::DrawSkymap(TH2D *map, TList *contourList)
{
        TCanvas *c = MakeSkymap();
        return NULL;
}

TCanvas *TPicasso::MakeJointPlot()
{
        return NULL;
}

TCanvas *TPicasso::DrawJointPlot(TList *l, Option_t *option)
{
        return NULL;
}

TCanvas *TPicasso::MergeCanvas(TList *l) {

        TCanvas *c = NULL;

        TObject *obj = NULL;
        TIter Next(l);
        while ( (obj = Next()) ) {

                if (!obj->InheritsFrom("TCanvas")) continue;

                if (c == NULL) c = (TCanvas *) obj->Clone();
                else {

                        TList *_l = this->GetListOfObjects((TCanvas*) obj);
                        TList *primitives = c->GetListOfPrimitives();
                               primitives->Add(_l);
                        primitives->Print();
                        
                        _l->Clear("nodelete");
                        _l->Delete();
                }
        }

        return c;
}

inline TCanvas *TPicasso::MergeCanvas(TCanvas *c0, TCanvas *c1) {

        TList *l = new TList();
                l->Add(c0);
                l->Add(c1);
        
        TCanvas *c = MergeCanvas(l);

        l->Clear("nodelete");
        l->Delete();

        return c;
}

TList *TPicasso::GetListOfObjects(TCanvas *canvas)
{
        TList *l = new TList();

        TObject *obj = NULL;
        TIter Next(canvas->GetListOfPrimitives());
        while ( (obj = Next()) )
        {
                if ( TString(obj->GetName()).EqualTo("TFrame"))
                if ( TString(obj->GetName()).EqualTo("PaveText")) continue;
                l->Add(obj);
        }

        return l;
}

TString TPicasso::GetTitle(TCanvas *canvas)
{
        TObject *obj = GetListOfObjects(canvas)->First();
        return obj == NULL ? "" : obj->GetTitle();
}

bool TPicasso::Write(TString output, TCanvas* canvas)
{
        std::vector<TCanvas *> canvases({canvas});
        return this->Write(output, canvases);
}

bool TPicasso::Write(TString output, std::vector<TCanvas *> &canvases)
{
        TString _output = output;
        _output.ToLower();

        if (_output.EndsWith(".pdf"))
        {
                // Generate a PDF containing all canvases
                TPDF *pdf = new TPDF(output);
                pdf->On();

                for (int i = 0; i < canvases.size(); i++)
                {
                pdf->NewPage();
                pdf->SetTitle(this->GetTitle(canvases[i]));
                canvases[i]->Paint();
                }

                gROOT->GetListOfSpecials()->Remove(gVirtualPS);
                delete gVirtualPS;
                gVirtualPS = nullptr;

                return true;
        }
        else
        {
                // Write canvases individually with indexed filenames
                for (size_t i = 0; i < canvases.size(); i++)
                {
                TString indexedOutput = output;

                // Extract base and extension
                Ssiz_t dotIndex = indexedOutput.Last('.');
                TString baseName = (dotIndex != kNPOS) ? indexedOutput(0, dotIndex) : indexedOutput;
                TString extension = (dotIndex != kNPOS) 
                                ? indexedOutput(dotIndex + 1, indexedOutput.Length() - dotIndex - 1) // +1 to skip the dot
                                : TString(""); // Return an empty TString if no dot found
                                
                // Append index to the base name
                indexedOutput = Form("%s-%zu%s", baseName.Data(), i + 1, extension.Data());

                canvases[i]->Write(indexedOutput); // Save each canvas with a unique indexed name
                }

                return true;
        }
}