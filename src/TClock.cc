/**
 **********************************************
 *
 * \file TClock.cc
 * \brief Source code of the Handbox class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include "TClock.h"
ClassImp(TClockAbstract)
ClassImp(TClock<Clock::Unix>)
ClassImp(TClock<Clock::TAI>)
ClassImp(TClock<Clock::GPS>)

#include <vector>
#include <regex>

#include <date/date.h>
#include <date/tz.h>

/** @cond */
TClock<Clock::Unix> *gClock     = new TClock();
TClock<Clock::Unix> *gClockUnix = new TClock<Clock::Unix>();
TClock<Clock::GPS>  *gClockGPS  = new TClock<Clock::GPS>();
TClock<Clock::TAI>  *gClockTAI  = new TClock<Clock::TAI>();

template <Clock T>
const int TClock<T>::unixTimeGPS = 315'964'800;

template <Clock T>
const int TClock<T>::gpsOffsetTAI = 19;
/** @endcond */

template <Clock T>
const std::vector<int> TClock<T>::leapSeconds = { // Expressed GPS Time

        46'828'800, // 1981-06-30 23:59:60 UTC
        78'364'801, // 1982-06-30 23:59:60 UTC
       109'900'802, // 1983-06-30 23:59:60 UTC
       173'059'203, // 1985-06-30 23:59:60 UTC
       252'028'804, // 1987-12-31 23:59:60 UTC
       315'187'205, // 1989-12-31 23:59:60 UTC
       346'723'206, // 1990-12-31 23:59:60 UTC
       393'984'007, // 1992-06-30 23:59:60 UTC
       425'520'008, // 1993-06-30 23:59:60 UTC
       457'056'009, // 1994-06-30 23:59:60 UTC
       504'489'610, // 1995-12-31 23:59:60 UTC
       551'750'411, // 1997-06-30 23:59:60 UTC
       599'184'012, // 1998-12-31 23:59:60 UTC
       820'108'813, // 2005-12-31 23:59:60 UTC
       914'803'214, // 2008-12-31 23:59:60 UTC
     1'025'136'015, // 2012-06-30 23:59:60 UTC
     1'119'744'016, // 2015-06-30 23:59:60 UTC
     1'167'264'017  // 2016-12-31 23:59:60 UTC
                    // ...
};

template <Clock T>
const std::map<TString, TString> TClock<T>::timezoneOffsets = {
    {"IDLW", "-12:00"},  // International Date Line West
    {"NT",   "-11:00"},    // Nome Time
    {"AHST", "-10:00"},  // Alaska-Hawaii Standard Time
    {"YST",  "-09:00"},   // Yukon Standard Time
    {"PST",  "-08:00"},   // Pacific Standard Time
    {"PDT",  "-07:00"},   // Pacific Daylight Time
    {"MST",  "-07:00"},   // Mountain Standard Time
    {"CST",  "-06:00"},   // Central Standard Time
    {"CDT",  "-05:00"},   // Central Daylight Time
    {"EST",  "-05:00"},   // Eastern Standard Time
    {"ESDT", "-04:00"},   // Eastern Standard Time
    {"NST",  "-03:30"},   // Newfoundland Standard Time
    {"FNT",  "-02:00"},   // Fernando de Noronha Time
    {"GMT",  "+00:00"},   // Greenwich Mean Time
    {"UTC",  "+00:00"},   // Greenwich Mean Time
    {"CET",  "+01:00"},   // Central European Time
    {"CEST", "+02:00"},   // Central European Summer Time
    {"EET",  "+02:00"},   // Eastern European Time
    {"MSK",  "+03:00"},   // Moscow Standard Time
    {"IRST", "+03:30"},  // Iran Standard Time
    {"PKT",  "+05:00"},   // Pakistan Standard Time
    {"IST",  "+05:30"},   // Indian Standard Time
    {"NPT",  "+05:45"},   // Nepal Time
    {"BST",  "+06:00"},   // Bangladesh Standard Time
    {"MMT",  "+06:30"},   // Myanmar Time
    {"ICT",  "+07:00"},   // Indochina Time
    {"WIB",  "+07:00"},   // Western Indonesian Time
    {"AWST", "+08:00"},   // Australian Western Standard Time
    {"JST",  "+09:00"},   // Japan Standard Time
    {"ACST", "+09:30"},  // Australian Central Standard Time
    {"AEST", "+10:00"},  // Australian Eastern Standard Time
    {"NCT",  "+11:00"},   // New Caledonia Time
    {"NZST", "+12:00"},  // New Zealand Standard Time
                         // Add more timezone offsets as needed

    {"GMT-12", "-12:00"},
    {"GMT-11", "-11:00"},
    {"GMT-10", "-10:00"},
    {"GMT-9",  "-9:00"},
    {"GMT-8",  "-08:00"},
    {"GMT-7",  "-07:00"},
    {"GMT-6",  "-06:00"},
    {"GMT-5",  "-05:00"},
    {"GMT-4",  "-04:00"},
    {"GMT-3",  "-03:00"},
    {"GMT-2",  "-02:00"},
    {"GMT-1",  "-01:00"},
    {"GMT+0",  "+00:00"},
    {"GMT-0",  "+00:00"},
    {"GMT+1",  "+01:00"},
    {"GMT+2",  "+02:00"},
    {"GMT+3",  "+03:00"},
    {"GMT+4",  "+04:00"},
    {"GMT+5",  "+05:00"},
    {"GMT+6",  "+06:00"},
    {"GMT+7",  "+07:00"},
    {"GMT+8",  "+08:00"},
    {"GMT+9",  "+09:00"},
    {"GMT+10", "+10:00"},
    {"GMT+11", "+11:00"},
    {"GMT+12", "+12:00"},

    {"UTC-12", "-12:00"},
    {"UTC-11", "-11:00"},
    {"UTC-10", "-10:00"},
    {"UTC-9",  "-9:00"},
    {"UTC-8",  "-08:00"},
    {"UTC-7",  "-07:00"},
    {"UTC-6",  "-06:00"},
    {"UTC-5",  "-05:00"},
    {"UTC-4",  "-04:00"},
    {"UTC-3",  "-03:00"},
    {"UTC-2",  "-02:00"},
    {"UTC-1",  "-01:00"},
    {"UTC+0",  "+00:00"},
    {"UTC-0",  "+00:00"},
    {"UTC+1",  "+01:00"},
    {"UTC+2",  "+02:00"},
    {"UTC+3",  "+03:00"},
    {"UTC+4",  "+04:00"},
    {"UTC+5",  "+05:00"},
    {"UTC+6",  "+06:00"},
    {"UTC+7",  "+07:00"},
    {"UTC+8",  "+08:00"},
    {"UTC+9",  "+09:00"},
    {"UTC+10", "+10:00"},
    {"UTC+11", "+11:00"},
    {"UTC+12", "+12:00"}
};

template <Clock T>
TString TClock<T>::GetName()
{
        TString name = gPrint->GetTypename(__PRETTY_FUNCTION__)[0];
                name.ReplaceAll("Clock::", "");

        return name;
}

template <Clock T>
double TClock<T>::Timestamp(const char *datetime, const char *format)
{
        double timestamp = Parse(datetime, format);
        return ROOT::IOPlus::Helpers::IsNaN(timestamp) ? -1 : timestamp;
}

template <Clock T>
int TClock<T>::GMT()
{
        std::time_t time = std::time(nullptr);
        return std::difftime(
                std::mktime(std::localtime(&time)), 
                std::mktime(std::gmtime(&time))
        ) / 3600;
}

template <Clock T>
TString TClock<T>::Format(Double_t timestamp, const Option_t* option, int precision) {
    
    TString opt = option;
    opt.ToLower();
    
    Double_t factor = 1.0;
    
    if (opt.Contains("ns") || opt.Contains("nanosecond")) {
        factor = 1e9;
    } else if (opt.Contains("us") || opt.Contains("microsecond")) {
        factor = 1e6;
    } else if (opt.Contains("ms") || opt.Contains("millisecond")) {
        factor = 1e3;
    } else if (opt.Contains("second") || opt.Contains("sec")) {
        factor = 1.0;
    } else if (opt.Contains("minute") || opt.Contains("min")) {
        factor = 1.0 / 60.0;
    } else if (opt.Contains("hour") || opt.Contains("h")) {
        factor = 1.0 / 3600.0;
    } else if (opt.Contains("day")) {
        factor = 1.0 / 86400.0;
    } else if (opt.Contains("week") || opt.Contains("w")) {
        factor = 1.0 / 604800.0;
    } else if (opt.Contains("year") || opt.Contains("y")) {
        factor = 1.0 / 31536000.0;
    }
    
    return precision > 0 ? Form("%.*f", precision, timestamp * factor) : Form("%f", timestamp * factor);
}

template <Clock T>
inline double TClock<T>::Timestamp()
{
        auto now = std::chrono::system_clock::now();
        auto nanoseconds_since_epoch = std::chrono::duration_cast<std::chrono::nanoseconds>(now.time_since_epoch()).count();
        return gClock->ConvertTo<T>(static_cast<double>(nanoseconds_since_epoch) / 1'000'000'000.0);
}
template <Clock T>
inline TString TClock<T>::GuessUnit(double t0, double t1, int N)
{
        double dt = std::abs(t1 - t0) / N;

        if (dt < 60*24)   return "seconds";
        if (dt < 60*24*7) return "minutes";
        if (dt < 60*24*30.5) return "hours";
        if (dt < 60*24*365) return "days";
        if (dt < 60*24*365*5) return "months";
        return "years";
}

template <Clock T>
void TClock<T>::FormatTimeAxis(const char* hname, const Option_t* option, const char *timezone, int precision, int axis)
{
        TH1* h = (TH1*)gROOT->FindObject(hname);
        if (!h) return;

        TString opt = option;
                opt.ToLower();

        bool formatX = (axis == 0); // Default to X-axis
        TAxis* a = NULL;
        switch (axis) {
                case 0: a = h->GetXaxis();
                break;

                case 1: a = h->GetYaxis();
                break;

                case 2: a = h->GetZaxis();
                break;

                default:
                        std::cerr << "Invalid axis index!" << std::endl;
                        return;
        }

        int first = a->GetFirst();
        double ufirst = a->GetBinLowEdge(first);
        int last = a->GetLast();
        double ulast = a->GetBinUpEdge(last);
           
        Int_t divxo  = 0;
        Double_t bwx = 0.;
        Double_t x1o = 0.;
        Double_t x2o = 0.;
     
        if(a->GetNdivisions() <= 0 ) return;
        
        TString precision_str = precision < 1 ? "%f" : "%."+TString::Itoa(precision,10)+"f";
        THLimitsFinder::Optimize(ufirst, ulast, a->GetNdivisions()%100,x1o,x2o,divxo,bwx,"");
        
        double u0 = a->GetBinLowEdge(1);
        TString u0_str = Form(precision_str, u0);
        if (precision < 1) {
                while (u0_str.Contains(".") && u0_str.EndsWith("0") && !u0_str.EndsWith(".0")) u0_str.Chop();
                if (u0_str.EndsWith(".")) u0_str.ReplaceAll(".", ".0");
        }

        TString unit = opt.EqualTo("") ? GuessUnit(ufirst-u0, ulast-u0, divxo) : opt;
        for (int i = 0; i <= divxo; i++) {

                double bw = (x2o - x1o) / divxo;
                a->SetMaxDigits(1024); // remove 10^N scaling
                        
                TClockAbstract *clock = TClock<T>::Get();
                TString value = clock->Format(x1o + i * bw - u0, unit.Data(), precision);
                
                if (precision < 1) {
                        while (value.Contains(".") && value.EndsWith("0") && !value.EndsWith(".0")) value.Chop();
                        if (value.EndsWith(".")) value.ReplaceAll(".", ".0");
                }

                a->ChangeLabel(i + 1, -1, -1, -1, kBlack, -1, value);
                a->SetTitle(Form("Time [%s] from %s (%s %s)", unit.Data(), clock->Format(nullptr, u0, "UTC").Data(), clock->GetName().Data(), u0_str.Data()));
        }

        if (gPad) {
                gPad->Modified();
                gPad->Update();
        }
}

template <Clock T>
TClockAbstract *TClock<T>::Get() { 

        using ClockT    = std::integral_constant<Clock, T>;
        using ClockGPS  = std::integral_constant<Clock, Clock::GPS>;
        using ClockTAI  = std::integral_constant<Clock, Clock::TAI>;
        using ClockUnix = std::integral_constant<Clock, Clock::Unix>;

        if (std::is_same<ClockT, ClockUnix>::value) return gClockUnix;
        if (std::is_same<ClockT, ClockGPS>::value ) return gClockGPS;
        if (std::is_same<ClockT, ClockTAI>::value ) return gClockTAI;

        return gClock;
}

template <Clock T>
TH1* TClock<T>::FormatTimeAxis(TH1 *h, const Option_t* option, const char *timezone, int precision, int axis) 
{
        if (!h) return NULL;
        
        TString opt = option;
        opt.ToLower();
        
        // Remove existing TExec with the same name if found
        TList* functions = h->GetListOfFunctions();
        TObject* existingExec = functions->FindObject(TString(h->GetName())+"::"+__METHOD_NAME__);
        if (existingExec) {
                functions->Remove(existingExec);
                delete existingExec;
        }

        // Add new TExec
        functions->Add(new TExec(
                TString(h->GetName())+"::"+__METHOD_NAME__, TString::Format("TClock<"+TClock<T>::GetName()+">::"+__METHOD_NAME__+"(\"%s\", \"%s\", \"%s\", %d, %d);", 
                h->GetName(), opt.Data(), timezone, precision, axis)
        ));

        return h;
}

template <Clock T>
bool TClock<T>::IsLeapSecondImpl(double timestamp)
{
        using ClockT    = std::integral_constant<Clock, T>;
        using ClockGPS  = std::integral_constant<Clock, Clock::GPS>;
        using ClockTAI  = std::integral_constant<Clock, Clock::TAI>;
        using ClockUnix = std::integral_constant<Clock, Clock::Unix>;

        if (std::is_same<ClockT, ClockGPS>::value) {

                if (std::find(TClock<T>::leapSeconds.begin(), TClock<T>::leapSeconds.end(), gClock->TimeS(timestamp)) != TClock<T>::leapSeconds.end())
                        return true;
        }

        if (std::is_same<ClockT, ClockUnix>::value) {

                timestamp -= unixTimeGPS - TClock<T>::GetLeapSecondsImpl(timestamp);
                int timeS = gClock->TimeS(timestamp)+1;
                int timeN = gClock->TimeN(timestamp);

                if(std::find(TClock<T>::leapSeconds.begin(), TClock<T>::leapSeconds.end(), timeS) != TClock<T>::leapSeconds.end())
                        return timeN >= 500'000'000;
        } 
        
        if (std::is_same<ClockT, ClockTAI>::value) {
                return TClock<Clock::GPS>::IsLeapSecondImpl(TClock<T>::ConvertTo<Clock::GPS>(timestamp));
        }

        return false;
}

template <Clock T>
int TClock<T>::GetLeapSecondsImpl(double timestamp)
{
        using ClockT    = std::integral_constant<Clock, T>;
        using ClockGPS  = std::integral_constant<Clock, Clock::GPS>;
        using ClockTAI  = std::integral_constant<Clock, Clock::TAI>;
        using ClockUnix = std::integral_constant<Clock, Clock::Unix>;

        if (std::is_same<ClockT, ClockGPS>::value) {

                int offset = 0;
                for (int leapSecondTime : leapSeconds) {

                        if (leapSecondTime > timestamp) break;
                        offset++;
                }

                return offset;
        }

        if (std::is_same<ClockT, ClockUnix>::value) {

                double GTimeS = timestamp - unixTimeGPS;
                int nLeapSeconds = 0;

                for(int i = 0, N = TClock::leapSeconds.size(); i < N; i++) {

                        if(GTimeS+nLeapSeconds > TClock::leapSeconds[i]) nLeapSeconds++;

                        bool isLeapSecond = ROOT::IOPlus::Helpers::EpsilonEqualTo((double) GTimeS+nLeapSeconds, (double) TClock::leapSeconds[i]);
                        if(isLeapSecond) nLeapSeconds++;
                }

                return nLeapSeconds;
        } 
        
        if (std::is_same<ClockT, ClockTAI>::value) {
                return TClock<Clock::GPS>::GetLeapSecondsImpl(TClock<T>::ConvertTo<Clock::GPS>(timestamp));
        }

        throw std::invalid_argument("Leap second not implemented for this clock.");
}

template <Clock T>
double TClock<T>::ParseTimeOffset(TString _offset_str)
{
        std::string offset_str = _offset_str.Data();
        if(offset_str.empty()) return 0;
      
        // Define the regex pattern
        std::regex pattern(R"(([+-]?)(\d{1,2}):?(\d{2})?:?(\d{2})?(\.\d{1,9})?)");

        // Match the string against the regex pattern
        std::smatch match;
        if (std::regex_match(offset_str, match, pattern)) {

                // Determine the sign of the offset (+/-)
                int sign = (match[1].str() == "-") ? -1 : 1;

                // Extract hours, minutes, seconds, and nanoseconds
                int hours = std::stoi(match[2].str());
                int minutes = match[3].matched ? std::stoi(match[3].str()) : 0;
                int seconds = match[4].matched ? std::stoi(match[4].str()) : 0;
                
                double nanoseconds = 0;
                if (match[5].matched) {
                        // Get the string part without the leading dot
                        std::string ns_str = match[5].str().substr(1); // Remove the leading '.'

                        nanoseconds = std::stod(ns_str) / pow(10,ns_str.size()) * 1e9; // Convert to nanoseconds
                }
                
                // Calculate total offset in seconds with nanoseconds as fraction
                double total_seconds = sign * ((hours * 3600) + (minutes * 60) + seconds + 1e-9 * nanoseconds);
                return total_seconds;

        } 
        
        throw std::invalid_argument("Invalid offset string format");
}

template <Clock T>
double TClock<T>::Parse(const char *_datetime, const char *_format)
{
        // Format guesser
        std::vector<TString> formats;
        if  (_format != nullptr) formats.push_back(TString(_format).ReplaceAll("%z", "%Z"));
        else {
                
                std::vector<TString> dates { "%Y-%m-%d", "%Y/%m/%d", "%Y %m %d", 
                                             "%d-%m-%Y", "%d/%m/%Y", "%d %m %Y", "%a, %d %b %Y" };
                std::vector<TString> times { "%T", "%Hh%Mm%Ss", "%H:%M:%S", "%I%p:%M:%S", "%Hh%Mm", "%H:%M", "%I%p%M", "%Hh", "%I%p" };
                std::vector<TString> zones { "%z", "%Ez", "%Z"};

                for (const auto& zone : zones) {
                        for (const auto& time : times) {
                                for (const auto& date : dates) {

                                        formats.push_back(date + " " + time + " " + zone);
                                        formats.push_back(time + " " + date + " " + zone);

                                        formats.push_back(date + " " + time);
                                        formats.push_back(time + " " + date);

                                        formats.push_back(date);
                                        formats.push_back(time);
                                        formats.push_back(zone);

                                        formats.push_back(date + " " + zone + " " + time);
                                        formats.push_back(time + " " + zone + " " + date);
                                        formats.push_back(zone + " " + time + " " + date);
                                        formats.push_back(zone + " " + date + " " + time);
                                }
                        }
                }
        }

        // Datetime preprocess
        TString datetime = _datetime;
        for (auto it = timezoneOffsets.end(); it != timezoneOffsets.begin(); --it) // reverse iteration: UTC-x has priority over UTC (same for GTM)
                datetime = datetime.ReplaceAll(it->first, it->second);

        std::regex pattern(R"(\.(\d{1,9}))");
        std::smatch match;
        for(int i = 0, N = formats.size(); i < N; i++) {

                TString format = formats[i];

                std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds> tp; 
                std::istringstream in(datetime.Data()); // Example datetime string with timezone
                
                std::string tz;
                const date::time_zone *zone = nullptr;

                // Extract nanosecond information (if found)
                double nanoseconds = 0;
                        
                // Define the regex pattern
                std::string datetime_str = datetime.Data();
                if (std::regex_match(datetime_str, match, pattern)) {

                        std::string _nanoseconds = match[1];
                        while (_nanoseconds.length() < 9)
                                _nanoseconds += "0";

                        // Convert to double
                        nanoseconds = std::stod(_nanoseconds);
                }
                
                // Parse datetime string
                if(format.Contains("%Z")) {

                        try { in >> date::parse(format.Data(), tp, tz); }
                        catch(const std::runtime_error& e) { }
                        
                } else {

                        try { in >> date::parse(format.Data(), tp); }
                        catch(const std::runtime_error& e) { }
                }
                
                if(in.fail() || in.bad()) continue;
                if(!in.eof() && in.tellg() != datetime.Length()) continue;

                if(format.Contains("%Z")) {

                        if(tz.empty()) zone = date::current_zone();
                        else zone = date::locate_zone(tz);

                } else {

                        zone = date::locate_zone("UTC");
                }

                auto zt = date::make_zoned(zone, tp);
                return TClock<Unix>::ConvertTo<T>((std::chrono::duration_cast<std::chrono::nanoseconds>(zt.get_sys_time().time_since_epoch()).count()+nanoseconds)/1'000'000'000);
        }

        TString _format_str = _format ? "format \""+TString(_format)+"\"" : "default formats";
        throw std::runtime_error("failed to parse \""+TString(_datetime)+"\" with "+_format_str+". please provide appropriate format information.");
}

template <Clock T>
TString TClock<T>::Format(const char *_format, double timestamp, const char *timezone, int precision)
{
        TString format = (_format == nullptr || *_format == '\0') ? "%Y-%m-%d %H:%M:%S %Z" : _format;

        // Get timezone
        TString formatSuffix = timezone;
        TString timeOffset;
        const date::time_zone* zone = date::current_zone(); // Use local timezone if not specified
        if (!TString(timezone).EqualTo("")) {

                auto it = timezoneOffsets.find(timezone);
                if (it == timezoneOffsets.end()) {
                        zone = date::locate_zone(timezone);
                        formatSuffix = "";

                } else {

                        zone = date::locate_zone("UTC");
                        timeOffset = it->second;
                        formatSuffix = it->first;
                }
        }

        if(!formatSuffix.EqualTo("")) {

                format.ReplaceAll("%Z", formatSuffix);
                format.ReplaceAll("%z", timeOffset.ReplaceAll(":", ""));
                format.ReplaceAll("%Ez", timeOffset);
        }

        //
        // Handle sub-second case
        auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::duration<double>(this->ConvertTo<Unix>(timestamp) + this->ParseTimeOffset(timeOffset)));
        auto sys_time = date::sys_time<std::chrono::nanoseconds>{duration};

        double second = this->IsLeapSecond(timestamp) ? 60.0 + 1e-9*this->TimeN(timestamp) : TString(date::format("%S", date::make_zoned(zone, sys_time))).Atof();
        std::regex pattern("%(\\.(\\*|\\d+)+)?S");

        format = format.ReplaceAll("%S", "%."+TString::Itoa(precision,10)+"S");

        std::string formatStr = format.Data();
        auto begin = std::sregex_iterator(formatStr.begin(), formatStr.end(), pattern);
        auto end = std::sregex_iterator();

        std::vector<TString> precisions;
        for (std::sregex_iterator i = begin; i != end; ++i) {
                std::smatch match = *i;
                if (match.size() > 2) {
                        precisions.push_back(match[2].str().c_str());
                }
        }

        precisions.erase(std::unique(precisions.begin(), precisions.end()), precisions.end());
        for(int i = 0; i < precisions.size(); i++) {
                
                TString _second = Form("%."+precisions[i]+"f", second);
                if(_second.EqualTo("0")) _second = "00";
               
                int pos = _second.Index(".");
                while(pos != -1 && pos < 2) {

                        _second = "0" + _second;
                        pos = _second.Index(".");
                }

                TString _second_int = pos < 0 ? _second : _second(0, pos);
                while (_second_int.Length() < 2) {
                        _second     = "0" + _second;
                        _second_int = "0" + _second_int;
                }

                format.ReplaceAll("%."+precisions[i]+"S", _second);
        }

        // Format the sys_time as a string using the specified timezone
        return TString(date::format(format.Data(), date::make_zoned(zone, sys_time)).c_str());
}

template <Clock T>
TString TClock<T>::LocalTz()
{
        auto localtime = date::make_zoned(date::current_zone(), std::chrono::system_clock::now());
        return localtime.get_time_zone()->name().c_str();
}

template class TClock<Clock::Unix>;
template class TClock<Clock::TAI>;
template class TClock<Clock::GPS>;