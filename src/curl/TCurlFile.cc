#include "ROOT/IOPlus/Impl/TCurlFileImpl.h"

ClassImp(TCurlFile)

TCurlFile::TCurlFile(const char *filename, Option_t *option, int ttl, const char *pool, Int_t compression)
{
    owner = std::this_thread::get_id();
    
    pImpl = new TCurlFile::Impl();
    SetTTL(ttl);
    SetPool(pool);
    
    gDirectory = nullptr;
    SetName(filename);
    TDirectoryFile::Build(this, nullptr);
    
    fD = -1;
    fFile = this;
    fFree = nullptr;
    fVersion = gROOT->GetVersionInt(); // ROOT version in integer format
    fUnits = 4;
    fOption = option;
    SetCompressionSettings(compression);
    fWritten = 0;
    fSumBuffer = 0;
    fSum2Buffer = 0;
    fBytesRead = 0;
    fBytesWrite = 0;
    fClassIndex = nullptr;
    fSeekInfo = 0;
    fNbytesInfo = 0;
    fProcessIDs = nullptr;
    fNProcessIDs = 0;
    SetBit(kBinaryFile, kFALSE);
    
    fOption = option;
    fOption.ToUpper();
    
    if (fOption == "NEW")
        fOption = "CREATE";
    
    Bool_t create = (fOption == "CREATE") ? kTRUE : kFALSE;
    Bool_t recreate = (fOption == "RECREATE") ? kTRUE : kFALSE;
    Bool_t update = (fOption == "UPDATE") ? kTRUE : kFALSE;
    Bool_t read = (fOption == "READ") ? kTRUE : kFALSE;
    
    if (!create && !recreate && !update && !read) {
        read = kTRUE;
        fOption = "READ";
    }
    
    Bool_t devnull = kFALSE;
    const char *fname = nullptr;
    
    if (!filename || !filename[0]) {
        Error("TCurlFile", "file name is not specified");
        goto zombie;
    }
    
    // support dumping to /dev/null on UNIX
    if (!strcmp(filename, "/dev/null") && !gSystem->AccessPathName(filename, kWritePermission)) {
        devnull = kTRUE;
        create = kTRUE;
        recreate = kFALSE;
        update = kFALSE;
        read = kFALSE;
        fOption = "CREATE";
        SetBit(TFile::kDevNull);
    }
    
    gROOT->cd();
    
    fname = gSystem->ExpandPathName(filename);
    if (fname) {
        SetName(fname);
        delete[](char *) fname;
        fname = GetName();
    } else {
        Error("TCurlFile", "error expanding path %s", filename);
        goto zombie;
    }
    
    if (recreate) {
        if (!gSystem->AccessPathName(fname, kFileExists))
            gSystem->Unlink(fname);
        create = kTRUE;
        fOption = "CREATE";
    }
    
    if (create && !devnull && !gSystem->AccessPathName(fname, kFileExists)) {
        Error("TCurlFile", "file %s already exists", fname);
        goto zombie;
    }
    
    if (update) {
        if (gSystem->AccessPathName(fname, kFileExists)) {
            update = kFALSE;
            create = kTRUE;
        }
        if (update && gSystem->AccessPathName(fname, kWritePermission)) {
            Error("TCurlFile", "no write permission, could not open file %s", fname);
            goto zombie;
        }
    }
    
    if (read) {
        if (gSystem->AccessPathName(fname, kFileExists)) {
            Error("TCurlFile", "file %s does not exist", fname);
            goto zombie;
        }
        if (gSystem->AccessPathName(fname, kReadPermission)) {
            Error("TCurlFile", "no read permission, could not open file %s", fname);
            goto zombie;
        }
    }
    
    fRealName = fname;
    
    if (create || update)
        SetWritable(kTRUE);
    else
        SetWritable(kFALSE);
    
    return;
    
    zombie:
        MakeZombie();
        gDirectory = gROOT;
}

TCurlFile::~TCurlFile() 
{
    Close();
    
    if(pImpl) {
        delete pImpl;
        pImpl = NULL;
    }

    if(fChecksum) {
        delete[] fChecksum;
        fChecksum = NULL;
    }
}

TString TCurlFile::GetCache(const char *url) const
{
    TMD5 *md5 = TMD5::FileChecksum(this->GetName());
    return this->GetPoolDirectory() + "/" + TString(url).ReplaceAll("/", "_");
}

int TCurlFile::GetTTL() const {
    return this->ttl;
}

void TCurlFile::SetTTL(int ttl) {
    this->ttl = ttl;
}

TString TCurlFile::GetPoolDirectory() const
{
    TString dir = (TString) gSystem->TempDirectory();
            dir = dir + (dir.EndsWith("/") ? "" :"/") + this->fPool;

    gSystem->mkdir(dir, true);
    return dir;
}

const char *TCurlFile::GetPool() const
{
    return this->fPool;
}

void TCurlFile::SetPool(const char *pool)
{
    this->fPool = pool;
}

const char *TCurlFile::Checksum() const
{
    TMD5 *md5 = TMD5::FileChecksum(this->GetName());
    if(this->fChecksum) {
        delete[] this->fChecksum;
    }

    if(md5 == nullptr) {
    
        this->fChecksum = new char[1];
        this->fChecksum[0] = '\0';
    
    } else {
    
        TString fChecksum = md5->AsString(); 
        this->fChecksum = new char[fChecksum.Length() + 1];
        std::strcpy(this->fChecksum, fChecksum.Data());

        delete md5;
        md5 = nullptr;
    }
    
    return this->fChecksum;
}

TCurlDict TCurlFile::GetHeaders() const
{
    return this->pImpl->GetHeaders();
}

void TCurlFile::ClearHeaders()
{
    this->pImpl->ClearHeaders();
}

TCurlFile &TCurlFile::AddHeader(TString header, TString value)
{
    this->pImpl->AddHeader(header, value);
    return *this;
}

TString TCurlFile::GetHeader(TString name) const
{
    return this->pImpl->GetHeader(name);
}

TCurlFile &TCurlFile::SetHeaders(const TCurlDict &headers)
{
    this->ClearHeaders();
    for (auto it = headers.begin(); it != headers.end(); it++)
        this->AddHeader(it->first, it->second);

    return *this;
}

TCurlFile &TCurlFile::RemoveHeader(TString header)
{
    this->pImpl->RemoveHeader(header);
    return *this;
}

bool TCurlFile::Hit(std::shared_ptr<TCurlRequest> request, Option_t *option)
{
    // Check if cache is enabled
    if (ttl == 0)
        return false;

    // Check if request is GET (others should never be cached)
    request = request ? request : this->pImpl->request;
    if (request == nullptr) 
        return false;
    if (request->method != TCurlMethod::GET)
        return false;

    struct stat stat_buf;
    if (stat(this->GetCache(request->uri), &stat_buf)) 
        return false;

    // Check last modified time
    time_t now = time(nullptr);
    if(now - stat_buf.st_mtime > this->ttl || !stat_buf.st_size)
        return false;

    // Check on last modification on remote by requesting the header only
    TString opt = TString(request->GetOptions()) + " " + option;
            opt.ToUpper();

    if(opt.Contains("NOREMOTE")) 
        return false;

    auto response = this->pImpl->RequestHeader(request);
    if(response == NULL) return false; // on curl error

    // Check Expires for cache validation
    TString expires = response->GetHeader("Expires");
    if(!expires.EqualTo("")) {

        time_t expiry = TCurlFile::Impl::_curl_parse_time(expires);
        return expiry > now;
    }

    // Use Cache-Control for strong cache validation
    TString cacheControl = response->GetHeader("Cache-Control");
    if(cacheControl.Contains("no-cache") || cacheControl.Contains("no-store"))
        return false;

    // Check Last-Modified for cache validation
    TString lastModified = response->GetHeader("Last-Modified");
    if(!lastModified.EqualTo("")) {

        time_t lastModifiedTime = TCurlFile::Impl::_curl_parse_time(lastModified);
        return lastModifiedTime < stat_buf.st_mtime;
    }

    // Check ETag for cache validation
    TString etag = response->GetHeader("ETag");
    if (!etag.EqualTo("")) {

        TString cache = this->GetCache(request->uri);
        TMD5 *md5 = TMD5::FileChecksum(cache);
        TString fCheckSumUrl = md5->AsString();
        delete md5;

        return fCheckSumUrl.EqualTo(etag);
    }

    return false;
}

bool TCurlFile::IsLocal(const char *uri)
{
    return TFileReader::Exists(uri) || TString(TUrl(uri).GetHost()).EqualTo("");
}

void TCurlFile::SetProgressChar(char c)
{
    this->pImpl->SetProgressChar(c);
}

void TCurlFile::SetProgressWidth(int width)
{
    this->pImpl->SetProgressWidth(width);
}

void TCurlFile::SetProgressPrefix(const char *prefix)
{
    this->pImpl->SetProgressPrefix(prefix);
}

void TCurlFile::SetProgressSuffix(const char *suffix)
{
    this->pImpl->SetProgressSuffix(suffix);
}

std::shared_ptr<const TCurlResponse> TCurlFile::GetResponse() const
{
    return this->pImpl->response;
}

std::shared_ptr<const TCurlRequest> TCurlFile::GetRequest() const
{
    return this->pImpl->request;
}

bool TCurlFile::Send(std::shared_ptr<TCurlRequest> payload, Option_t *option)
{
    assert(owner == std::this_thread::get_id() && "TCurlFile is meant to be single-threaded only! Please instanciate class within child thread.");

    if(payload == NULL) return false;

    // Check cache validity
    TString cache =  this->GetCache(payload->uri);
    bool hit = this->Hit(payload, "NOREMOTE");    
    if ( hit ) {

        this->pImpl->request = payload;
        if (this->Hit(payload, option)) {

            // Check if existing cache matches checksum of saved file
            TMD5 *md5 = TMD5::FileChecksum(cache);
            TString fCheckSumUrl = md5->AsString();
            delete md5;

            return fCheckSumUrl.EqualTo(this->Checksum()) ? true : gSystem->CopyFile(cache, this->GetName(), true) == 0;
        }
    }

    // Delete existing cache if invalid
    struct stat stat_buf;
    const char *fname = this->ttl ? cache.Data() : this->GetName();
    if(!hit && stat(fname, &stat_buf) == 0) {
        gSystem->Unlink(fname);
    }

    FILE *fp = fopen(fname, !payload->noResume ? "ab+" : "wb");
    if (!fp) {
        std::cerr << "Error opening file `" << fname << "`." << std::endl;
        return false;
    }

    if(!payload->noResume) {
        payload->ResumeFromLarge(ftell(fp));
        fseek(fp, 0, SEEK_END);
    }

    auto response = this->pImpl->Request(payload, fp);
    fclose(fp);

    // Delete downloaded file if something went wrong
    if(stat(fname, &stat_buf) == 0) {

        if(stat_buf.st_size < 1) {
            gSystem->Unlink(fname);
            return false;
        }

        if(response != NULL && response->GetStatusCode() >= 400) {
            gSystem->Unlink(fname);
            return false;
        }
    }

    if(this->ttl) {

        TMD5 *md5 = TMD5::FileChecksum(fname);
        if(md5 == nullptr) return false;

        TString fCheckSumUrl = md5->AsString();
        delete md5;

        return fCheckSumUrl.EqualTo(this->Checksum()) ? true : gSystem->CopyFile(fname, this->GetName(), true) == 0;
    }

    
    return this->pImpl->GetErrorCode() == CURLE_OK;
}

void TCurlFile::ClearPool()
{
    TString dir = this->GetPoolDirectory();
    if (gSystem->AccessPathName(dir, kFileExists)) {
        gSystem->Unlink( dir );
    }
}

void TCurlFile::ClearPool(const char *pool)
{
    TString dir = (TString) gSystem->TempDirectory();
            dir = dir + (dir.EndsWith("/") ? "" :"/") + pool;

    if (gSystem->AccessPathName(dir, kFileExists)) {
        gSystem->Unlink( dir );
    }
}

void TCurlFile::Evict(const char *uri) const
{
    gSystem->Unlink(this->GetCache(uri));
}

int TCurlFile::GetErrorCode() const { 
    return this->pImpl->GetErrorCode();
}

const char *TCurlFile::GetError() const { 
    return this->pImpl->GetError();
}

void TCurlFile::Dump()
{
    // Check if error found
    if(this->GetErrorCode() != CURLE_OK) {
        std::cerr << "curl_easy_perform(" << this->GetErrorCode() << ") failed: " << this->GetError() << std::endl;
    }

    // Dump file
    std::ifstream src(this->GetName(), std::ios::binary);
    if (!src) {
        std::cout << "No target `" << this->GetName() << "` found. Please download first to put file in place." << std::endl;
        return;
    }

    auto request  = this->GetRequest();
    auto response = this->GetResponse();
    if(!response || !request) std::cout << "HTTP 2xx";
    else {

        std::cout << "HTTP " << response->GetStatusCode();

        // Check HTTP status code
        if(!this->Hit()) std::cout << std::endl;
        else std::cout << " [CACHED]: " << this->GetCache(request->uri) << std::endl;

    }

    std::string line;
    std::cout << "HTTP Size: " << this->GetSize() << "B" << std::endl;
    std::cout << "HTTP Checksum: " << this->Checksum() << std::endl;

    bool isBinary = this->IsBinary();
    std::cout << "HTTP Content: " << (isBinary ? "[binary/octet-stream]" : "") << std::endl;
    if(!isBinary) {
    
        while (std::getline(src, line)) {
            std::cout << "> " << line << std::endl;
        }
    }

    src.close();
}

void TCurlFile::Print(Option_t *opt)
{
    UNUSED(opt);

    auto request = this->GetRequest();
    TString uri = request == NULL ? "<no request>" : request->uri;

    std::cout << "TCurlFile.Print  name=" << this->GetName() << ", uri='" << uri << "'";
    std::cout << (this->Hit() ? " [CACHED]" : "") << ", ttl=" << this->GetTTL() << ", option=" << this->GetOption();

    struct stat stat_buf;
    if (stat(this->GetName(), &stat_buf) == 0)
        std::cout << ", size=" << this->GetSize();

    std::cout << std::endl;
}

bool TCurlFile::IsBinary() const
{
    std::ifstream file(this->GetName(), std::ios::binary);
    if (!file) return false; 

    char ch;
    while (file.get(ch)) {

        if ((unsigned char)ch < 9 || ((unsigned char)ch > 13 && (unsigned char)ch < 32) || (unsigned char)ch == 127) {
            return true;
        }
    }

    return false;
}
