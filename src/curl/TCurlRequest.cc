#include "ROOT/IOPlus/TCurlRequest.h"

TCurlRequest::TCurlRequest(const char *uri, Option_t *options, const TCurlDict &headers, const TCurlMethod &method, const char *parameters)
    : uri(uri), options(options), headers(headers), method(method), parameters(parameters) {
    
    TString opt = options;
            opt.ToUpper();

    uri = gSystem->ExpandPathName(uri);

    this->HideProgress(opt.Contains("NOPROGRESS"));
    this->FollowLocation(!opt.Contains("NOFOLLOW"));
    this->IgnorePeerValidation(opt.Contains("IGNOREPEER"));
    this->NoBody(opt.Contains("NOBODY"));
    this->Verbose(opt.Contains("V"));
}

TCurlRequest::~TCurlRequest() {}

const char *TCurlRequest::GetUri() const {
    return uri;
}

Option_t * TCurlRequest::GetOptions() const
{
    return options;
}

TCurlMethod TCurlRequest::GetMethod() const {
    return method;
}

TCurlDict TCurlRequest::GetHeaders() const
{
    return this->headers;
}

void TCurlRequest::ClearHeaders() 
{
    this->headers.clear();
}

void TCurlRequest::AddHeader(TString key, TString value) 
{
    this->headers[key] = value;
}

void TCurlRequest::RemoveHeader(TString key) 
{
    auto it = this->headers.find(key);
    if ( it != this->headers.end() ) this->headers.erase(it);
}

const char *TCurlRequest::GetParameters() const {
    return parameters;
}

void TCurlRequest::IgnorePeerValidation(bool ignorePeerValidation)
{
    this->ignorePeerValidation = ignorePeerValidation;
}

void TCurlRequest::SetUserAgent(const char *userAgent)
{
    this->userAgent = userAgent;
}

void TCurlRequest::FollowLocation(bool followLocation)
{
    this->followLocation = followLocation;
}

void TCurlRequest::ResumeFromLarge(int64_t resumeBytePos)
{
    this->resumeBytePos = resumeBytePos;
}

void TCurlRequest::HideProgress(bool noProgress)
{
    this->noProgress = noProgress;
}

void TCurlRequest::NoBody(bool noBody)
{
    this->noBody = noBody;
}

void TCurlRequest::NoResume(bool noResume)
{
    this->noResume = noResume;
}

void TCurlRequest::Verbose(bool verbose)
{
    this->verbose = verbose;
}