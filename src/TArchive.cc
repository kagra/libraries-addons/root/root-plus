#include "ROOT/IOPlus/Impl/TArchiveImpl.h"

ClassImp(TArchive)

TArchive::TArchive(const char *archiveName, Option_t *option, const char *member): TArchiveFile()
{    
    this->SetOption(option);
    
    if (fOption == "NEW")
        fOption = "CREATE";
    
    Bool_t create = (fOption == "CREATE") ? kTRUE : kFALSE;
    Bool_t recreate = (fOption == "RECREATE") ? kTRUE : kFALSE;
    Bool_t update = (fOption == "UPDATE") ? kTRUE : kFALSE;
    Bool_t read = (fOption == "READ") ? kTRUE : kFALSE;
    
    if (!create && !recreate && !update && !read) {
        read = kTRUE;
        fOption = "READ";
    }
    
    Bool_t devnull = kFALSE;
    const char *fname = nullptr;
    
    if (!archiveName || !archiveName[0]) {
        Error("TArchive", "file name is not specified");
        goto zombie;
    }
    
    // support dumping to /dev/null on UNIX
    if (!strcmp(archiveName, "/dev/null") && !gSystem->AccessPathName(archiveName, kWritePermission)) {
        devnull = kTRUE;
        create = kTRUE;
        recreate = kFALSE;
        update = kFALSE;
        read = kFALSE;
        fOption = "CREATE";
        SetBit(TFile::kDevNull);
    }
    
    gROOT->cd();
    
    fname = gSystem->ExpandPathName(archiveName);
    if (fname) {
        SetName(fname);
        delete[](char *) fname;
        fname = GetName();
    } else {
        Error("TArchive", "error expanding path %s", archiveName);
        goto zombie;
    }

    if (recreate) {
        if (!gSystem->AccessPathName(archiveName, kFileExists))
            gSystem->Unlink(archiveName);
        create = kTRUE;
        fOption = "CREATE";
    }
    
    if (create && !devnull && !gSystem->AccessPathName(archiveName, kFileExists)) {
        Error("TArchive", "file %s already exists", archiveName);
        goto zombie;
    }
    
    if (update) {
        if (gSystem->AccessPathName(archiveName, kFileExists)) {
            update = kFALSE;
            create = kTRUE;
        }
        if (update && gSystem->AccessPathName(archiveName, kWritePermission)) {
            Error("TArchive", "no write permission, could not open file %s", archiveName);
            goto zombie;
        }
    }
    
    if (read) {
        if (gSystem->AccessPathName(archiveName, kFileExists)) {
            Error("TArchive", "file %s does not exist", archiveName);
            goto zombie;
        }
        if (gSystem->AccessPathName(archiveName, kReadPermission)) {
            Error("TArchive", "no read permission, could not open file %s", archiveName);
            goto zombie;
        }
    }

    Open();

    if(!TString(member).EqualTo("") && this->SetCurrentMember(member) == ARCHIVE_FATAL) {
        std::cerr << "Failed to read member `" << member << "` in " << archiveName << std::endl;
    }

    return;
    
    zombie:
        gDirectory = gROOT;
}

TArchive::~TArchive() 
{
    Close();
}

bool TArchive::IsCompressed() const
{
    CompressionFormat format = TArchive::GetCompression(fArchiveName);
    return format != CompressionFormat::NONE && format != CompressionFormat::UNKNOWN;
}

TArchive::CompressionFormat TArchive::GetCompression() const
{
    return this->GetCompression(this->pImpl->GetCompression());
}

TString TArchive::enum2str(TArchive::CompressionFormat compression)
{
    switch(compression) {

        case CompressionFormat::GZIP:
            return "gz";
        case CompressionFormat::BZIP2:
            return "bz2";
        case CompressionFormat::XZ:
            return "xz";
        case CompressionFormat::LZ4:
            return "lz4";
        case CompressionFormat::LZMA:
            return "lzma";
        case CompressionFormat::ZSTD:
            return "zst";
        case CompressionFormat::ZIP:
            return "zip";

        default:
            return "";
    }
}

TArchive::CompressionFormat TArchive::GetCompression(const char* compression)
{
    compression = TFileReader::Extension(compression);

    if (strcmp(compression, "gz") == 0) {
        return CompressionFormat::GZIP;
    } else if (strcmp(compression, "bz2") == 0) {
        return CompressionFormat::BZIP2;
    } else if (strcmp(compression, "xz") == 0) {
        return CompressionFormat::XZ;
    } else if (strcmp(compression, "lz4") == 0) {
        return CompressionFormat::LZ4;
    } else if (strcmp(compression, "lzma") == 0) {
        return CompressionFormat::LZMA;
    } else if (strcmp(compression, "zst") == 0) {
        return CompressionFormat::ZSTD;
    } else if (strcmp(compression, "zip") == 0) {
        return CompressionFormat::ZIP;
    } else if (strcmp(compression, "none") == 0) {
        return CompressionFormat::NONE;
    }

    return CompressionFormat::UNKNOWN;
}

Int_t TArchive::OpenArchive() 
{
    return this->Open();
}

Int_t TArchive::Open()
{
    pImpl = new TArchive::Impl(this->fArchiveName);

    this->fArchiveMembers = this->pImpl->GetEntryNames();
    this->Rollback(); // Initialize `fArchiveBuffer` based on fArchiveMembers

    return this->pImpl->Open();
}

Int_t TArchive::Extract(const char *destination)
{
    return this->pImpl->Extract(destination);
}

bool TArchive::ExtractMember(TString member, TString destination)
{
    if(this->SetCurrentMember(member) == ARCHIVE_FATAL) {
        std::cerr << "Failed to read member `" << member << "` in " << this->fArchiveName << std::endl;
        return false;
    }

    return this->pImpl->ExtractMember(this->GetCurrentIndex(), destination);
}


std::ifstream TArchive::OpenMember(const char *member, bool binaryMode) 
{
    if(this->SetCurrentMember(member) == ARCHIVE_FATAL) {
        std::cerr << "Failed to read member `" << member << "` in " << this->fArchiveName << std::endl;
        return std::ifstream();
    }

    TString fName;
    FILE *fp = ROOT::IOPlus::Helpers::TempFileName("temp-", &fName);
    fclose(fp); // Create empty file.

    int index = this->GetCurrentIndex();
    if(index < 0) return std::ifstream();

    if(!this->pImpl->ExtractMember(this->GetCurrentIndex(), fName))
        return std::ifstream();

    return binaryMode ? std::ifstream(fName, std::ios::binary) : std::ifstream(fName);
}

std::ifstream TArchive::OpenCurrentMember(bool binaryMode)
{
    return this->OpenMember(this->GetCurrentMember(), binaryMode);
}

TString TArchive::GetCurrentMember()
{
    return this->fCurrentMember;
}

int TArchive::GetCurrentIndex()
{
    unsigned int index = std::find(this->fArchiveMembers.begin(), this->fArchiveMembers.end(), this->fCurrentMember) - this->fArchiveMembers.begin();
    return index == this->fArchiveMembers.size() ? -1 : index;
}

Int_t TArchive::SetCurrentMember(const char *member)
{
    this->fCurrentMember = member;
    if (this->fCurrentMember.BeginsWith("./")) {
        this->fCurrentMember = this->fCurrentMember(2, this->fCurrentMember.Length() - 2);
    }

    int pos = this->GetCurrentIndex();
    if (pos < 0) return ARCHIVE_FATAL;

    this->pImpl->Seekg(pos);
    return ARCHIVE_OK;
}

int TArchive::GetN()
{
    return this->fArchiveMembers.size();
}

std::vector<TString> TArchive::GetListOfMembers()
{
    std::vector<TString> keys;
    keys.reserve(this->fArchiveBuffer.size()); // Reserve space to avoid unnecessary reallocations

    for (const auto& pair : this->fArchiveBuffer) {
        keys.push_back(pair.first);
    }
    
    return keys;
}

Int_t TArchive::Write(const char *newArchiveName, const CompressionFormat &format)
{   
    CompressionFormat _format = format;
    if (_format == CompressionFormat::UNKNOWN) {

        TString extension = newArchiveName;
        Ssiz_t dotPos = extension.Last('.');

        if(dotPos == kNPOS) extension = "";
        else {
            extension = extension(dotPos + 1, extension.Length() - dotPos - 1);
            extension.ToLower(); // Convert extension to lowercase
        }

        if (extension == "gz") {
            _format = CompressionFormat::GZIP;
        } else if (extension == "bz2") {
            _format = CompressionFormat::BZIP2;
        } else if (extension == "xz") {
            _format = CompressionFormat::XZ;
        } else if (extension == "lz4") {
            _format = CompressionFormat::LZ4;
        } else if (extension == "lzma") {
            _format = CompressionFormat::LZMA;
        } else if (extension == "zst") {
            _format = CompressionFormat::ZSTD;
        } else if (extension == "zip") {
            _format = CompressionFormat::ZIP;
        } else {
            _format = CompressionFormat::NONE;
        }
    }

    return Write(newArchiveName, _format, this->fArchiveBuffer);
}

Int_t TArchive::Write(const char *newArchiveName, const CompressionFormat &format, TArchiveBuffer archiveBuffer)
{
    return this->pImpl->Write(newArchiveName, format, archiveBuffer);
}

Int_t TArchive::Commit()
{
    int ret = this->pImpl->Write(this->GetName(), this->GetCompression(), this->fArchiveBuffer);
    this->Reset();

    return ret;
}

Int_t TArchive::Reset() {

    this->Close();
    return this->Open();    
}

void TArchive::Close()
{
    if(pImpl) {
        delete pImpl;
        pImpl = NULL;
    }

    if(fChecksum) {
        delete[] fChecksum;
        fChecksum = NULL;
    }  

    this->fArchiveMembers.clear();  
    this->fArchiveBuffer.clear();  
}

Option_t *TArchive::GetOption()
{
    return this->fOption;
}

void TArchive::SetOption(Option_t *option)
{
    this->fOption = option;
    this->fOption.ToUpper();
}

bool TArchive::IsOpen() const
{
    return this->pImpl->GetHandler() != nullptr;
}

const char *TArchive::Checksum() const
{
    TMD5 *md5 = TMD5::FileChecksum(this->GetName());
    if(this->fChecksum) {
        delete[] this->fChecksum;
    }

    if(md5 == nullptr) {
        this->fChecksum = new char[1];
        this->fChecksum[0] = '\0';
    }
    else {
        TString fChecksum = md5->AsString(); 
        this->fChecksum = new char[fChecksum.Length() + 1];
        std::strcpy(this->fChecksum, fChecksum.Data());

        delete md5;
        md5 = nullptr;
    }
    
    return this->fChecksum;
}

void TArchive::Print(Option_t *opt) 
{
    TArchiveFile::Print(opt);
}

void TArchive::Dump()
{
    this->Print();

    std::vector<TString> members = GetListOfMembers();
    std::cout << "This archive contains " << members.size() << " member(s):" << std::endl;
    for(int i = 0, N = members.size(); i < N; i++) {
        std::cout << "Member #" << i << ": " << members[i] << std::endl;
    }
}

void TArchive::Rollback(const char *member)
{
    TString _member = TString(member);
    this->fArchiveBuffer.clear();
    for(int i = 0, N = this->fArchiveMembers.size(); i < N; i++) {

        if(!_member.EqualTo("") && !_member.EqualTo(this->fArchiveMembers[i])) continue;
        this->fArchiveBuffer[this->fArchiveMembers[i]] = this->fArchiveName + ":" + this->fArchiveMembers[i];
    }
}

void TArchive::Add(TString member, TString fileName)
{
    TString _member = member;
    if (_member.BeginsWith("./")) {
        _member = _member(2, _member.Length() - 2);
    }

    if(this->fArchiveBuffer.find(_member) != this->fArchiveBuffer.end()) {
        std::cerr << "Warning: Member `" << _member << "` is already found in `" << this->fArchiveName << "`. skip." << std::endl;
        return;
    }
    
    this->fArchiveBuffer.insert(std::make_pair(_member, fileName));
}

void TArchive::Remove(TString member)
{
    TString _member = member;
    if (_member.BeginsWith("./")) {
        _member = _member(2, _member.Length() - 2);
    }

    this->fArchiveBuffer.erase(_member);
}