/**
 * @file TKeyboard.cc
 * @brief Source code of the TKeyboard class
 *
 * @details
 * This file contains the implementation of the TKeyboard class.
 *
 * @date 2019-03-27
 * @author Marco Meyer <marco.meyer@cern.ch>
 */

#include "TKeyboard.h"
ClassImp(TKeyboard)

TKeyboard *gKeyboard   = &TKeyboard::Instance();

// Initialize the static map with ASCII control characters
const std::map<unsigned char, TString> TKeyboard::asciiSpecMap = {
    {0, "NUL"}, {1, "SOH"}, {2, "STX"}, {3, "ETX"}, {4, "EOT"},
    {5, "ENQ"}, {6, "ACK"}, {7, "BEL"}, {8, "BS"}, {9, "TAB"},
    {10, "LF"}, {11, "VT"}, {12, "FF"}, {13, "CR"}, {14, "SO"},
    {15, "SI"}, {16, "DLE"}, {17, "DC1"}, {18, "DC2"}, {19, "DC3"},
    {20, "DC4"}, {21, "NAK"}, {22, "SYN"}, {23, "ETB"}, {24, "CAN"},
    {25, "EM"}, {26, "SUB"}, {27, "ESC"}, {28, "FS"}, {29, "GS"},
    {30, "RS"}, {31, "US"}, {32, "SP"}, {127, "DEL"}
};

int TKeyboard::x = 0;
int TKeyboard::y = 0;
#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

void TKeyboard::Connect(TPad *c)
{
	if (c == NULL) return;

	c->Connect(
		"ProcessedEvent(Int_t,Int_t,Int_t,TObject*)", "TKeyboard", 
		this, "Capture(Int_t,Int_t,Int_t,TObject*)"
	);
}

void TKeyboard::Capture(Int_t event, Int_t x, Int_t y, TObject *selected)
{
    TPad *c = static_cast<TPad *>(gTQSender);

    gPrint->Debug(100, __METHOD_NAME__,
		"Canvas %s: event=%d, x=%d (x0 = %d), y=%d (y0 = %d), selected=%s",
		c->GetName(), event, x, TKeyboard::x, y, TKeyboard::y, selected ? selected->IsA()->GetName() : "null"
	);

    // Get the singleton instance
    TKeyboard& keyboard = TKeyboard::Instance();

    // Build a dynamic sequence based on the event
    std::vector<unsigned char> sequence;

    // Add direction-specific information if arrow keys are pressed
	if (event == kArrowKeyRelease) {

        if (x < TKeyboard::x) {
            // Left arrow sequence
            sequence.push_back(0x1B);  // ESC
            sequence.push_back(0x5B);  // '['
            sequence.push_back(0x44);  // 'D' (left arrow)
        } else if (x > TKeyboard::x) {
            // Right arrow sequence
            sequence.push_back(0x1B);  // ESC
            sequence.push_back(0x5B);  // '['
            sequence.push_back(0x43);  // 'C' (right arrow)
        }

        if (y < TKeyboard::y) {
            // Top (up) arrow sequence
            sequence.push_back(0x1B);  // ESC
            sequence.push_back(0x5B);  // '['
            sequence.push_back(0x41);  // 'A' (up arrow)
        } else if (y > TKeyboard::y) {
            // Bottom (down) arrow sequence
            sequence.push_back(0x1B);  // ESC
            sequence.push_back(0x5B);  // '['
            sequence.push_back(0x42);  // 'B' (down arrow)
        }

    } else if ( event != kArrowKeyPress) {

		sequence.push_back(static_cast<unsigned char>(event));
	}

    // Update static coordinates for arrow key handling
    if (event == kArrowKeyPress || event == kArrowKeyRelease) {
        // Update the coordinates via the singleton instance
        keyboard.x = x;
        keyboard.y = y;
    }

    // Add the constructed sequence to keylist via the singleton instance
	keyboard.keylistFromGUI.insert(keyboard.keylistFromGUI.begin(), KeyEvent(sequence));

	// Process events to ensure ROOT's event loop is up to date
    gSystem->ProcessEvents();
}

int TKeyboard::Read(bool wait)
{
	if(gROOT->IsBatch()) return EOF;

    int chr = -1;

    #ifdef _WIN32
        if (_kbhit() || wait) chr = _getch();
    #else

		struct termios oldt, newt;
		tcgetattr(STDIN_FILENO, &oldt); // Save current terminal settings
		newt = oldt;
		newt.c_lflag &= ~(ICANON | ECHO); // Disable canonical mode and echo
		tcsetattr(STDIN_FILENO, TCSANOW, &newt); // Apply the new settings

		// Blocking mode
		if (wait) chr = getchar(); // Read a character
		else {

			int flags = fcntl(STDIN_FILENO, F_GETFL, 0); // Get current file descriptor flags
			fcntl(STDIN_FILENO, F_SETFL, flags | O_NONBLOCK); // Set non-blocking mode
			
			unsigned char buf;
			ssize_t n = read(STDIN_FILENO, &buf, 1); // Attempt to read a character
			if (n > 0) chr = buf; // If a character is read, store it
			
			fcntl(STDIN_FILENO, F_SETFL, flags);
		}

		tcsetattr(STDIN_FILENO, TCSANOW, &oldt); // Restore the original settings
    #endif

    // Ensure no characters are printed to the console
	if(chr != EOF) {
		gPrint->Debug(100, __METHOD_NAME__, "New char received `%s`", Reveal(chr).Data());
	}

    return chr;
}

std::vector<unsigned char> TKeyboard::ReadSequence(int tick) {
    
	std::vector<unsigned char> sequence;
    auto start = std::chrono::steady_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count();
	
	// Blocking mode
    #ifndef _WIN32
	struct termios oldt, newt;
	tcgetattr(STDIN_FILENO, &oldt); // Save current terminal settings
	newt = oldt;
	newt.c_lflag &= ~(ICANON | ECHO); // Disable canonical mode and echo
	tcsetattr(STDIN_FILENO, TCSANOW, &newt); // Apply the new settings
	#endif

	while (1) {

        int chr = Read();
        if (chr == EOF) {

			elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count();
			if (elapsed > tick) break;
			continue;			
		}

		// Cast int back to char
		sequence.push_back(static_cast<unsigned char>(static_cast<char>(chr & 0xFF)));

 		start = std::chrono::steady_clock::now(); // Reset the timer on every input
		gPrint->Debug(100, __METHOD_NAME__,"Elapsed time: %dms / %dms" , elapsed, tick);
		gSystem->ProcessEvents();
    }

	if(sequence.size()) {
		gPrint->Debug(100, __METHOD_NAME__,"Timeout limit reached: %dms / %dms" , elapsed, tick);
	}

    #ifndef _WIN32
	tcsetattr(STDIN_FILENO, TCSANOW, &oldt); // Restore the original settings
	#endif

	usleep(tick);

	if(!sequence.size() && this->keylistFromGUI.size()) {
		sequence = this->keylistFromGUI.back().keys;
		this->keylistFromGUI.pop_back();
	}

    return sequence;
}

TKeyboard::KeyEvent TKeyboard::KeyUp(int tick) {

    std::vector<unsigned char> sequence = ReadSequence(tick);
	
	bool found = false;
    if (!sequence.empty()) {
		
		keylistUpdate = std::chrono::steady_clock::now();

        KeyEvent event(sequence);
        for (const auto& _event : this->keylist) {
            found |= _event.Contains(event);
        }

        if (!found) {

            this->keylist.insert(this->keylist.begin(), event);
            return KeyEvent();
        }
    }

    if (AllowPop(tick)) {

        KeyEvent event = this->keylist.back();
		gPrint->Debug(100, __METHOD_NAME__, "Key triggered `%s` (DEC %s)", event.Reveal().Data(), event.ToDec().Data());

        this->keylist.pop_back();
        return event;
    }

    return KeyEvent();
}

TKeyboard::KeyEvent TKeyboard::KeyDown(int tick)
{
    std::vector<unsigned char> sequence = ReadSequence(tick);
	
	if(sequence.size()) {
		
		KeyEvent event = KeyEvent(sequence);
		for( auto _event : this->keylist ) {
			if(_event.Contains(event)) return KeyEvent();
		}

		gPrint->Debug(100, __METHOD_NAME__, "Key triggered `%s` (DEC %s)", event.Reveal().Data(), event.ToDec().Data());
		this->keylist.insert(this->keylist.begin(), event);
		return event;
	}

	if(AllowPop(tick)) this->keylist.pop_back();
	return KeyEvent();
}

TKeyboard::KeyEvent TKeyboard::KeyPress(int tick)
{
    std::vector<unsigned char> sequence = ReadSequence(tick);
	
	if(sequence.size()) {

		KeyEvent event = KeyEvent(sequence);
		if(std::find(this->keylist.begin(), this->keylist.end(), event) == this->keylist.end())
			this->keylist.insert(this->keylist.begin(), event);

		gPrint->Debug(100, __METHOD_NAME__, "Key triggered `%s` (DEC %s)", event.Reveal().Data(), event.ToDec().Data());
		return event;
	}

	if(AllowPop(tick)) {
        KeyEvent event = this->keylist.back();
		this->keylist.pop_back();
	}
	
    return KeyEvent();
}

TKeyboard::KeyEvent TKeyboard::Listen(const std::vector<KeyEvent> &keystop, KeyAction action, int tick) {

	if(gROOT->IsBatch()) return KeyEvent();
	
	while(1) {
		TKeyboard::KeyEvent event;
		switch(action) {
			case KeyAction::Up:
				event = this->KeyUp(tick);
				break;
			case KeyAction::Down:
				event = this->KeyDown(tick);
				break;
			case KeyAction::Press:
				event = this->KeyPress(tick);
				break;
		}

		if(!event.Empty()) {
	
			bool process  = (keystop.size() == 0);
				 process |= std::find(keystop.begin(), keystop.end(), event) != keystop.end();

			if(process) { 
				gPrint->Debug(10, __METHOD_NAME__, ">> Waiting for new sequence input");
				return event;
			}
		}

		gSystem->ProcessEvents();
    }

	return KeyEvent();
}

void TKeyboard::Dump(KeyAction action, int tick) {

	if(gROOT->IsBatch()) return;

	int occurrence = 0;
    std::cout << ">> Waiting for sequence input" << std::endl;

	while(1) {

        TKeyboard::KeyEvent event;
		switch(action) {
			case KeyAction::Up:
				event = this->KeyUp(tick);
				break;
			case KeyAction::Down:
				event = this->KeyDown(tick);
				break;
			case KeyAction::Press:
				event = this->KeyPress(tick);
				break;
		}

		if(event.Empty()) continue;
		gPrint->ClearPage();

		auto now = std::chrono::system_clock::now();
		std::time_t now_time_t = std::chrono::system_clock::to_time_t(now);
		std::tm* now_tm = std::localtime(&now_time_t);
		
    	std::cout << std::endl << "Last sequence #" << (++occurrence) << " received at " <<std::put_time(now_tm, "%Y-%m-%d %H:%M:%S") << std::endl;

		std::cout << std::endl << "DEC\tHEX\tBIN\t\tASCII" << std::endl;
		for(auto key : event)
			std::cout << Dec(key) << "\t0x" << Hex(key) << "\t0b" << Binary(key) << "\t`" << Reveal(key) << "`" << std::endl;

		std::cout << std::endl << ">> Waiting for new batch sequence" << std::endl;
		gSystem->ProcessEvents();
    }
}

TString TKeyboard::Binary(unsigned char decimal) {

    std::bitset<8> binary(decimal);
    return TString(binary.to_string().c_str());
}

TString TKeyboard::Hex(unsigned char decimal) {

    std::stringstream hexStream;

	hexStream << std::hex << std::uppercase 
              << std::setw(2) << std::setfill('0') 
              << static_cast<int>(decimal);

    return hexStream.str().c_str();
}

int TKeyboard::Dec(unsigned char decimal) {

    return static_cast<int>(decimal);
}

TString TKeyboard::Reveal(unsigned char chr) {
    
    // Check if the character is printable
    switch (chr) {
        case '\n': return "\\n";
		case '\r': return "\\r";
		case '\t': return "\\t";
		case '\b': return "\\b";
		case '\f': return "\\f";
		case '\v': return "\\v";
		default:
            if (std::isprint(chr) && chr != ' ') {
                return TString(static_cast<char>(chr));
            } else if (asciiSpecMap.find(chr) != asciiSpecMap.end()) {
                return asciiSpecMap.find(chr)->second;
            } else {
                std::ostringstream oss;
                oss << "\\x" << std::hex << std::setw(2) << std::setfill('0') << Dec(chr);
                return TString(oss.str().c_str());
            }
            break;
    }

	return TString('\0');
}

TKeyboard::KeyEvent TKeyboard::PressAnyToContinue() 
{
	KeyEvent event;
	while (event.Empty() && gPrint->IsInteractive()) {
	
		std::vector<unsigned char> sequence = ReadSequence();
		if(sequence.size()) event = KeyEvent(sequence);

		gSystem->ProcessEvents();
	}
		
    return event;
}
