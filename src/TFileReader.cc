/**
 * @file TFileReader.cc
 * @brief Source code of the TFileReader class
 *
 * @details
 * This file contains the implementation of the TFileReader class.
 *
 * @date 2019-03-27
 * @author Marco Meyer <marco.meyer@cern.ch>
 */

#include "TFileReader.h"
ClassImp(TFileReader)

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

/** @cond */
const int TFileReader::kList = 0;
const int TFileReader::kChain1D = 1;
const int TFileReader::kChain2D = 2;
const int TFileReader::kSum = 3;
const int TFileReader::kTree = 4;

const int TFileReader::kOpenLimit = 512;
const bool TFileReader::kReopen = false;
const bool TFileReader::kReuse = true;
const char TFileReader::kSeparator = '|';
const bool TFileReader::kUniqID = true;
const bool TFileReader::kAllID = false;
const int TFileReader::kDefaultDepth = -1;
const int TFileReader::kDefaultKey = -1;
/** @endcond */

std::vector<TFile*> TFileReader::vFiles = {};
int TFileReader::Instance = 0;
bool TFileReader::kOpenLimitTriggered = false;

TString TFileReader::ExpandVariables(TString str, bool bPwd)
{ 
        return TPrint::ExpandVariables(str, bPwd);
}

TFileReader::TFileReader(const char* name, const char* option, const char* origin, TClass* cl, int maxDepth)
: TFileReader::TFileReader(name, option, origin, (cl == NULL) ? TString("") : TString(cl->GetName()), maxDepth) {

        TString eMessage = (TString) "No origin found.. ";
        TPrint::ErrorIf(TString(origin).EqualTo(""), this->GetMethodName(__METHOD_NAME__), eMessage);
}

bool TFileReader::AppendToDirectory(std::vector<TObject*> vObject, TDirectory *gDir)
{
        if(TPrint::ErrorIf(gDir == NULL, __METHOD_NAME__, "Directory is NULL ptr..")) return 0;

        for(int i = 0; i < vObject.size(); i++) {

                if(vObject[i] == NULL) continue;
                if (!gDir->GetList()->FindObject( vObject[i] ) ) gDir->Append(vObject[i]);
        }

        return 1;
}

bool TFileReader::AppendToDirectory(TList *l, TDirectory *dir) {

        if(l == NULL) return 1;
        if(TPrint::ErrorIf(dir == NULL, __METHOD_NAME__, "Directory is NULL ptr..")) return 0;

        bool b = true;

        std::vector<TObject*> vObject;
        TObject *obj = NULL;
        TIter Next(l);
        while ((obj = (TObject*) Next())) {

                if (obj->InheritsFrom("TList")) b &= TFileReader::AppendToDirectory((TList*) obj, dir);
                else vObject.push_back(obj);
        }

        return ( vObject.size() ) ? TFileReader::AppendToDirectory(vObject, dir) : true;
}

bool TFileReader::CleanTextFile(TString fname)
{
        if(!fname.EqualTo("")) {

                std::ofstream f(fname, std::ios::out);
                if(!f.is_open()) return 0;

                f.close();
        }

        return 1;
}

TString TFileReader::FetchTreeDescriptor(TString fname)
{
        TString descriptor;

        std::vector<TString> vi = ExpandWildcardUrl(fname);
        for(int j = 0, J = vi.size(); j < J; j++) {

                TString vij = vi[j];

                std::ifstream f(vi[j], std::ios::in);
                if( TPrint::WarningIf(!f.is_open(), __METHOD_NAME__, "Cannot open \"" + fname + "\"") ) continue;

                for(std::string line; getline(f, line); ) {

                        TString str = line;
                        str.ToLower();
                        str.ReplaceAll(" ", "");

                        if(str.BeginsWith("#descriptor")) { // In case the descriptor line is starting like "# descriptor ? XXX/D:YYY/I:[...]"

                                TPrint::Debug(11, __METHOD_NAME__, "Descriptor tag line found in \"" + fname + "\"");
                                descriptor = line;
                                TPRegexp r1("#[ ]*descriptor[ =:%\\-/\\;$]*(.*)");
                                r1.Substitute(descriptor, "$1");

                                break;

                        } else if(str.BeginsWith("#")) { // In case the descriptor is like "# XXX/D:YYY/I:[...]"

                                descriptor = line;
                                descriptor.ReplaceAll(" ", "");
                                descriptor = descriptor(1, descriptor.Length()); // Remove the #

                                TPRegexp r1("\\w*\\/\\w:*");
                                if(descriptor.Index(r1) == 0) break;

                        } else {

                                descriptor = FetchTreeDescriptor(line);
                                if(!descriptor.EqualTo("")) break;
                        }
                }

                f.close();
        }

        if( !descriptor.EqualTo("") ) {
                TPrint::Debug(11, __METHOD_NAME__, "Descriptor \"" + descriptor + "\" found in \"" + fname + "\"");
        } else {
                TPrint::Debug(11, __METHOD_NAME__, "No descriptor found in \"" + fname + "\"");
        }


        descriptor.ReplaceAll("(", "{");
        descriptor.ReplaceAll(")", "}");
        return descriptor;
}

bool TFileReader::Init(TString origin)
{
        origin.ReplaceAll('\n', ' ');

        TString eMessage;
        if(!origin.EqualTo("")) {

                eMessage = (TString) "No origin found.. ";
                if( TPrint::ErrorIf(origin.EqualTo(""), this->GetMethodName(__METHOD_NAME__), eMessage) ) return false;

                if(HasPattern(origin) && !this->option.Contains("~")) {
                        TPrint::Debug(this->GetMethodName(__METHOD_NAME__), "Replacement char found, \"~\" option added..");
                        this->option += "~";
                }

                if(TPrint::IsDebugEnabled() && !this->option.Contains("v")) {
                        TPrint::Debug(10, this->GetMethodName(__METHOD_NAME__), "Debug mode enabled");
                        this->option += "v";
                }

                this->origin = origin;
                origin.ReplaceAll("(","");
                origin.ReplaceAll(")","");
        }

        if(this->origin.EqualTo("")) {
                eMessage = (TString) "No origin found.. ";
                if( TPrint::ErrorIf(origin.EqualTo(""), this->GetMethodName(__METHOD_NAME__), eMessage) ) return false;
        }

        this->Clear();

        if(this->option.Contains("~")) {

                TPrint::Debug(10, this->GetMethodName(__METHOD_NAME__), "TFileReader(\"" + (TString) this->GetName() + "\") with option \"" + this->option + "\" is ready..");
                return 1;
        }

        if(!this->option.Contains("c") && this->origin.Contains(";")) {

                TPrint::Debug(10, this->GetMethodName(__METHOD_NAME__), "TFileReader(\"" + (TString) this->GetName() + "\") cycle found in the origin.. c-option enabled");
                this->option += "c";
        }

        eMessage = (TString) "Loading origin \"" + this->origin + "\" with option \"" + this->option + "\". Please wait..";
        TPrint::Debug(10, this->GetMethodName(__METHOD_NAME__), eMessage);

        return this->Add(this->origin);
}

TString TFileReader::SubstituteIdentifier(TString pattern, TString identifier)
{
        std::vector<TString> vIdentifier;
                        vIdentifier.push_back(identifier);

        return SubstituteIdentifier(pattern, vIdentifier);
}

TString TFileReader::SubstituteIdentifier(TString pattern, std::vector<TString> identifier)
{
        if( TPrint::WarningIf(pattern.EqualTo(""), __METHOD_NAME__, "String used for substitution is empty..") ) return "";
        if( TPrint::WarningIf(identifier.size() == 0, __METHOD_NAME__, "Identifier std::vector is empty..") ) return "";

        TString globalIdentifier = identifier.size() > 0 ? identifier[0] : "";
        for( int i = 1, N = identifier.size(); i < N; i++)
                globalIdentifier += TFileReader::kSeparator + identifier[i];

        TString filename = pattern;
                filename.ReplaceAll("{}", globalIdentifier);

        for( int i = 0, N = identifier.size(); i < N; i++)
                filename.ReplaceAll("{"+TString::Itoa(i,10)+"}", identifier[i]);

        return filename;
}


bool TFileReader::Add(TString fqfn0) {

        TString eMessage = (TString) "Loading \"" + fqfn0 + "\".. Please wait..";
        TPrint::Debug(11, this->GetMethodName(__METHOD_NAME__), eMessage);

        // Look for spaces in the fqfn path
        TObjArray *fqfnArray = fqfn0.Tokenize(" ");
        for (int i = 0; i < fqfnArray->GetEntries(); i++) {

                TString fqfn = ((TObjString *)(fqfnArray->At(i)))->String();

                std::vector<TString> vi = ExpandWildcard(fqfn, this->option, this->maxDepth);
                for(int j = 0; j < vi.size(); j++) {

                        TString vij = vi[j];

                        // Dummy input..
                        if(vij.EqualTo("")) {

                                this->vTypes.push_back("f");
                                this->vFqfns.push_back("");
                                this->vClassNames.push_back("NULL");
                                continue;
                        }

                        TObjArray *array = vij.Tokenize(' ');
                        TString v_type = ((TObjString *) array->At(0))->String();
                        TString v_fqfn = ((TObjString *) array->At(1))->String();
                        TString v_className = ((TObjString *) array->At(2))->String();
                                v_className = v_className(1, v_className.Length()-3);
        
                        delete array;
                        array = NULL;

                        if(!className.EqualTo("")) {

                                eMessage = "\"" + v_fqfn + "\" is \""+v_className+"\", only \""+className+"\" are allowed.. skip";
                                if(TPrint::WarningIf(!TClass(v_className).InheritsFrom(className), this->GetMethodName(__METHOD_NAME__), eMessage)) continue;
                        }

                        this->vTypes.push_back(v_type);
                        this->vFqfns.push_back(v_fqfn);
                        this->vClassNames.push_back(v_className);
                }
        }

        delete fqfnArray;
        fqfnArray = NULL;

        return 1;
}

bool TFileReader::Add(int N)
{
        TString eMessage = (TString) "Loading " + TString::Itoa(N, 10)+" times the origin \""+this->origin+"\" using ID. Please wait..";
        TPrint::Debug(10, this->GetMethodName(__METHOD_NAME__), eMessage);

        eMessage = "Replacement char found, indexes between [0..N] will be used as ID";
        TPrint::WarningIf(this->HasPattern(), this->GetMethodName(__METHOD_NAME__), eMessage);

        for(int i = 0; i < N; i++) {

                TString fqfn = SubstituteIdentifier(this->origin, TString::Itoa(i,10));
                if(!Add(fqfn)) return 0;
        }

        return 1;
}

bool TFileReader::Add(std::vector<std::vector<TString>> identifiers)
{
        TString eMessage = (TString) "Loading origin \""+this->origin+"\" using identifiers. Please wait..";
        TPrint::Debug(10, this->GetMethodName(__METHOD_NAME__), eMessage);

        eMessage = "You are not supposed to use TFileReader::Add without \"~\" option";
        if(TPrint::ErrorIf(!this->option.Contains("~"), this->GetMethodName(__METHOD_NAME__), eMessage)) return 1;

        eMessage = "No origin path found.. abort";
        if( TPrint::ErrorIf(!this->HasOrigin(), this->GetMethodName(__METHOD_NAME__), eMessage) ) return 0;

        // Perhaps not useful.. TBC
        //eMessage = "No replacement char found and std::vector ID provided";
        //TPrint::Warning(!this->HasPattern(), this->GetMethodName(__METHOD_NAME__), eMessage);

        for(int i = 0; i < identifiers.size(); i++) {

                TString fqfn = SubstituteIdentifier(this->origin, identifiers[i]);
                if(!Add(fqfn)) return 0;
        }

        return 1;
}

bool TFileReader::Open(TString url, TFile* &f, Option_t* option, bool bReuse)
{
        TString opt = option;
        opt.ToLower();

        // Check if file already exists
        if(bReuse == TFileReader::kReuse) {

                TIter Next(gROOT->GetListOfFiles());
                while(( f = (TFile*) Next() )) {

                        TString fname = ApplyCorrectionUrl(f->GetName());
                        if( url.EqualTo(fname) && TString(option).Contains(TString(f->GetOption()))) {

                                TString eMessage = "File \"" + url + "\" already opened with the option \"" + option + "\".. It will just be linked.";
                                TPrint::Debug(20, __METHOD_NAME__, eMessage);

                                f->cd();
                                return true;
                        }
                }
        }

        // Check if too many files opened
        if(gROOT->GetListOfFiles()->GetSize() >= kOpenLimit) {

                TString eMessage = "File max limit reached, circular buffer enabled..";
                TPrint::WarningIf(!kOpenLimitTriggered, __METHOD_NAME__, eMessage);
                TFileReader::kOpenLimitTriggered = true;

                for(int i = 0; i < TFileReader::vFiles.size(); i++) {

                        if(TFileReader::vFiles[i] == NULL) continue;
                        TFileReader::vFiles[i]->Close();

                        TPrint::Debug(11, __METHOD_NAME__, (TString) "Circular buffer.. Deleting " + TFileReader::vFiles[i]->GetName());

                        delete TFileReader::vFiles[i];
                        TFileReader::vFiles.erase(TFileReader::vFiles.begin() + i);

                        break;
                }
        }

        //If no file already open.. standard procedure..
        TString eMessage = "\"" + url + "\" is not a ROOT file.. skip";
        if( TPrint::WarningIf(!IsRootFile(url), __METHOD_NAME__, eMessage) ) return false;
        if( (opt.Contains("UPDATE") || opt.Contains("NEW") || opt.Contains("CREATE")) && !TFileReader::DirName(url).EqualTo("")) {

                TPrint::Warning(__METHOD_NAME__, "Outgoing subdirectory does not exist.. Try to create it..");
                gSystem->Exec(Form("mkdir -p %s",TFileReader::DirName(url).Data()));
        }

        gErrorIgnoreLevel = kBreak;
        f = TFile::Open(url, option);
        gErrorIgnoreLevel = kPrint;

        // Check if file was properly instancied
        eMessage = "Failed to open \"" + url + "\" with option \"" + option + "\"";
        if( TPrint::ErrorIf(f == NULL, __METHOD_NAME__, eMessage) ) return false;

        // Check if file is zombie
        eMessage = "File " + url + " is zombie.. try to recover";
        if( TPrint::WarningIf(f->IsZombie(), __METHOD_NAME__, eMessage) ) {

                // Check if file can be recovered
                eMessage = "File " + url + " cannot be recovered..";
                if( TPrint::ErrorIf(!f->TestBit(TFile::kRecovered), __METHOD_NAME__, eMessage) ) {

                        delete f;
                        f = NULL;
                        return false;
                }
        }

        // Check if file is open
        eMessage = "File " + url + " instancied but cannot be opened..";
        if( TPrint::ErrorIf(!f->IsOpen(), __METHOD_NAME__, eMessage) ) {

                delete f;
                f = NULL;
                return false;
        }

        if(opt.EqualTo("new") || opt.EqualTo("create") || opt.EqualTo("recreate") || opt.EqualTo("update")) {

                eMessage = "File " + url + " cannot be written..";
                if( TPrint::ErrorIf(!f->IsWritable(), __METHOD_NAME__, eMessage)) {

                        delete f;
                        f = NULL;
                        return false;
                }
        }

        vFiles.push_back(f);
        return true;
}

bool TFileReader::WritePDF(TString filename, Option_t *option, TString title)
{
    //TODO
    UNUSED(filename);
    UNUSED(option);
    UNUSED(title);

    return 0;
}

bool TFileReader::WritePDF(TString filename, Option_t *option, TList *l)
{
        if(l == NULL) l = (TList*) gROOT->GetListOfCanvases();

        // Count canvases
        int nCanvas = 0;
        for(int i = 0; i < l->GetSize(); i++) {

                TCanvas *c = (TCanvas*) l->At(i);
                c->cd(); //Needed to set back gPad to the main canvas, not subpad

                TString eMessage = Form("One \"%s\" object has been found in the provided list.. skip", c->IsA()->GetName());
                if(TPrint::WarningIf(!l->At(i)->InheritsFrom("TCanvas"), __METHOD_NAME__, eMessage)) continue;
                else nCanvas++;
        }

        if(nCanvas == 0) {

                TPrint::Message(__METHOD_NAME__, "No canvas to turn to PDF file.");
                return 1;
        }

        TString opt = option;
        TString filename0;

        if(opt.EqualTo("OPEN") || opt.EqualTo("RECREATE") || !Isset(filename.Data())) filename0 = filename;
        else if(opt.EqualTo("UPDATE") || opt.EqualTo("REOPEN")) {

                filename0 = TString(
                        "/tmp/" + BaseName(filename, ".pdf") + "-" +
                        TString::Itoa((int) ROOT::IOPlus::Helpers::GetRandom(10000, ROOT::IOPlus::E_MICROTIMEPID),10) + ".pdf"
                        );
        }

        int iCanvas = 1;
        TPDF *pdf = new TPDF(filename0);
        pdf->On();

        for(int i = 0; i < l->GetSize(); i++) {

                if(l->At(i)->InheritsFrom("TCanvas")) {

                        TCanvas *c = (TCanvas*) l->At(i);
                        TString title = TString(c->GetName()) + "; " + TString(c->GetTitle());

                        pdf->NewPage();
                        pdf->SetTitle(title);

                        c->Modified();
                        c->Update();
                        TPrint::ProgressBar("Please wait while converting canvas to PDF.. ", "", iCanvas, nCanvas);
                        c->Paint();

                        TPrint::Debug(
                                10, __METHOD_NAME__,
                                "* Name:\"" + TString(c->GetName()) + "\"; (Title: \""+TString(c->GetTitle())+"\")");
                        iCanvas++;
                }
        }
        pdf->Close("EmbedFont");

        delete pdf;
        pdf = NULL;

        if(filename0 != filename) {

                TPrint::Message(__METHOD_NAME__, "An existing PDF file found here \""+filename+"\"..");
                TPrint::Message(__METHOD_NAME__, "Merging procedure on going !");

                TString filename_tmp = TString(
                        "/tmp/" + BaseName(filename, ".pdf") + "-" +
                        TString::Itoa((int) ROOT::IOPlus::Helpers::GetRandom(10000, ROOT::IOPlus::E_MICROTIMEPID),10) + "-tmp.pdf"
                        );

                TString command0 = Form(
                        "gs -dBATCH -dNOPAUSE -dDOPDFMARKS=true -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dEmbedAllFonts=true -sOutputFile=%s %s %s",
                        filename_tmp.Data(), filename.Data(), filename0.Data()
                        );

                TString command1 = Form(
                        "mv %s %s",
                        filename_tmp.Data(), filename.Data()
                        );

                TPrint::Debug(10, __METHOD_NAME__, command0);
                TPrint::Debug(10, __METHOD_NAME__, command1);

                TPrint::RedirectTTY(__METHOD_NAME__);
                std::cout << command0 << std::endl;
                if( system(command0) == -1) {

                        std::cerr << "[Error] System command was aborted.." << std::endl;
                        std::cerr << command0 << std::endl;
                }

                std::cout << command1 << std::endl;
                if( system(command1) == -1) {
                        std::cerr << "[Error] System command was aborted.." << std::endl;
                        std::cerr << command1 << std::endl;
                }

                TString err;
                TPrint::GetTTY(__METHOD_NAME__, NULL, &err);
                TPrint::TTY(__METHOD_NAME__, "", err);
        }

        TPrint::Message(
                __METHOD_NAME__,
                Form("%s%d canvas(es)%s dumped into \"%s\" (Option: `%s`)",
                     TPrint::kGreen.Data(),
                     gROOT->GetListOfCanvases()->GetSize(),
                     TPrint::kNoColor.Data(), filename.Data(),
                     opt.Data()
                     )
                );

        return 1;
}

TString TFileReader::Extension(TString fqfn) {

        TObjArray *fqfnArray = fqfn.Tokenize(".");
        if(fqfnArray->GetEntries() == 1 || fqfnArray->GetEntries() == 0) return "";

        return ((TObjString *)(fqfnArray->At(fqfnArray->GetEntries()-1)))->String();
}

TString TFileReader::StripExtension(TString fqfn) {

        TString out;

        TObjArray *fqfnArray = fqfn.Tokenize(".");
        if(fqfnArray->GetEntries() == 1 || fqfnArray->GetEntries() == 0) return fqfn;

        for (int i = 0; i < fqfnArray->GetEntries()-1; i++) {

                if(i != 0) out += ".";
                out += ((TObjString *)(fqfnArray->At(i)))->String();
        }

        return out;
}

bool TFileReader::IsEmpty(TString filename) {

        std::ifstream f(filename, std::ios::in);
        if(!f.is_open()) return false;

        TString str;
        if(f.good()) f >> str;
        if(str.EqualTo("")) return true;

        f.close();
        return false;
}

bool TFileReader::Touch(TString filename) {

        TPrint::Debug(10, __METHOD_NAME__, "Touch \"" + filename + "\"");

        std::ofstream fASCII(filename, std::ios::out | std::ios::app);
        if( TPrint::ErrorIf(!fASCII.is_open(), __METHOD_NAME__, "Failed to write into \"" + filename + "\"") ) return 0;

        fASCII.close();
        return 1;
}

bool TFileReader::RecursiveRmEmptyDir(TDirectory* dir) {

        TDirectory *gParentDirectory = gDirectory;

        if(dir == NULL) dir = gDirectory;
        dir->cd();

        bool b = true;
        TObject *obj = NULL;
        TIter Next(dir->GetList());
        while ((obj = (TObject*) Next())) {

                if (!obj->InheritsFrom("TDirectory")) b = false;
                else {

                        bool b0 = RecursiveRmEmptyDir((TDirectory*) obj);
                        if(b0) dir->rmdir(obj->GetName());
                        else b = false;
                }
        }

        gParentDirectory->cd();

        return b;
}

TDirectory* TFileReader::RecursiveMkdir(TString dirname, TString url, Option_t *opt) {

        TDirectory *gParentDirectory = gDirectory;

        TFile *f = NULL;
        if(!url.EqualTo("")) Open(url, f, opt);

        TDirectory *gDir = RecursiveMkdir(dirname);
        gParentDirectory->cd();

        return gDir;
}

TDirectory* TFileReader::RecursiveMkdir(TString dirname0, TDirectory* dir) {

        TDirectory *gParentDirectory = gDirectory;

        if(dir == NULL) dir = gDirectory;
        dir->cd();

        TString dirname = ObjRelativeDirName(dirname0);
        TObjArray *fqfnArray = dirname.Tokenize("/");

        for (int i = 0; i < fqfnArray->GetEntries(); i++) {

                TString subdirname = ((TObjString *)(fqfnArray->At(i)))->String();
                subdirname.ReplaceAll(kSeparator, "=");

                if( !gDirectory->Get(subdirname) )
                        gDirectory->mkdir(subdirname, "", true);

                gDirectory->cd(subdirname);
        }

        delete fqfnArray;
        fqfnArray = NULL;

        TDirectory *gFinal = gDirectory;
        gParentDirectory->cd();

        return gFinal;
}

TObject* TFileReader::OpenAndRead(TString fqfn, TFile*& f)
{
        TString url = Url(fqfn);
        TString obj = Obj(fqfn);

        Open(url, f, "READ", kReuse);
        return f->Get(obj);
}

bool TFileReader::Write(TObject* obj, TString fqfn, Option_t *opt)
{
        TString url = Url(fqfn);
        TString dirname = Obj(fqfn);

        TDirectory *gParentDirectory = gDirectory;

        TFile *f = NULL;
        Open(url, f, opt);

        // Check if file is opened (e.g. subdirectory missing)
        if(f == NULL) return false;

        // Create substructure
        RecursiveMkdir(dirname)->cd();

        // Write object
        if(TString(obj->GetName()).EqualTo("")) {

                TPrint::Error(__METHOD_NAME__, "Trying to write an unnamed object writing into \"%s\"", fqfn.Data());
                Close(f);
                gParentDirectory->cd();
                return false;
        }

        TString keypath = ApplyCorrection(url + ":/" + dirname + "/" + obj->GetName());
        TString eMessage = "Object with name \"" + keypath + "\" already exists";
        if(TPrint::WarningIf(gSystem->Getenv("NOUPDATE") && gDirectory->FindKey(obj->GetName()), __METHOD_NAME__, eMessage)) {

                Close(f);
                gParentDirectory->cd();
                return false;
        }
        
        TPrint::RedirectTTY(__METHOD_NAME__);
        if(!obj->InheritsFrom("TDirectory")) gDirectory->WriteTObject(obj);
        else CopyDirectory((TDirectory*) obj, gDirectory);
        TPrint::TTY(__METHOD_NAME__);
        
        Close(f);
        gParentDirectory->cd();
        return true;
}

bool TFileReader::CopyDirectory(TDirectory *dir1, TDirectory *dir2)
{
        if(dir1 == dir2) return true;

        TObject *obj = dir2->GetDirectory(dir1->GetName());
        if( !obj ) dir2->mkdir(dir1->GetName());
        //TObject *obj = dir2->Get(dir1->GetName());
        //if( !obj ) dir2->mkdir(dir1->GetName());
        //else {
        //
        //        TString eMessage = "An object named \"" + TString(obj->GetName()) + "\" of type \""+obj->IsA()->GetName()+"\" already exists in " + dir2->GetPath() + " and it is not a directory..";
        //        if(TPrint::Error(!obj->InheritsFrom("TDirectory"), __METHOD_NAME__, eMessage)) return false;
        //}

        TDirectory *gDestination = dir2->GetDirectory( TString(dir2->GetPath()) + "/" + dir1->GetName() );
        if(TPrint::ErrorIf(gDestination == NULL, __METHOD_NAME__, "Directory \"" + TString(dir1->GetName()) + "\" not found..")) return false;

        obj = NULL;
        TIter Next(dir1->GetList());
        while( (obj = (TObject*) Next()) ) {

                if(!obj->InheritsFrom("TDirectory")) gDestination->WriteTObject(obj);
                else CopyDirectory((TDirectory*) obj, gDestination);
        }

        return true;
}

TString TFileReader::GetMethodName(TString methodName)
{
        this->methodName = "TFileReader(" + TString(this->GetName()) + ")::Init";

        TObjArray *fqfnArray = this->methodName.Tokenize(kSeparator);
        methodName = ((TObjString *)(fqfnArray->At(fqfnArray->GetEntries()-1)))->String();

        delete fqfnArray;
        fqfnArray = NULL;

        return "TFileReader(" + TString(this->GetName()) + ")::" + methodName;
}

std::vector<TString> TFileReader::SplitString(const TString x, TString delimiter)
{
        std::vector<TString> vStr;

        TObjArray *tx = x.Tokenize(delimiter);
        for (int i = 0; i < tx->GetEntries(); i++) vStr.push_back(((TObjString *) tx->At(i))->String());

        delete tx;
        tx = NULL;

        return vStr;
}

void TFileReader::Clear()
{
        if((int) this->vFqfns.size() == 0) return;

        // Clear all vector
        TPrint::Debug(10, this->GetMethodName(__METHOD_NAME__), "Deleting all known pointers and clearing all stored vector..");
        this->vFqfns.clear();
        this->vTypes.clear();
        this->vClassNames.clear();

        // In case quiet mode is enabled.. it is at least showing the progress bar..
        if(TFileReader::Instance != 0) TPrint::Debug(10, __METHOD_NAME__, "Instance TFileReader #" + TString::Itoa(TFileReader::Instance, 10));
        else {

                TPrint::Debug(10, __METHOD_NAME__, "Instance TFileReader #" + TString::Itoa(TFileReader::Instance,10) + " : " + TString::Itoa(TFileReader::vFiles.size(),10) + " file(s) to close");
                for(int i = 0; i < TFileReader::vFiles.size(); i++) {

                        if(!TPrint::IsSuperQuiet()) TPrint::ProgressBar(TPrint::kPurple + "Closing files in memory.. " + TPrint::kNoColor, "", i+1, TFileReader::vFiles.size(), 16);

                        if(TFileReader::vFiles[i] == NULL) continue;
                        TFileReader::vFiles[i]->Close();

                        delete TFileReader::vFiles[i];
                        TFileReader::vFiles[i] = NULL;
                }
                TFileReader::vFiles.clear();
        }
}

std::vector<TString> TFileReader::ExpandWildcard(TString fqfn, Option_t* option, int maxDepth)
{
        TString opt = option;
        opt.ToLower();

        std::vector<TString> v;
        TString url = Url(fqfn);
        TString obj = Obj(fqfn);
        TPrint::Debug(10, __METHOD_NAME__, "Expanding wildcard.. \"" + fqfn + "\"");
        if(TPrint::ErrorIf(HasPattern(fqfn), __METHOD_NAME__, "Replacement char found, please replace {}.. abort")) {
                return {};
        }

        // Expand wildcard in filenames
        std::vector<TString> vUrl = ExpandTextFile({url}, option, maxDepth);
        if(TPrint::IsDebugEnabled() && !TFileReader::IsTextFile(url)) {

                for(int i = 0; i < vUrl.size(); i++) {

                        TString eMessage = "Input \"" + vUrl[i] + "\" found in text file \"" + url + "\"";
                        if(vUrl[i].EqualTo("")) eMessage = "Dummy input added and used as offset..";
                        TPrint::Debug(30, __METHOD_NAME__, eMessage);
                }
        }

        if(vUrl.size() == 0 && !opt.Contains("~")) {

                TString eMessage = "No file matching with \"" + fqfn + "\"";
                TPrint::Error(__METHOD_NAME__, eMessage);
        }

        if(!opt.Contains("c") && obj.Contains(";")) {

                TPrint::Debug(10, __METHOD_NAME__,  "Cycle found in the object provided.. c-option enabled");
                opt += "c";
        }

        TPrint::Debug(10, __METHOD_NAME__, "Expanding object list in \"" + fqfn + "\"");

        int N = vUrl.size();
        for(int i = 0; i < N; i++) {

                if(!TPrint::IsSuperQuiet()) {
                        TPrint::SetSingleCarriageReturn();
                        TPrint::ProgressBar("Expanding wildcard.. \"" + fqfn + "\" ", "", i+1, N, 64);
                }

                if(vUrl[i].EqualTo("")) {
                        v.push_back("");
                        continue;
                }

                TString filename0 = FileName(vUrl[i]);
                TString obj0 = Obj(vUrl[i]);
                if(obj0.EqualTo("/")) obj0 = obj;

                // Expand wildcard in files
                std::vector<TString> v2 = ExpandWildcardObj(filename0, obj0, opt, maxDepth);

                TString eMessage = "No object found in \""+filename0+"\", matching with \"" + obj0 + "\"";
                if(v2.size() == 0) TPrint::Debug(30, __METHOD_NAME__, eMessage);

                v.insert(v.end(), v2.begin(), v2.end());
        }

        return v;
}

std::vector<TString> TFileReader::ExpandTextFile(std::vector<TString> vTextFile, Option_t* option, int maxDepth)
{
        std::vector<TString> vUrl;
        TString opt = option;

        vTextFile = ExpandWildcardUrl(vTextFile, option);
        for(int i = 0; i < vTextFile.size(); i++) {

                if(IsRootFile(vTextFile[i])) {

                        std::vector<TString> vTemp = ExpandWildcardUrl(vTextFile[i], option);
                        vUrl.insert( vUrl.end(), vTemp.begin(), vTemp.end());
                        continue;

                } else if(IsTextFile(vTextFile[i]) && opt.Contains("t")) {

                        std::vector<TString> vTemp = ExpandWildcardUrl(vTextFile[i], option);
                        vUrl.insert( vUrl.end(), vTemp.begin(), vTemp.end());
                        continue;
                }

                std::ifstream ifs(vTextFile[i].Data(), std::ifstream::in); // TODO: Add a check if a directory is specified..
                TString eMessage = "Failed to open text file \"" + vTextFile[i] + "\".. skip";
                if( TPrint::WarningIf(!ifs, __METHOD_NAME__, eMessage) ) return vUrl;

                bool bBreak = false;
                std::vector<TString> vUrlBuffer;
                while(ifs.good()) {

                        std::string line;
                        getline(ifs, line);

                        TString file = line;
                        if(file.EqualTo("")) {  // Add space in the list.. this will be loaded as NULL

                                vUrlBuffer.push_back("");

                        } else if(opt.Contains("0")) {

                                // Zero option to just read the text file whatever is inside..
                                vUrlBuffer.push_back(line);

                        } else if(IsTextFile(file)) {

                                std::vector<TString> vTemp = ExpandWildcardUrl(file, option);
                                if(!opt.Contains("t")) vTemp = ExpandTextFile(vTemp, option, maxDepth-1);

                                vUrlBuffer.insert( vUrlBuffer.end(), vTemp.begin(), vTemp.end());

                        } else if(IsRootFile(file)) {

                                std::vector<TString> vTemp = ExpandWildcardUrl(file, option);
                                vUrlBuffer.insert( vUrlBuffer.end(), vTemp.begin(), vTemp.end());

                        } else {

                                // If this is not just a list of file.. then just include filename
                                TPrint::Warning(__METHOD_NAME__, "This text file %s, seems to contains other elements than just ROOT file list.. Skip..", vTextFile[i].Data());
                                bBreak = true;
                                break;
                        }
                }

                // Remove the last empty line before \0
                if(vUrlBuffer.size() && vUrlBuffer.back().EqualTo("")) vUrlBuffer.pop_back();

                // Insert all elements found..
                if(!bBreak) vUrl.insert( vUrl.end(), vUrlBuffer.begin(), vUrlBuffer.end());
                ifs.close();
        }

        return vUrl;
}

TString TFileReader::RelativePath(TString abs, TString ref) {

    TString rel = "";

    abs = gSystem->UnixPathName(abs);
    if(!abs.BeginsWith("/")) abs = TString(gSystem->WorkingDirectory()) + "/" + abs;
    std::vector<TString> array_abs = SplitString(abs, '/');

    if(ref == "") ref = gSystem->pwd();
    std::vector<TString> array_ref  = SplitString(ref, '/');

    int i0 = 0;
    for(int i = 0; i < TMath::Min(array_abs.size(),array_ref.size()); i++) {
        if(array_abs[i] != array_ref[i]) break;
        i0 = i+1;
    }

    // Do not print the relative path, if longer than the absolute one..
    if(array_abs.size() <= array_ref.size()) return abs;

    // Write relative path
    for(int i = i0; i < array_ref.size(); i++)
        rel += "../";

    for(int i = i0; i < array_abs.size(); i++) {
        rel += array_abs[i] + "/";
    }

    return rel(0, rel.Length()-1);
}

std::vector<TString> TFileReader::ExpandWildcardUrl(std::vector<TString> vUrl, Option_t* option)
{
        std::vector<TString> vFinalUrl;
        for(int i = 0; i < vUrl.size(); i++) {

                std::vector<TString> vNewUrl = ExpandWildcardUrl(vUrl[i], option);
                vFinalUrl.insert( vFinalUrl.end(), vNewUrl.begin(), vNewUrl.end() );
        }

        return vFinalUrl;
}

std::vector<TString> TFileReader::ExpandWildcardUrl(TString url, Option_t* option)
{
        TString opt = option;
        opt.ToLower();

        url.ReplaceAll("(", "");
        url.ReplaceAll(")", "");
        url.ReplaceAll(".*", "*");

        if(TPrint::ErrorIf(HasPattern(url), __METHOD_NAME__, "Replacement char found, please replace {}.. abort")) {
                return {};
        }

        std::vector<TString> v;
        std::vector<TString> vFinalUrl;
        std::vector<TString> vUrl = ROOT::IOPlus::Helpers::Explode(' ', url);

        for(int i = 0; i < vUrl.size(); i++) {

                TString filename0 = FileName(vUrl[i]);
                TString obj0 = Obj(vUrl[i]);
               
                if(!obj0.EqualTo("/")) obj0 = kSeparator + obj0;
                else obj0 = "";

                TString eMessage = "Processing file \"" + TString(vUrl[i]) + "\"";
                TPrint::Debug(30, __METHOD_NAME__, eMessage);

                TString directory;
                int slashpos = filename0.Last('/');
                if (slashpos>=0) {

                        directory = filename0(0,slashpos); // Copy the directory name
                        filename0.Remove(0,slashpos+1); // and remove it from url

                } else directory = gSystem->UnixPathName(gSystem->WorkingDirectory());

                const char* epath = gSystem->ExpandPathName(directory.Data());
                TString epath_str = epath;
                delete[] epath;

                void *dir = gSystem->OpenDirectory(epath_str);
                eMessage = "Directory \"" + directory + "\" not found ("+TPrint::MakeItShorter(vUrl[i],30,TPrint::kNoColor)+")"; //, while processing \"" + epath_str + "/" + TString(vUrl[i]) + "\"";
                if( TPrint::ErrorIf(!dir, __METHOD_NAME__, eMessage) ) {

                        gSystem->FreeDirectory(dir);
                        continue;
                }
                if(vUrl[i].EqualTo("")) {

                        gSystem->FreeDirectory(dir);
                        continue;
                }

                // Regex trick because "*"" should be replaced, but not patterns like [0-9]*
                int index = 0;
                Ssiz_t length = 0;

                TString rgx(filename0.Prepend("^").Append("$").Data());
                while((index = rgx.Index("\\*", &length, index)) != -1) {

                        if(index == 0) {

                                rgx = "." + rgx;
                                index++;

                        } else if(rgx[index-1] != ']') {

                                rgx = rgx(0,index) + ".*" + rgx(index+1, rgx.Length());
                                index++;
                        }

                        index++;
                }

                TRegexp regex(rgx);

                //create a TList to store the file names (not yet sorted)
                TList l;

                // Loop over content of each directory..
                const char* file;
                while ((file = gSystem->GetDirEntry(dir))) {

                        if (!strcmp(file,".") || !strcmp(file,"..")) continue;

                        TString s = file;
                        if ( (url != file) && s.Index(regex) == kNPOS) continue;

                        eMessage = "File \"" + TString(file) + "\" has been accepted using \"" + TString(rgx) + "\"";
                        TPrint::Debug(30, __METHOD_NAME__, eMessage);

                        l.Add(new TObjString(epath_str + "/" + file + obj0));
                }

                eMessage = "File \"" + TString(/*epath_str + "/" + */ vUrl[i]) + "\" is removed due to comment tag..";
              
                if(TString(vUrl[i].Strip(TString::kBoth)).BeginsWith("#")) TPrint::Debug(1, __METHOD_NAME__, eMessage);
                else {
                        eMessage = "File \"" + TString(/*epath_str + "/" + */ vUrl[i]) + "\" not found.."; //, while processing \"" + epath_str + "/" + TString(vUrl[i]) + "\"";
                        if(l.GetEntries() == 0 && !opt.Contains("~")) TPrint::Error(__METHOD_NAME__, eMessage);
                }
                
                gSystem->FreeDirectory(dir);

                //sort the files in alphanumeric order
                l.Sort();
                TIter Next(&l);
                TObjString *obj;

                while ((obj = (TObjString*) Next())) {

                        file = obj->GetName();
                        v.push_back(TString(file));
                }

                l.Delete();
        }

        if(v.size() == 0 && opt.Contains("~")) {

                TString eMessage = "\"~\" option detected.. And no file found.. Origin added in the std::vector";
                TPrint::Debug(10, __METHOD_NAME__, eMessage);
                v.push_back(url);

        } else if (vUrl.size() == 0) {

                TString eMessage;
                if(IsRootFile(url)) eMessage = "No file named \"" + url + "\" found..";
                else eMessage = "No valid file found behind the following path.. \"" + url + "\"";
                if( TPrint::WarningIf(!v.size(), __METHOD_NAME__, eMessage) ) return v;
        }

        return v;
}

std::vector<TString> TFileReader::ExpandWildcardObj(TString url, TString obj, Option_t* option, int maxDepth)
{
        TString eMessage;
        TString obj_tmp = ApplyCorrectionObj(obj);
        obj = obj_tmp;

        if(TPrint::ErrorIf(HasPattern(url), __METHOD_NAME__, "Replacement char found in url, please remove brackets \"{}\".. abort")) {
                return {};
        }
        if(TPrint::ErrorIf(HasPattern(obj), __METHOD_NAME__, "Replacement char found in obj, please remove brackets \"{}\".. abort")) {
                return {};
        }

        TString obj0 = obj;

        obj.ReplaceAll("+", "\\+");
        obj.ReplaceAll("=", "\\=");
        obj.ReplaceAll("&", "\\&");
        //obj.ReplaceAll("^", "\\^");
        obj.ReplaceAll("!", "\\!");
        //obj.ReplaceAll("$", "\\$");
        obj.ReplaceAll("<", "\\<");
        obj.ReplaceAll(">", "\\>");
        obj.ReplaceAll("(", "");
        obj.ReplaceAll(")", "");
        obj.ReplaceAll("[", "\\[");
        obj.ReplaceAll("]", "\\]");
        obj.ReplaceAll("*", "[^/\\[\\]]*");
        obj.ReplaceAll(".*", "[^/\\[\\]]*");
        obj.ReplaceAll("?", "[^/\\[\\]]?");

        // List content of the rootfile
        while(obj_tmp.MaybeWildcard() || obj_tmp.Contains("^") || obj_tmp.Contains("$") ) {

                obj_tmp = ObjDirName(obj_tmp);
                if(maxDepth >= 0) maxDepth++;
        }

        TString opt = option;
        opt.ToLower();

        if( opt.Contains("t") ) {

                TString eMessage = "No object expansion expected.. t-option has been used. (Should only keep text files !)";
                TPrint::Debug(11, __METHOD_NAME__, eMessage);

                std::vector<TString> v(1);
                v[0] = "t " + url + " fstream";
                return v;
        }

        if(!opt.Contains("c") && obj.Contains(";")) {

                TPrint::Debug(10, __METHOD_NAME__, "Cycle found in the provided obj.. c-option enabled");
                opt += "c";
        }

        std::vector<TString> v    = LsRoot(url + kSeparator + obj_tmp, option, maxDepth);

        TString obj_dirname  = ObjDirName(obj);
        TString obj_basename = ObjBaseName(obj);
        int obj_cycle    = ObjCycle(obj);

        //    bool first       = true;
        //    bool firstIsFile = false;
        TString fqfn = ApplyCorrection(url + kSeparator + obj0);
        for(int i = 0; i < v.size(); i++) {

                TObjArray *array = v[i].Tokenize(' ');
                TString vi_type = ((TObjString *)array->At(0))->String();
                TString vi_fqfn = ((TObjString *)array->At(1))->String();
                TString vi_className = ((TObjString *)array->At(2))->String();

                delete array;
                array = NULL;

                TString vi_url = Url(vi_fqfn);
                TString vi_obj = Obj(vi_fqfn);
                int vi_cycle = ObjCycle(vi_fqfn);


                if(vi_obj.EqualTo(obj0) && !vi_className.EqualTo("(TDirectoryFile*)")) return {v[i]};

                // Prepare the regex std::string, including exception when no object
                TString regex_str  = (obj.EqualTo("/")) ? ".*" : TString("^" + obj_dirname);
                if(!obj_basename.EqualTo("")) {

                        if(obj_basename.First('/') != 0) regex_str += "/";
                        regex_str += obj_basename;

                        if(obj_cycle) regex_str += ";" + TString::Itoa(obj_cycle,10) + "$";
                        else if (vi_cycle) regex_str += ";[0-9]*$";
                }

                // Remove object which doesn't match with wildcard
                TRegexp regex(regex_str);
                vi_obj = Obj(vi_fqfn)(regex);

                eMessage  = "Object \"" + Obj(vi_fqfn) + "\"\n";
                eMessage += "+ Regex \"" + regex_str + "\"";
                eMessage += " => " + ((!vi_obj.EqualTo("")) ? TString("is VALID") : TString("will be REMOVED"));
                TPrint::Debug(30, __METHOD_NAME__, eMessage);

                if(vi_obj.EqualTo("")) {

                        v.erase(v.begin() + i);
                        i--;
                }

        }

        return v;
}

bool TFileReader::CopyTo(std::vector<TObject*> vObject, std::vector<TString> vPath, TDirectory* dir)
{
        if(vObject.size() != vPath.size()) {

                TPrint::Warning(__METHOD_NAME__, "Number of object is not matching with provided path list.. cannot copy into \"%s\"", dir->GetName());
                return 0;
        }

        TDirectory *gParentDirectory = gDirectory;
        for(int i = 0, N= vObject.size(); i < N; i++) {

                if(vObject[i] == NULL) continue;

                TDirectory *gDir = RecursiveMkdir(vPath[i], dir);
                gDir->cd();

                TObject *obj = vObject[i]->Clone();
                if (!gDir->GetList()->FindObject( obj ) )
                        gDir->Append(obj);

                gParentDirectory->cd();
        }

        return 1;
}

bool TFileReader::CopyTo(std::vector<TString> vStr, TDirectory* dir)
{
        std::vector<TString> vPath;
        std::vector<TObject*> vObject;
        for(int i = 0, N = vStr.size(); i < N; i++) {

                vPath.push_back(Obj(ObjDirName(vStr[i])));
                vObject.push_back(ReadObj(vStr[i]));
        }

        bool b = CopyTo(vObject, vPath, dir);
        for(int i = 0, N = vObject.size(); i < N; i++)
                delete vObject[i];

        vPath.clear();
        vObject.clear();

        return b;
}

std::vector<TString> TFileReader::LsPath(TDirectory* dir)
{
        std::vector<TString> v;
        TDirectory *gParentDirectory = gDirectory;

        TKey *key = NULL;
        TIter Next(dir->GetListOfKeys());
        while ((key = (TKey*) Next())) {

                if (!key->InheritsFrom("TDirectory")) v.push_back(Obj(gDirectory->GetPath()));
                else {

                        dir->cd(key->GetName());
                        std::vector<TString> subv = LsPath((TDirectory*) key->ReadObj());
                        v.insert(v.end(), subv.begin(), subv.end());
                        dir->cd();
                }
        }

        gParentDirectory->cd();
        return v;
}

std::vector<TObject*> TFileReader::Get(std::vector<TObject*> vObject, TString str)
{
        std::vector<TObject*> v;
        for(int i = 0, N = vObject.size(); i < N; i++) {

                if(vObject[i] == NULL) continue;
                TPrint::Debug(10, __METHOD_NAME__, "%s != %s", vObject[i]->GetName(), str.Data());
                if(TString(vObject[i]->GetName()).Contains(str)) v.push_back(vObject[i]);
        }

        return v;
}

std::vector<TObject*> TFileReader::LsDirectory(TDirectory* dir)
{
        std::vector<TObject*> v;

        TDirectory *gParentDirectory = gDirectory;

        TObject *obj = NULL;
        TIter Next(dir->GetList());
        while ((obj = (TObject*) Next())) {

                if (!obj->InheritsFrom("TDirectory")) v.push_back(obj);
                else {

                        dir->cd(obj->GetName());
                        std::vector<TObject*> subv = LsDirectory((TDirectory*) obj);
                        v.insert(v.end(), subv.begin(), subv.end());
                        dir->cd();
                }
        }

        gParentDirectory->cd();
        return v;
}

std::vector<TString> TFileReader::LsRoot(TString fqfn, Option_t* option, int depth)
{
        TString opt = option;
        opt.ToLower();

        TString url = Url(fqfn);
        TString obj = ApplyCorrectionObj(Obj(fqfn));

        if(obj.Contains(";")) opt += "c";

        TString obj_dirname = ObjDirName(obj);
        TString obj_basename = ObjBaseName(obj);
        int obj_cycle = ObjCycle(obj);

        TDirectory *gParentDirectory = NULL;
        gParentDirectory = gDirectory;

        TString eMessage = "Read \"" + fqfn + "\"\nwith the option \"" + option + "\"";
        TPrint::Debug(30, __METHOD_NAME__, eMessage);

        TFile *f = NULL;
        TKey *key = NULL;
        if( !Open(url, f, "READ", TFileReader::kReuse) ) return {};
        if(!obj_dirname.EqualTo("")) {
                gErrorIgnoreLevel = kBreak;
                gDirectory->cd(obj_dirname);
                gErrorIgnoreLevel = kPrint;
        }

        TList *list = NULL;
        TList *list0 = gDirectory->GetListOfKeys();
        if(obj_basename.EqualTo("")) list = list0;
        else {
                list = new TList;
                TIter Next(list0);
                while( (key = (TKey*) Next()) ) {

                        if(key == NULL) continue;

                        if(!obj_basename.EqualTo(key->GetName())) continue;

                        int obj0_cycle = key->GetCycle();
                        int last_cycle = GetLastCycle(key->GetName(), list0);

                        if (obj_cycle != 0) {
                                if(obj0_cycle == obj_cycle) list->Add(key);
                        } else if(opt.Contains("c")) {
                                list->Add(key);
                        } else if(obj0_cycle == last_cycle) {
                                list->Add(key);
                        }
                }

                obj = ObjDirName(obj);
        }

        TDirectory *gDir0 = gDirectory;
        gParentDirectory->cd();

        return LsRoot(gDir0, opt, depth, list); // list is deleted inside LsRoot(TDirectory*,...)
}

std::vector<TString> TFileReader::LsRoot(TDirectory *gDirectoryToList, Option_t* option, int depth, TList *list)
{
        TString opt = option;
        opt.ToLower();

        TString eMessage;
        TKey* key = NULL;

        TDirectory *gParentDirectory = NULL;
        if(list == NULL) {

                gParentDirectory = gDirectory;
                eMessage = "Start listing \"" + TString(gDirectoryToList->GetPath()) + "\"\n and maxDepth=" + TString::Itoa(depth,10);
                TPrint::Debug(30, __METHOD_NAME__, eMessage);

                list = gDirectoryToList->GetListOfKeys();

        } else {

                eMessage = "Looking into \"" + TString(gDirectoryToList->GetPath()) + "\" with current depth=" + TString::Itoa(depth, 10);
                TPrint::Debug(30, __METHOD_NAME__, eMessage);
        }

        std::vector<TString> v;
        TIter Next(list);
        TString oldName = "";
        int cycle = 1;

        TString obj = Obj(gDirectory->GetPath());

        // First print all objects, except TDirectory type
        while ((key = (TKey*) Next())) {

                if(oldName.CompareTo(key->GetName()) != 0) cycle = 1;

                int last_cycle = GetLastCycle(key->GetName(), list);
                if(!opt.Contains("c") && key->GetCycle() != last_cycle) continue;

                TString className = key->GetClassName();
                TClass *cl = gROOT->GetClass(className);
                if (!cl->InheritsFrom("TDirectory") ) {

                        TString newfqfn = KeyPath(key);
                        if(opt.Contains("c")) newfqfn += ";" + TString::Itoa(key->GetCycle(), 10);

                        if(!opt.Contains("f") && !opt.Contains("d")) v.push_back("f " + newfqfn + " (" + className + "*)");
                        else if(opt.Contains("f")) v.push_back("f " + newfqfn + " (" + className + "*)");

                        eMessage = "f " + newfqfn + " (" + className + "*)";
                        TPrint::Debug(20, __METHOD_NAME__, eMessage);
                }

                oldName = key->GetName();
                cycle++;
        }

        Next.Reset();
        while ((key = (TKey*) Next())) {

                TString className = key->GetClassName();
                TClass *cl = gROOT->GetClass(className);

                if (cl->InheritsFrom("TDirectory") ) {

                        TDirectory *dir = (TDirectory*) key->ReadObj();
                        if(dir != NULL) {
                                gErrorIgnoreLevel = kBreak;
                                gDirectory->cd(dir->GetName());
                                gErrorIgnoreLevel = kPrint;
                        }

                        TString newfqfn = KeyPath(key);
                        if((!opt.Contains("f") && !opt.Contains("d")) || depth == 0) v.push_back("d " + newfqfn + " (" + className + "*)");
                        else if(opt.Contains("d")) v.push_back("d " + newfqfn + " (" + className + "*)");

                        if(depth == 0) {

                                eMessage = "Max depth reached.. stop looking into subdirectories !";
                                TPrint::Debug(11, __METHOD_NAME__, eMessage);

                                gDirectory->cd("..");
                                continue;
                        }

                        eMessage = "d " + newfqfn + " (" + className + "*)";
                        TPrint::Debug(20, __METHOD_NAME__, eMessage);

                        if(dir->GetListOfKeys() == NULL) {

                                gDirectory->cd("..");
                                continue;
                        }

                        std::vector<TString> v2 = LsRoot(dir, option, depth-1, dir->GetListOfKeys());
                        gDirectory->cd("..");

                        v.insert( v.end(), v2.begin(), v2.end() );
                        v2.clear();
                }
        }

        if(TString(list->GetName()).EqualTo("LsRoot")) {

                delete list;
                list = NULL;
        }

        //Close(f); // File can be reuse.. so no need to delete it.. it will be deleted at the end..

        if(gParentDirectory) gParentDirectory->cd();
        return v;
}

int TFileReader::ObjCycle(TString objname)
{
        int dotcoma = objname.Last(';');
        if(dotcoma == -1) return 0;

        objname = objname(dotcoma+1, objname.Length() - dotcoma+1);
        int cycle = 0;

        if(!objname.EqualTo("")) cycle = objname.Atoi();
        return cycle;
}

int TFileReader::GetLastCycle(TString name, TList* l)
{
        TIter Next(l);
        TKey *key = NULL;

        int cycle = 0;
        while ((key = (TKey*) Next())) {

                if(TString(key->GetName()).EqualTo(name)) {

                        if(cycle < key->GetCycle()) cycle = key->GetCycle();
                }
        }

        return cycle; // return 0 means not key found..
}

std::vector<TString> TFileReader::Browse(TDirectory *dir)
{
        // First print all objects, except TDirectory type
        if(dir == NULL) return {};

        std::vector<TString> v;
        TList *l = dir->GetList();
        TIter Next(l);

        TObject *obj = NULL;
        while ((obj = (TObject*) Next())) {

                if (!obj->IsA()->InheritsFrom("TDirectory") ) {

                        TString path = ApplyCorrectionObj((TString) dir->GetPath() + "/" + obj->GetName());
                        v.push_back(path);

                } else {

                        TDirectory *subdir = (TDirectory*) obj;
                        std::vector<TString> v2 = Browse(subdir);

                        v.insert( v.end(), v2.begin(), v2.end() );
                        v2.clear();
                }
        }

        return v;
}

bool TFileReader::Isset(TString fqfn, TString obj)
{
        if(!IsRootFile(fqfn)) {

                std::ifstream f(fqfn);
                return f.good();
        }

        TString url = Url(fqfn);
        if(IsTextFile(url)) {

                std::ofstream f(url, std::ios::out);
                if(f.good()) {

                        f.close();
                        return 1;
                }

                f.close();
                return 0;
        }

        obj = Obj(fqfn) + "/" + obj;
        obj = ApplyCorrectionObj(obj);
        TString obj_basename = ObjBaseName(obj);
        TString obj_dirname = ObjDirName(obj);

        gErrorIgnoreLevel = kBreak;
        TFile *f = TFile::Open(url, "READ");
        if(f == NULL) return 0;

        if( !gDirectory->cd(obj_dirname) ) return 0;

        gErrorIgnoreLevel = kPrint;

        // If no obj specified but, the file exists
        bool bIsset;
        if(obj_basename == "") bIsset = true;
        else {

                TKey *key = gDirectory->FindKey(obj_basename);
                bIsset = (key != NULL);
        }

        if(f) f->Close();

        return bIsset;
}

TString TFileReader::Domain(TString fqfn)
{
        TString url = Url(fqfn);
        url = ApplyCorrectionUrl(url);

        TUrl u(url);
        TString protocol = u.GetProtocol();
        TString hostname = u.GetHost();

        TString domain;
        if(!protocol.EqualTo("")) domain += protocol + "://" + hostname;
        else domain += hostname;

        return domain;
}

TString TFileReader::Url(TString fqfn)
{
        // This line has been commented because when passing a wildcard expandpathname is failing.. e.g.$PWD/Full-(.*).txt
        // To be propagated to other functions if problematic...
        //gSystem->ExpandPathName(fqfn);     // Interprete environment variables:
        TString url = fqfn;

        int space = url.Last(' ');
        if(space != -1) url = url(space+1, url.Length() - space+1);

        TPRegexp regex("^\\w*://");
        Ssiz_t regex_length;

        int dotslashslashpos = url.Index(regex, &regex_length); // To skip protocol ://
        if(dotslashslashpos == -1) {
                url = "file://" + url;
                dotslashslashpos = url.Index(regex, &regex_length);
        }
        int dotdotpos;
        dotdotpos = url.Index(kSeparator, dotslashslashpos + regex_length);

        int slash = url.First('/');
        if(dotslashslashpos - slash + 1 > 0 && dotslashslashpos != -1) url = url.Remove(dotslashslashpos); // If a / is found before :// => Double slash in the object name
        else if (dotdotpos != dotslashslashpos && dotdotpos != -1) url = url.Remove(dotdotpos);

        return url;
}

TString TFileReader::FileName(TString fqfn)
{
        TString url = Url(fqfn);
        url = ApplyCorrectionUrl(url);

        return TUrl(url).GetFile();
}

TString TFileReader::Obj(TString fqfn)
{
        //gSystem->ExpandPathName(fqfn);     // Interprete environment variables
        TString obj = "";

        int space = fqfn.Last(' ');
        if(space != -1) fqfn = fqfn(space+1, fqfn.Length() - space+1);

        TPRegexp regex("^\\w*://");
        Ssiz_t regex_length;
        int dotslashslashpos = fqfn.Index(regex, &regex_length); // To skip protocol ://

        int dotdotpos;
        if(dotslashslashpos != -1) dotdotpos = fqfn.Index(kSeparator, dotslashslashpos + regex_length);
        else dotdotpos = fqfn.Index(kSeparator);

        int slash = fqfn.First("/");

        if(dotslashslashpos - slash + 1 > 0 && dotslashslashpos != -1) obj = fqfn(dotslashslashpos + 1, fqfn.Length() - dotslashslashpos + 1);
        if (dotdotpos != dotslashslashpos && dotdotpos != -1) obj = fqfn(dotdotpos + 1,fqfn.Length() - dotdotpos + 1);

        // Remove absolute path
        while(obj.Index("/") == 0 || obj.Index(".") == 0) obj = obj(1, obj.Length());

        // Trim std::string
        obj.ReplaceAll("^[ ]*", "");
        obj.ReplaceAll("[ ]*$", "");

        return "/" + obj;
}

TString TFileReader::BaseName(TString fqfn, TString extension)
{
        TString filename = FileName(fqfn);
        TString basename = gSystem->BaseName(filename);
        if (basename.EqualTo(".") || basename.EqualTo("/")) basename = "";

        if(extension.EqualTo(".*")) extension = "\\..*";
        TPRegexp r(extension + "$");
        int index = basename.Index(r);
        if(index != -1 && !extension.EqualTo("")) basename.Remove(index);

        return basename;
}



TString TFileReader::DirName(TString fqfn, int depth)
{
        TString filename = FileName(fqfn);
        
        std::vector<TString> vDirname = ROOT::IOPlus::Helpers::Explode("/", filename);
        vDirname.pop_back();

        for(int i = 1, N = vDirname.size(); i < N; i++) {

                if(vDirname[i].EqualTo("")) {

                        vDirname.erase(vDirname.begin() + i);
                        i--;
                }
        }

        while(depth > 1) vDirname.pop_back();

        TString dir = ROOT::IOPlus::Helpers::Implode("/", vDirname);
        return dir.EqualTo("") ? "." : dir;
}

TString TFileReader::ObjBaseName(TString obj)
{
        TString basename = gSystem->BaseName(obj);

        int dotcoma = basename.Index(";");
        if (dotcoma != -1) basename = basename(0, dotcoma);
        if (basename.EqualTo(".") || basename.EqualTo("/")) basename = "";

        return basename;
}

TString TFileReader::ObjDirName(TString obj, int depth)
{
        TString dirname = gSystem->DirName(obj);
        if (dirname.EqualTo(".") || dirname.EqualTo("/")) dirname = "";

        // In case a depth is specified..
        if(depth != -1) {

                TObjArray *fqfnArray = dirname.Tokenize("/");
                for (int i = 0; i < fqfnArray->GetEntries(); i++) {

                        if(depth == i) dirname = ((TObjString *)(fqfnArray->At(i)))->String();
                }

                if(dirname.EqualTo("")) return "/";
        }

        return dirname;
}

TString TFileReader::ObjRelativeDirName(TString os)
{
        while(os.Index("/") == 0 || os.Index(".") == 0) os = os(1, os.Length());

        return os;
}

bool TFileReader::IsRootFile(TString fqfn)
{
        return fqfn.EndsWith(".root");
}

bool TFileReader::IsXmlFile(TString fqfn)
{
        fqfn = ROOT::IOPlus::Helpers::Explode(kSeparator, fqfn)[0];
        return fqfn.EndsWith(".xml") || fqfn.EndsWith(".xml.gz");
}

bool TFileReader::Exists(TString fqfn, TString obj_cmpl)
{
        if(fqfn.EqualTo("")) return false;

        TString fileName = FileName(fqfn);    
        TString objName = Obj(fqfn);
    
        bool fileExists = !gSystem->AccessPathName(fileName);
        if(!fileExists) return false;

        if(objName.EqualTo("/")) return true;
        return GetKey(fqfn, obj_cmpl) != NULL;
}

bool TFileReader::Find(TString fqfn)
{
        if(fqfn.EqualTo("")) return false;
        //gSystem->ExpandPathName(fqfn);      // Interprete environment variables..  might be useful ? TBC

        std::vector<TString> vFqfns0 = ExpandWildcardUrl(fqfn, "~");
        if(vFqfns0.size() == 0) return false;

        for(int i = 0, Ni = this->vFqfns.size(); i < Ni; i++) {

                TString filename = FileName(this->vFqfns[i]);
                for(int j = 0, Nj = vFqfns0.size(); j < Nj; j++) {

                        TString filename0 = FileName(vFqfns0[j]);
                        if(filename.EqualTo(filename0)) return true;
                }
        }

        return false;
}

bool TFileReader::IsTextFile(TString fqfn)
{
        return fqfn.EndsWith(".txt");
}

std::vector<TObject*> TFileReader::MergeObjects(Option_t *opt, int buffer)
{
	std::vector<TString> vInput = GetFqfn();

        std::vector<TString> vPath;
        for(int i = 0, N = vInput.size(); i < N; i++) {
        
                TString path = TFileReader::Obj(vInput[i]);
                if(std::find(vPath.begin(), vPath.end(), path) != vPath.end()) continue;
                
                vPath.push_back(path);
        }
        
        std::vector<TObject*> objects;
        for(int i = 0, N = vPath.size(); i < N; i++) {

                TObject *object = MergeObjects(vPath[i], opt, buffer);
                if(object) objects.push_back(object);
        }

        return objects;
}

TObject* TFileReader::MergeObjects(TString path, Option_t *opt, int buffer)
{
	std::vector<TString> vInput = GetFqfn();

        std::vector<TObject*> vObjects;
        for(int i = 0, N = vInput.size(); i < N; i++)
        {
                if(!vInput[i].EndsWith(kSeparator+path)) continue;
                vObjects.push_back(GetKey(i));
        }

        return TFileReader::MergeObjects(vObjects, opt, buffer);
}

TObject* TFileReader::MergeObjects(std::vector<TObject*> objects, Option_t *opt, int buffer)
{
        TList *l = new TList;
        for(int i = 0, N = objects.size(); i < N; i++)
                l->Add(objects[i]);

        TObject *object = MergeObjects(l, opt, buffer);

        l->Clear("nodelete");
        return object;
}

TObject* TFileReader::MergeObjects(TList *list, Option_t *opt, int buffer)
{
        UNUSED(opt);
        UNUSED(buffer); // Might be used later on to optimize object merging
        if(list->GetEntries() == 0) return NULL;
        if(list->GetEntries() == 1) return list->At(0);
        
        TObject *object = list->At(0);
        TString dataClass = object->ClassName();
        if(object->InheritsFrom(TKey::Class())) {
                dataClass = ((TKey*) object)->GetClassName();
        }
        
        TClass *cl = TClass::GetClass(dataClass);
        if(cl->InheritsFrom(TTree::Class())) {

                return (TObject*) TTree::MergeTrees(list, opt);
                
        } else if(cl->InheritsFrom(TProfile2D::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Merging is currently not implementeed for " + dataClass);
                return NULL;

        } else if(cl->InheritsFrom(TProfile::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Merging is currently not implementeed for " + dataClass);
                return NULL;

        } else if(cl->InheritsFrom(TGraph2D::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Merging is currently not implementeed for " + dataClass);
                return NULL;

        } else if(cl->InheritsFrom(TGraph::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Merging is currently not implementeed for " + dataClass);
                return NULL;

        } else if(cl->InheritsFrom(TH3::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Merging is currently not implemented for " + dataClass);
                return NULL;

        } else if(cl->InheritsFrom(TH2::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Merging is currently not implemented for " + dataClass);
                return NULL;

        } else if(cl->InheritsFrom(TH1::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Merging is currently not implemented for " + dataClass);
                return NULL;

        } else {

                TPrint::Warning(__METHOD_NAME__, "Merging is currently not supported for " + dataClass);
                return NULL;
        }

        return object;
}

std::vector<TObject*> TFileReader::ChainObjects(Option_t *opt, int buffer)
{
	std::vector<TString> vInput = GetFqfn();

        std::vector<TString> vPath;
        for(int i = 0, N = vInput.size(); i < N; i++) {
        
                TString path = TFileReader::Obj(vInput[i]);
                if(std::find(vPath.begin(), vPath.end(), path) != vPath.end()) continue;
                
                vPath.push_back(path);
        }
        
        std::vector<TObject*> objects;
        for(int i = 0, N = vPath.size(); i < N; i++) {

                TObject *object = ChainObjects(vPath[i], opt, buffer);
                if(object) objects.push_back(object);
        }

        return objects;
}

TObject* TFileReader::ChainObjects(TString path, Option_t *opt, int buffer)
{
	std::vector<TString> vInput = GetFqfn();
        
        std::vector<TObject*> vObjects;
        for(int i = 0, N = vInput.size(); i < N; i++)
        {
                if(!vInput[i].EndsWith(kSeparator+path)) continue;
                vObjects.push_back(GetKey(i));
        }

        return TFileReader::ChainObjects(vObjects, opt, buffer);
}

TObject* TFileReader::ChainObjects(std::vector<TObject*> objects, Option_t *opt, int buffer)
{
        UNUSED(opt);
        UNUSED(buffer); // Might be used later.. to subdivide the merging if too many files

        TList *l = new TList;
        for(int i = 0, N = objects.size(); i < N; i++) {
                l->Add(objects[i]);
        }

        TObject *object = ChainObjects(l, opt, buffer);
        
        l->Clear("nodelete");
        return object;
}

TObject* TFileReader::ChainObjects(TList *list, Option_t *opt, int buffer)
{
        UNUSED(opt);
        UNUSED(buffer); // Might be used later.. to subdivide the merging if too many files

        if(list->GetEntries() == 0) return NULL;
        
        TObject *object = list->At(0);
        TString dataClass = object->ClassName();
        if(object->InheritsFrom(TKey::Class())) {
                dataClass = ((TKey*) object)->GetClassName();
        }
        
        TClass *cl = TClass::GetClass(dataClass);
        if(cl->InheritsFrom(TTree::Class())) {

                TList *userInfo = ((TTree*)((TKey *) object)->ReadObj())->GetUserInfo();
                object = new TChain("chain_"+ROOT::IOPlus::Helpers::GetRandomStr());

                TList *userInfoChain = ((TTree*) object)->GetUserInfo();
                if(userInfoChain->GetEntries() == 0) {
                        
                        for(int i = 0, N = userInfo->GetEntries(); i < N; i++)
                                userInfoChain->Add(userInfo->At(i));
                }

                for(int i = 0, N = list->GetEntries(); i < N; i++) {

                        if(TPrint::WarningIf(!list->At(i)->InheritsFrom(TKey::Class()), __METHOD_NAME__, "Chaining TTrees must use TKeys.. skip"))
                                continue;

                        TKey *key = (TKey*) list->At(i);
                     
                        TString fname = key->GetFile()->GetName();
                        TString path = (TString) key->GetMotherDir()->GetPath() + "/" + key->GetName();
                                path = path(fname.Length() + 1, path.Length() - fname.Length() + 1);

                        ((TChain*) object)->Add(fname + "/" + path);
                }

        } else if(cl->InheritsFrom(TProfile2D::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Chaining is currently not implementeed for " + dataClass);
                return NULL;

        } else if(cl->InheritsFrom(TProfile::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Chaining is currently not implementeed for " + dataClass);
                return NULL;

        } else if(cl->InheritsFrom(TGraph2D::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Chaining is currently not implementeed for " + dataClass);
                return NULL;

        } else if(cl->InheritsFrom(TGraph::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Chaining is currently not implementeed for " + dataClass);
                return NULL;

        } else if(cl->InheritsFrom(TH3::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Chaining is currently not implemented for " + dataClass);
                return NULL;

        } else if(cl->InheritsFrom(TH2::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Chaining is currently not implemented for " + dataClass);
                return NULL;

        } else if(cl->InheritsFrom(TH1::Class())) {

                TPrint::Warning(__METHOD_NAME__, "Chaining is currently not implemented for " + dataClass);
                return NULL;

        } else {

                TPrint::Warning(__METHOD_NAME__, "Chaining is currently not supported for " + dataClass);
                return NULL;
        }

        return object;
}


std::vector<TString> TFileReader::RecursiveExpandTextFile(TString fqfn)
{
        if(!IsTextFile(fqfn)) return {fqfn};
        std::vector<TString> v0;

        // Expand the initial file..
        std::vector<TString> vi = ExpandWildcardUrl(fqfn);
        for(int i = 0, N = vi.size(); i < N; i++) {

                if(vi.size() > 1) {
                        TPrint::SetSingleCarriageReturn();
                        TPrint::ProgressBar("Recursive expand.. \""+vi[i]+"\" ", "", i+1, N);
                }

                std::ifstream f(vi[i], std::ios::in);
                if( TPrint::WarningIf(!f.is_open(), __METHOD_NAME__, "Cannot open \"" + fqfn + "\"") ) return {fqfn};

                for(std::string line; getline(f, line); ) {

                        TString str = line;
                        if(str.BeginsWith("#")) continue;
                        else if( IsTextFile(str)) {

                                std::vector<TString> v = RecursiveExpandTextFile(str);
                                for(int j = 0, J = v.size(); j < J; j++) {

                                        if( find(v0.begin(), v0.end(), v[j]) == v0.end() ) v0.push_back(v[j]);
                                        else TPrint::Warning(__METHOD_NAME__, "Duplicates found \"%s\".. skip to avoid infinite loops..", v[j].Data());
                                }

                        } else if (!str.EqualTo("")) {

                                v0.push_back(str);
                        }
                }
                f.close();
        }

        if(v0.size() == 0) return {fqfn};
        return v0;
}

TString TFileReader::ApplyCorrection(TString fqfn)
{
        TString url = Url(fqfn);
        TString obj = Obj(fqfn);

        return ApplyCorrectionUrl(url) + kSeparator + ApplyCorrectionObj(obj);
}

TString TFileReader::ApplyCorrectionUrl(TString os)
{
        // Remove space
        os.ReplaceAll("^[ ]*", "");
        os.ReplaceAll("[ ]*$", "");

        if(os.EqualTo("")) return "";

        // Add a protocol (NB:TUrl not fully working if no protocol found..)
        if(os.Index("://") == -1) os = "file://" + os;
        if(os.Index("://$")) return os;

        // Remove last slash
        int lastslashpos = os.Last('/');
        if(lastslashpos == os.Length() - 1 && lastslashpos >= 0) os = os.Remove(lastslashpos);

        int lastdotdotpos = os.Last(kSeparator);
        if(lastdotdotpos == os.Length()) os.Remove(lastdotdotpos);

        return os;
}

TString TFileReader::ApplyCorrectionObj(TString os)
{
        // Trim std::string
        os.ReplaceAll("^[ ]*", "");
        os.ReplaceAll("[ ]*$", "");

        // Strip out all // recursively
        while(os != os.ReplaceAll("//", "/") || os != os.ReplaceAll("/./", "/")) {}

        // Remove absolute path
        while(os.Index("/") == 0 || os.Index(".") == 0) os = os(1, os.Length());

        // Remove last slash
        int lastslashpos = os.Last('/');
        if(lastslashpos == os.Length() - 1 && lastslashpos >= 0) os = os.Remove(lastslashpos);

        if(os.First('/') == 0) os = os(1, os.Length());

        return (os.EqualTo("") || os.EqualTo(".")) ? "/" : "/" + os;
}

std::vector<TString> TFileReader::ListDirectory(TString path, bool bRecursive)
{
        std::vector<TString> v;
        path.Strip(TString::kTrailing, '/');
        path = path + "/";
        
        if (auto dir = opendir(path.Data())) {

                while (auto f = readdir(dir)) {

                if (TString(f->d_name).EqualTo("") || TString(f->d_name).BeginsWith('.')) continue;
                if (f->d_type == DT_DIR) {

                        std::vector<TString> v0 = bRecursive ? ListDirectory(path + f->d_name) : std::vector<TString>({});
                        v.insert(v.end(),v0.begin(),v0.end());
                        v.push_back(path + f->d_name);
                }
                
                if (f->d_type == DT_REG)
                        v.push_back(path + f->d_name);
                }

                closedir(dir);
        }

        return v;
}

bool TFileReader::IsDirectory(TString fqfn, TString obj)
{
        TString url = Url(fqfn);
                url = TFileReader::ExpandVariables(url);

        if(obj.EqualTo("")) {

                url = url.ReplaceAll("file://", "");

#if __cplusplus >= 201402L // C++14
                const fs::path path(url.Data()); // Constructing the path from a std::string is possible.
                std::error_code ec; // For using the non-throwing overloads of functions below.
                return fs::is_directory(path, ec);
#else
                struct stat info;
                if(stat(url.Data(), &info ) != 0) return false;
                else if(info.st_mode & S_IFDIR) return true;
                return 0;
#endif

        }

        //
        // Looking into root file..
        if(!IsRootFile(url)) return false;

        obj = Obj(fqfn) + "/" + obj;
        obj = ApplyCorrectionObj(obj);

        TString obj_basename = ObjBaseName(obj);
        TString obj_dirname = ObjDirName(obj);

        TFile *f = NULL;
        if( !Open(url, f, "READ") ) return false;
        TPrint::RedirectTTY(__METHOD_NAME__);
        gDirectory->cd(obj_dirname);
        TPrint::DumpTTY(__METHOD_NAME__);

        TKey *key = gDirectory->FindKey(obj_basename);
        TString eMessage = "No key named \"" + obj_basename + "\" in " + url + kSeparator + obj_dirname + " found..";

        bool isDirectory;
        if( TPrint::WarningIf(key == NULL, __METHOD_NAME__, eMessage) ) isDirectory = false;
        else isDirectory = gROOT->GetClass(key->GetClassName())->InheritsFrom("TDirectory");

        Close(f);

        return isDirectory;
}

bool TFileReader::IsProperlyFormatted(TString fqfn)
{
        TString domain, filename, objname;
        ParseFile(fqfn, domain, filename, objname);

        return (!filename.EqualTo("") && !objname.EqualTo(""));
}

bool TFileReader::UnlinkEmptyCorrupt(TString rootFile)
{
        if (TFileReader::Exists(rootFile)) { // Check if file is corrupted

                TDirectory *gParentDirectory = gDirectory;
                TFile testfile(rootFile);
                if (testfile.IsZombie() || testfile.TestBit(TFile::kRecovered) || !testfile.GetListOfKeys()->GetSize()) {
                        gSystem->Unlink(rootFile);
                        return true;
                }

                gParentDirectory->cd();
        }

        return false;
}

bool TFileReader::IsValid()
{
        if( !this->HasOrigin() ) return false;
        if(!this->vFqfns.size()) return false;

        return true;
}

bool TFileReader::HasPattern(TString fqfn)
{
        return fqfn.Index(TRegexp("{[0-9]*}")) != -1;
}

std::vector<TString> TFileReader::ParseIdentifier(TString fqfn, TString pattern)
{
        std::vector<TString> output;
        if(!HasPattern(pattern)) {

                TPrint::Warning(__METHOD_NAME__, "Cannot parse input std::string.. There is no brackets like-{} in the pattern.. skip");
                return {};
        }

        TString _pattern = pattern;
                _pattern.ReplaceAll("{}", "([^"+TString(kSeparator)+"./:]+)");
                _pattern.ReplaceAll(".*", "*").ReplaceAll("*", ".*");

        TPRegexp rgx(_pattern);
        std::vector<TString> identifier;

        int offset = 0;
        TString fname = FileName(fqfn);

        while(!fname.EqualTo("")) {

                TIter Next(rgx.MatchS(fname));

                TObjString* obj = (TObjString*) Next(); // Match
                if(obj == NULL) break;
                
                offset = fname.Index(obj->GetString()) + obj->GetString().Length();
                fname = fname(offset, fname.Length());
                while ( (obj = (TObjString*) Next()) ) {
                        identifier.push_back(obj->GetString());
                }
        }

        if(!identifier.size()) {

                TPrint::Warning(__METHOD_NAME__, "\"" + FileName(fqfn) + "\" cannot match pattern \""+pattern+"\"");
                return {};
        }

        TPrint::Debug(10, __METHOD_NAME__, "Parsing `%s` using regex `%s` => [%s]", 
		fqfn.Data(), _pattern.Data(), 
		ROOT::IOPlus::Helpers::Implode(", ", identifier).Data()
	);

        return identifier;
}

void TFileReader::ParseFile(TString fqfn, TString& domain, TString& filename, TString& objname)
{
        domain.Clear();
        filename.Clear();
        objname.Clear();

        domain   = Domain(fqfn);
        filename = FileName(fqfn);
        objname  = Obj(fqfn);
        objname  = ApplyCorrectionObj(objname);

        TString eMessage = "Parsing the following FQFN \"" + fqfn + "\"";
        TPrint::Debug(11, __METHOD_NAME__, eMessage);
        eMessage = "domain = " + domain;
        TPrint::Debug(11, __METHOD_NAME__, eMessage);
        eMessage = "filename = " + filename;
        TPrint::Debug(11, __METHOD_NAME__, eMessage);
        eMessage = "objname = " + objname;
        TPrint::Debug(11, __METHOD_NAME__, eMessage);
}

bool TFileReader::Close(int i) { return Close(vFiles[i]); }
bool TFileReader::Close(TFile* &f)
{
        if(f == NULL) return false;

        int index  = find(vFiles.begin(), vFiles.end(), f) - vFiles.begin();
        if (index != (int) vFiles.size()) vFiles.erase(vFiles.begin() + index);
        else {

                TString eMessage = "File \"" + TString(f->GetName()) + "\" not found in the filelist..";
                TPrint::Warning(__METHOD_NAME__, eMessage);
        }

        delete f;
        f = NULL;

        return true;
}

std::vector<TObject*> TFileReader::GetObjects()
{
        std::vector<TObject*> vObject(this->vFqfns.size(), NULL);
        if(this->option.Contains("t")) {

                TString eMessage = "Compute std::vector of object using t-option.. (N = "+TString::Itoa(this->vFqfns.size(),10)+")";
                TPrint::Debug(10, this->GetMethodName(__METHOD_NAME__), eMessage);

                for(int i = 0; i < this->vFqfns.size(); i++) {

                        if(this->vFqfns[i].EqualTo("")) vObject.push_back(NULL);
                        else {

                                TObject *obj = new TObjString(FileName(this->vFqfns[i]));
                                vObject.push_back(obj);
                        }
                }

        } else {

                std::vector<TKey*> vKey = GetKeys();
                for(int i = 0, N = vKey.size(); i < N; i++) {

                        vObject[i] = this->ReadObj(vKey[i]);
                }
        }
        return vObject;
}

std::vector<TKey*> TFileReader::GetKeys()
{
        if( TPrint::WarningIf(this->option.Contains("t"), __METHOD_NAME__, "Cannot compute std::vector of keys.. t-option found.") ) return {};

        std::vector<TString> vPath = GetFqfn();
        std::vector<TKey*> vKey(vPath.size());
        for(int i = 0, N = vKey.size(); i < N; i++)
                vKey[i] = this->GetKey(vPath[i]);

        return vKey;
}

TString TFileReader::KeyPath(TKey *key)
{
        if (TPrint::ErrorIf(key == NULL, __METHOD_NAME__, "Key passed is NULL ptr")) return "";
        if (TPrint::ErrorIf(key->GetMotherDir() == NULL, __METHOD_NAME__, "Mother dir of the key is NULL ptr")) return "";

        TString keypath = (TString) key->GetMotherDir()->GetPath() + "/" + key->GetName();
        return ApplyCorrection(keypath);
}

void TFileReader::Print() {

        // Check if it must be verbose
        TString eMessage = (TString) "Origin = \"" + this->origin + "\"";
        TPrint::Message(this->GetMethodName(__METHOD_NAME__), eMessage);

        if(this->option.Contains("v")) {

                eMessage = (TString) "vFqfns.size = \"" + this->vFqfns.size() + "\"";
                TPrint::Debug(10, this->GetMethodName(__METHOD_NAME__), eMessage);
                for(int i = 0, N = this->vFqfns.size(); i < N; i++)
                        TPrint::Debug(11, this->GetMethodName(__METHOD_NAME__), "vFqfns["+TString::Itoa(i,10)+"] = \"" + this->vFqfns[i] +"\"");
        }

        if(!this->option.Contains("t")) eMessage = TString::Itoa(this->vFqfns.size(),10) + " object(s) found";
        else eMessage = TString::Itoa(this->vFqfns.size(),10) + " text file(s) found";
        TPrint::Message(this->GetMethodName(__METHOD_NAME__), eMessage);

        eMessage = "\"~\" option found.. Please use \"TFileReader(" + TString(this->GetName()) + ")::Add()\".. something unexpected here";
        if(this->option.Contains("~") && !this->vFqfns.size()) TPrint::Error(this->GetMethodName(__METHOD_NAME__), eMessage);
}

TList* TFileReader::ReadObj(TList* l)
{
    if(l == NULL) return NULL;

    TList *lOutput = new TList;

    int N = l->GetSize();
    for(int i = 0; i < N; i++) {

        if(N > 512) TPrint::ProgressBar("Loading objects on memory.. ", "", i+1, N);

        TObject *obj = l->At(i);
        if(!obj->InheritsFrom("TKey")) lOutput->Add(obj);
        else {

            TKey *key = (TKey*) obj;
            lOutput->Add( key->ReadObj() );
        }
    }

    return lOutput;
}

TObject* TFileReader::ReadObj(TKey* key)
{
        if(key == NULL) return NULL;
        if(!key->InheritsFrom("TKey")) return key;

        TObject *obj = NULL;
        TString eMessage = "Key cannot be loaded.. (Invalid char in the object name? Too many files opened?)";
        if( TPrint::WarningIf(key == NULL, __METHOD_NAME__, eMessage) ) return NULL;

        obj = key->ReadObj();

        TString keypath;
        if(key->GetMotherDir() != NULL) keypath = (TString) key->GetMotherDir()->GetPath() + "/" + key->GetName();
        else keypath = (TString) "NoFileAttached.root"+kSeparator + "/" + key->GetName();

        keypath = ApplyCorrection(keypath);

        eMessage = "Reading object \"" + keypath + "\"";
        TPrint::Debug(11, __METHOD_NAME__, eMessage);

        return obj;
}

TObject* TFileReader::ReadObj(TDirectory *dir, TString obj_cmpl)
{
        if(dir == NULL) return NULL;

        TKey *key = GetKey(dir, obj_cmpl);
        return ReadObj(key);
}

TObject* TFileReader::ReadObj(TString fqfn, TString obj_cmpl)
{
        if(IsTextFile(fqfn)) return new TObjString(fqfn);
        if(fqfn.EqualTo("")) return NULL;

        TKey *key = GetKey(fqfn, obj_cmpl);
        return ReadObj(key);
}


TKey* TFileReader::GetKey(TDirectory *dir, TString obj_cmpl)
{
        if(dir == NULL) return NULL;

        TDirectory *gParentDirectory = gDirectory;
        obj_cmpl = ApplyCorrectionObj(obj_cmpl);

        TString obj_basename = ObjBaseName(obj_cmpl);
        TString obj_dirname = ObjDirName(obj_cmpl);
        
        int obj_cycle = ObjCycle(obj_cmpl);
        if(obj_cycle != 0) obj_basename += ";" + TString::Itoa(obj_cycle,10);
         
        TObjArray *objArray = obj_dirname.Tokenize("/");
        for (int i = 0; i < objArray->GetEntries(); i++) {

                TString subdirname = ((TObjString *)(objArray->At(i)))->String();
                subdirname.ReplaceAll(kSeparator, "=");
                
                if( !dir->Get(subdirname) ) return NULL;
                dir->cd(subdirname);
        }
        
        TKey *k = gDirectory->FindKey(obj_basename);

        gParentDirectory->cd();
        return k;
}

TKey* TFileReader::GetKey(TString fqfn, TString obj_cmpl)
{
        if(fqfn.EqualTo("")) return NULL;

        TDirectory *gParentDirectory = gDirectory;
        TString url = Url(fqfn);

        obj_cmpl = Obj(fqfn) + "/" + obj_cmpl;
        obj_cmpl = ApplyCorrectionObj(obj_cmpl);
        
        TString obj_basename = ObjBaseName(obj_cmpl);
        TString obj_dirname = ObjDirName(obj_cmpl);

        int obj_cycle = ObjCycle(obj_cmpl);
        if(obj_cycle != 0) obj_basename += ";" + TString::Itoa(obj_cycle,10);

        TFile *f = NULL;
        if( !Open(url, f, "READ", kReuse) ) return NULL;
        gErrorIgnoreLevel = kBreak;
        
        gDirectory->cd(obj_dirname);
        gErrorIgnoreLevel = kPrint;

        TKey *k = gDirectory->FindKey(obj_basename);

        gParentDirectory->cd();
        return k;
}
