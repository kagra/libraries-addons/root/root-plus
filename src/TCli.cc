/**
 **********************************************
 *
 * \file TCli.cc
 * \brief Source code of the Handbox class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include "TCli.h"

#define UNUSED(x) (void)(x) // Macro to hide all ununsed parameters

TApplication *TCli::fApp = NULL;
TBrowser *TCli::fBrowser = NULL;

bool TCli::bGraphicsLoop = true;
bool TCli::bOpenGraphics = false;
bool TCli::kNoHeader = false;
int TCli::lastOptionPos = 0;
const bool TCli::kReverseLogic = true;
// const bool TCli::kUseDefault = true;
// const bool TCli::kNotUseDefault  = !TCli::kUseDefault;

void TCli::EnableGraphicsHandler(){

    TCli::bGraphicsLoop = true;

    TPrint::Debug(10, __METHOD_NAME__, "Graphics catcher is enabled..");

    TPrint::SetSingleCarriageReturn();
    TPrint::Message(__METHOD_NAME__, "Please use Ctrl+C to close canvas(es)..\n");
    
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = TCli::GraphicsHandler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
}

void TCli::DisableSignalHandler() {

    TPrint::Debug(10, __METHOD_NAME__, "Signal catcher is disabled..");
    signal(SIGINT, SIG_DFL);
}

void TCli::GraphicsHandler(int s){

    std::cerr << std::endl;
    std::cerr << "** Caught signal " << s << std::endl;
    std::cerr << "** You pressed CTRL-C => Trying to exit graphics mode.." << std::endl;
    std::cerr << "** Resume normal execution.." << std::endl;

    TCli::DisableSignalHandler();
    TCli::bGraphicsLoop = false;

    TPrint::EmptyTrash();
}

void TCli::OpenGraphics(Option_t *option) {

    if(bOpenGraphics) return;     // Already opened

    TString opt = option;
    if(opt.EqualTo("")) opt = gSystem->Getenv("OPT");

    // If draw option is set.. start a session in advance..
    if((opt.Contains("b") || opt.Contains("g"))) {

        TPrint::Message(__METHOD_NAME__, "Graphics interface opened.. close windows to exit..");
        if(!TCli::fApp) {
            TCli::fApp = new TApplication("root", NULL, NULL);
            // TCli::fApp->Connect(); // connect keyboard?
        }
    }

    if(opt.Contains("b")) {

        TPrint::Message(__METHOD_NAME__, "TBrowser interface opened.. close windows to exit..");
        if(TCli::fBrowser == NULL) TCli::fBrowser = new TCliBrowser();
    }

    if(!opt.Contains("b") && !opt.Contains("g")) {

        TPrint::Message(__METHOD_NAME__, "No graphics interface called.. (Use b-option for tbrowser / g-option for graphics)");
    }

    TCli::bOpenGraphics = true;
}

void TCli::EnableMT(int nThreads) {
    ROOT::EnableImplicitMT(nThreads);
}

void TCli::DisableMT() {

    ROOT::DisableImplicitMT();
}

bool TCli::IsParallel(int nThreads) { return TCli::HasMT(nThreads); }
bool TCli::HasMT(int nThreads) {

    if(nThreads > 0 && GetNCores() < nThreads) return false;
    return ROOT::IsImplicitMTEnabled();
}

int TCli::GetNCores() { return TCli::GetMTPoolSize(); }
int TCli::GetMTPoolSize() {
    return IsParallel() ? ROOT::GetThreadPoolSize() : 1;
}

bool TCli::IsBatch() {
    return !gROOT->IsBatch();
}

bool TCli::IsInteractive() {
    return isatty(fileno(stdin));
}

bool TCli::HasGraphics(Option_t *option) {

    TString opt = option;
    if(opt.EqualTo("")) opt = gSystem->Getenv("OPT");

    return (TCli::bOpenGraphics || opt.Contains("g") || opt.Contains("b")) && !gROOT->IsBatch();
}

void TCli::WaitForGraphicsToBeClosed(Option_t *option) {

    TString opt = option;
    if(opt.EqualTo("")) opt = gSystem->Getenv("OPT");

    TCli::EnableGraphicsHandler();
    if ( gROOT->GetListOfCanvases()->GetSize() ) {

        TString eMessage = "Canvas found, but no graphics option enabled.. skip";
        if(TPrint::WarningIf(!HasGraphics(opt), __METHOD_NAME__, eMessage)) {

            TCli::bGraphicsLoop = false;
            return;
        }

        TPrint::Debug(10, __METHOD_NAME__, "List of canvases found:");
        for(int i = 0; i < gROOT->GetListOfCanvases()->GetSize(); i++) {

            TCanvas *c = (TCanvas*) gROOT->GetListOfCanvases()->At(i);
            c->cd();             //Needed to set back gPad to the main canvas, not subpad

            TPrint::Debug(10, __METHOD_NAME__, "* Name:\"" + TString(c->GetName()) + "\"; (Title: \""+TString(c->GetTitle())+"\")");
            c->Modified();
            c->Update();
        }

        while(TCli::bGraphicsLoop)
            gSystem->ProcessEvents();
    }

    TCli::CloseGraphics();
    TCli::DisableSignalHandler();
}

void TCli::CloseGraphics()
{
    if(fBrowser) {

        delete fBrowser;
        fBrowser = NULL;
    }

    TCli::bOpenGraphics = false;
}

void TCli::Init(int *newArgc, char **newArgv, Option_t *newOption) {

    /**
     * Default options: "verbose, random, graphics, multithread"
     */
    if(TPrint::ErrorIf(this->argc != 0, __METHOD_NAME__, "TCli already initialized..")) return;

    this->option = newOption;
    TString opt = this->option;
            opt.ToLower();

    this->kPrintTCliOnDestroy = true;

    this->iarg = 0;
    this->kUsage = true;
    this->bArgOptionLock = false;

    this->argc = *newArgc;
    this->lastOptionPos = this->argc;
    for(int i = 0; i < this->argc; i++) {

        TString argv_str = newArgv[i];
                argv_str.ReplaceAll('\n', ' ');

        if(argv_str.EqualTo("--")) this->lastOptionPos = i;
        argv.push_back(argv_str);
    }

    if(this->lastOptionPos != this->argc) {
        this->argv.erase(argv.begin()+this->lastOptionPos);
        this->argc--;
    }

    this->AddTitle("Default options");

    // Debug must be before redirecting..
    int iDebug = (int) Verbosity::VERBOSITY_NORMAL;

    bool debug = false;
    if (opt.Contains("verbose")) {

        opt.ReplaceAll("verbose", "");

        this->AddOption("-v", "", debug);
        if(debug) iDebug = (int) Verbosity::VERBOSITY_VERBOSE;
        this->AddOption("-vv", "", debug);
        if(debug) iDebug = (int) Verbosity::VERBOSITY_VERY_VERBOSE;
        this->AddOption("-vvv", "", debug);
        if(debug) iDebug = (int) Verbosity::VERBOSITY_DEBUG;

        bool bQuiet = false;
        this->AddOption("--silent|--quiet|-q", "", bQuiet);
        if(bQuiet) TPrint::kQuiet = true;

        bool bSuperQuiet = false;
        this->AddOption("--very-silent|--very-quiet|-qq", "", bSuperQuiet);
        if(bSuperQuiet) TPrint::kSuperQuiet = true;
    }

    if (opt.Contains("random")) {

        opt.ReplaceAll("random", "");

        int seed = 0;
        this->AddOption("--seed", "Random seed", seed);
        if(seed) ROOT::IOPlus::Helpers::SetSeed(seed);
    }

    if (opt.Contains("graphics")) {

        opt.ReplaceAll("graphics", "");

        bool bGraphics = false;
        this->AddOption("--graphics", "(Draw canvas)", bGraphics);
        if(bGraphics) TCli::OpenGraphics("g");
    }

    if (opt.Contains("multithread")) {

        opt.ReplaceAll("multithread", "");

        int nThreads = -1;
        this->AddOption("--threads|-j", "Number of concurrent threads", nThreads);
        if(nThreads > 0) TCli::EnableMT(nThreads);
    }

    this->AddOption("--no-header", "Remove program description", this->kNoHeader);

    this->AddOption("--help|-h", "Display help message", this->kUsage, kReverseLogic);

    this->AddOption("--debug", "Enable additional messages for debugging", debug);
    if(debug) iDebug = (int) Verbosity::VERBOSITY_DEBUG;

    TPrint::kDebug = TMath::Max(TPrint::kDebug, iDebug);
    TPrint::RedirectTTY(__METHOD_NAME__);

    TString _opt = opt.ReplaceAll(" ", "");
    if(TPrint::ErrorIf(!_opt.EqualTo(""), __METHOD_NAME__, "Invalid usage option provided.. \""+opt+"\" cannot be used.. please check !")) this->kUsage = false;
    this->AddTitle("User-defined options and arguments");

    // Put errors in the buffer..
    TString err;
    TPrint::GetTTY(__METHOD_NAME__, NULL, &err);
    if(!err.EqualTo("")) {
        bufferTTY[1].push_back(err);
    }
}

void TCli::AddTitle(TString title)
{
    this->vTitle.push_back(title);
    this->vTitlePos.push_back(this->vArgName.size());
}

void TCli::AddMessage(TString str)
{
    usage_msg += str + '\n';
}

bool TCli::Help(int iarg)
{
    if(iarg > 0 && iarg < this->argc) {

         if(this->argv[iarg].EqualTo("--help") || this->argv[iarg].EqualTo("-h")) {

            this->kUsage = false;
            return true;
        }
    }

    for(int i = 1; i < this->argc; i++) {

        if(this->argv[i].EqualTo("--help") || this->argv[i].EqualTo("-h")) {

            this->kUsage = false;
            return true;
        }
    }

    return false;
}

bool TCli::Valid(bool bForceUsage)
{
    if(TPrint::ErrorIf(this->argc == 0, __METHOD_NAME__, "class not initialized..")) return 0;
    for(int i = 0; i < this->argc; i++) {

        TString arg = this->argv[i];
                arg.ReplaceAll("%.", "\\%\\.");

        TPrint::Debug(10, __METHOD_NAME__, TString("arg["+TString::Itoa(i,10)+"] = ") + arg);
    }

    if(!this->kNoHeader) {

        this->Print();
        std::cout << std::endl;

        std::cout << TPrint::kBlue << '\t' << usage_msg << TPrint::kNoColor;
        std::cout << std::endl;
    }

    std::vector<int> vArgNotUsed = this->GetArgNotUsed();
    if(!this->kUsage || bForceUsage) {

        // Print usage line
        std::cout << TPrint::kGreen << '\t' << "Usage: \t" << TPrint::kNoColor << this->argv[0] << " ";

        bool bFirstMandatoryArg = true, firstVariadic = true, firstMandatory = true;
        for(int i = 0; i < this->vArgPlaceholder.size(); i++) {

            // Print title lines...
            unsigned int title_index = find(this->vTitlePos.begin(), this->vTitlePos.end(), i) - this->vTitlePos.begin();
            if( title_index != this->vTitlePos.size() ) {

                std::cout << std::endl << "\n\t\t" << TPrint::kPurple << "" << this->vTitle[title_index] << ":" << TPrint::kNoColor;
            }

            // Argument and options
            if(!this->vArgName[i].EqualTo("")) std::cout <<  "\t\t";
            else {

                if(bFirstMandatoryArg) std::cout <<  "\t\t";
                bFirstMandatoryArg = false;
            }

            std::cout << this->vArgColor[i];

            if (firstVariadic && this->vArgType[i] == ParameterType::Variadic) {
                firstVariadic = false;
                std::cout << std::endl << "\t\t";
            }
            if(this->vArgType[i] == ParameterType::Mandatory) {

                if(firstMandatory) {
                    std::cout << std::endl;
                    firstMandatory = false;
                }
                std::cout << "\n\t\t<";
            } else {
                std::cout << "\n\t\t[";
            }
    
            if(!this->vArgName[i].EqualTo("")) {

                std::cout << this->vArgName[i];
                if(!this->vArgPlaceholder[i].EqualTo("")) std::cout << " \"";
            }

            std::cout << this->vArgPlaceholder[i];
            if(!this->vArgName[i].EqualTo("") && !this->vArgPlaceholder[i].EqualTo("")) std::cout << "\"";

            if(this->vArgType[i] == ParameterType::Mandatory) {

                std::cout << ">";
            }

            else std::cout << "]";

            std::cout << TPrint::kNoColor;
        }

        if(std::find(this->vArgType.begin(), this->vArgType.end(), ParameterType::Variadic) != this->vArgType.end()) {
            std::cout << TPrint::kNoColor << " [..]" << TPrint::kNoColor;
        }

        if(!this->vArgPlaceholder.size()) std::cout << std::endl;
	    std::cout << std::endl << std::endl;

    } else {

        if((int) vArgNotUsed.size() != 0) {

            TString eMessage = this->argv[0] + ": too many parameters\n";
            eMessage += "Try '" + this->argv[0] + " --help' for more information";

            std::cerr << eMessage << std::endl;

            exit(1);
        }
    }

    int iMax = TMath::Max(this->bufferTTY[0].size(), this->bufferTTY[1].size());
    if (iMax) std::cout << std::endl;
    for(int i = 0; i < (int) iMax; i++) {
        if(i < this->bufferTTY[0].size() && !this->bufferTTY[0][i].EqualTo("")) std::cout << TPrint::Trim(this->bufferTTY[0][i]) << std::endl;
        if(i < this->bufferTTY[1].size() && !this->bufferTTY[1][i].EqualTo("")) std::cerr << TPrint::Trim(this->bufferTTY[1][i]) << std::endl;
    }

    if(!WrongUsageStr.EqualTo(""))
        std::cerr << "Error: " << WrongUsageStr;

    return this->kUsage;
}

std::vector<int> TCli::GetArgNotUsed() {

    std::vector <int> vNotUsedIndexes;

    for(int i = 1; i < this->argc; i++) {

        if( find(this->vArgAlreadyUsed.begin(), this->vArgAlreadyUsed.end(), i) == this->vArgAlreadyUsed.end() ) {

            vNotUsedIndexes.push_back(i);
        }
    }

    return vNotUsedIndexes;
}

int TCli::GetArgIndex(TString argname) {

    int iarg = -1;

    std::vector<TString> argLC;
    for(int j = 0, J = this->argv.size(); j < J; j++) {
        argLC.push_back(this->argv[j]);
        argLC[j].ToLower();
    }

    // Find the option !
    TObjArray *argname_array = argname.Tokenize("|");
    for (int i = 0; i < argname_array->GetEntries(); i++) {

        TString optname = TPrint::Trim(((TObjString *)(argname_array->At(i)))->String());
        TString optNameLC = optname;
                optNameLC.ToLower();

        int index = find(argLC.begin(), argLC.end(), optNameLC) - argLC.begin();
        if( index < (int) this->argv.size() ) {

            // Double option found in the user command
            if(iarg != -1) {

                TString eMessage = "Option \""+optname+"\" already called..";
                TPrint::Error(__METHOD_NAME__, eMessage);

                this->kUsage = false;
                return -1;
            }
            iarg = index;
        }
    }

    delete argname_array;
    argname_array = NULL;

    // Check if a parameter has been used 2 types = wrong command written by the user
    if( find(this->vArgAlreadyUsed.begin(), this->vArgAlreadyUsed.end(), iarg) != this->vArgAlreadyUsed.end() ) {

        this->kUsage = false;
        return -1;
    }

    return iarg;
}

bool TCli::CheckArg(int iarg) {

    TString str = "-?";
    if(iarg >= 0 && iarg < this->argc) str = this->argv[iarg];
    else {

        this->kUsage = false;
        return false;
    }

    if((str.BeginsWith("-") && !str.Contains(" ") && find(this->vArgAlreadyUsed.begin(), this->vArgAlreadyUsed.end(), iarg) != this->vArgAlreadyUsed.end() ) /*|| str.EqualTo("")*/) {

        if( str.EqualTo("--help") || str.EqualTo("-h") ) {

            this->kUsage = false;
            return false;
        }

        TPrint::DumpTTY(__METHOD_NAME__);

        TString eMessage;
        if(iarg < 0) eMessage = this->argv[0] + ": unknown parameter\n";
        else if(iarg >= this->argc) eMessage = this->argv[0] + ": missing parameter\n";
        else if(str.EqualTo("")) eMessage = this->argv[0] + ": empty string parameter found..; program not expected to be used like this.\n";
        else eMessage = this->argv[0] + ": unexpected predicate `" + str + "`; program not expected to be used like this. (one arg start with a dash ?)\n";
        eMessage += "Try '" + this->argv[0] + " --help' for more information";

        std::cerr << eMessage << std::endl;

        exit(1);
    }

    return true;
}

bool TCli::AddOption(TString name, TString description, bool &value, bool reverse_logic)
{
    TString eMessage = "Variadic argument already declared and goes last.";
    if( TPrint::ErrorIf(std::find(this->vArgType.begin(), this->vArgType.end(), ParameterType::Variadic) != this->vArgType.end(), __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Please declare all optional arguments first..";
    if( TPrint::ErrorIf(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( TPrint::ErrorIf(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        TPrint::Error(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from TCli..");
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgPlaceholder.push_back(description);
    this->vArgType.push_back(ParameterType::Optional);
    this->vArgColor.push_back(TPrint::kNoColor);

    bool b = false;
    int arg_index = this->GetArgIndex(name);

    if(arg_index != -1 && this->lastOptionPos > arg_index) {

        this->vArgAlreadyUsed.push_back(arg_index);
        b = true;
    }

    value = (reverse_logic == TCli::kReverseLogic) ? !b : b;
    return true;
}


//
// Add option for INT
//
bool TCli::AddOption(TString name, TString description, int &value, double placeholder)
{
    double dValue = (double) value;

    bool b = AddOption(name, description, dValue, placeholder);             // Use the double-like methods
    value = (int) dValue;

    return b;
}

bool TCli::AddOption(TString name, TString description, int &value, TString placeholder)
{
    return AddOption(name, description, value, ROOT::IOPlus::Helpers::InterpretFormula(placeholder));
}

bool TCli::AddOption(TString name, TString description, std::vector<int> &value, TString placeholder_str, char delimiter, int expected_size)
{
    std::vector<int> placeholder;

    TObjArray *array = placeholder_str.Tokenize(delimiter);
    for (int i = 0; i < array->GetEntries(); i++) {
        placeholder.push_back( ROOT::IOPlus::Helpers::InterpretFormula(((TObjString *)(array->At(i)))->String()) );
    }

    delete array;
    array = NULL;

    return TCli::AddOption(name, description, value, placeholder, delimiter, expected_size);
}

bool TCli::AddOption(TString name, TString description, std::vector<int> &value, std::vector<int> placeholder, char delimiter, int expected_size)
{
    std::vector<double> dValue(value.begin(), value.end());
    std::vector<double> dValueByDefault(placeholder.begin(), placeholder.end());
    for(int i = 0, N = value.size(); i < N; i++) dValue[i] = value[i];

    bool b = AddOption(name, description, dValue, dValueByDefault, delimiter, expected_size);

    value.resize(dValue.size());
    for(int i = 0, N = dValue.size(); i < N; i++) value[i] = (int) dValue[i];

    return b;
}

//
// Add option for Float
//
bool TCli::AddOption(TString name, TString description, float &value, double placeholder)
{
    double dValue = (double) value;

    bool b = AddOption(name, description, dValue, placeholder);             // Use the double-like methods
    value = (float) dValue;

    return b;
}

bool TCli::AddOption(TString name, TString description, float &value, TString placeholder)
{
    return AddOption(name, description, value, ROOT::IOPlus::Helpers::InterpretFormula(placeholder));
}

bool TCli::AddOption(TString name, TString description, std::vector<float> &value, TString placeholder_str, char delimiter, int expected_size)
{
    std::vector<float> placeholder;

    TObjArray *array = placeholder_str.Tokenize(delimiter);
    for (int i = 0; i < array->GetEntries(); i++) {

        TString str = ((TObjString *)(array->At(i)))->String();
        placeholder.push_back( ROOT::IOPlus::Helpers::InterpretFormula(str) );
    }
    delete array;
    array = NULL;

    return TCli::AddOption(name, description, value, placeholder, delimiter, expected_size);
}

bool TCli::AddOption(TString name, TString description, std::vector<float> &value, std::vector<float> placeholder, char delimiter, int expected_size)
{
    std::vector<double> dValue(value.begin(), value.end());
    std::vector<double> dValueByDefault(placeholder.begin(), placeholder.end());
    for(int i = 0, N = value.size(); i < N; i++) dValue[i] = value[i];

    bool b = AddOption(name, description, dValue, dValueByDefault, delimiter, expected_size);

    value.resize(dValue.size());
    for(int i = 0, N = dValue.size(); i < N; i++) value[i] = (float) dValue[i];

    return b;
}





//
// AddOption for Double
//
bool TCli::AddOption(TString name, TString description, std::vector<double> &value, TString placeholder_str, char delimiter, int expected_size)
{
    std::vector<double> placeholder;

    TObjArray *array = placeholder_str.Tokenize(delimiter);
    for (int i = 0; i < array->GetEntries(); i++) {

        TString str = ((TObjString *)(array->At(i)))->String();
        placeholder.push_back( ROOT::IOPlus::Helpers::InterpretFormula(str) );
    }
    delete array;
    array = NULL;

    return TCli::AddOption(name, description, value, placeholder, delimiter, expected_size);
}

bool TCli::AddOption(TString name, TString description, std::vector<double> &value, std::vector<double> placeholder, char delimiter, int expected_size)
{
    TString eMessage = "Variadic argument already declared and goes last.";
    if( TPrint::ErrorIf(std::find(this->vArgType.begin(), this->vArgType.end(), ParameterType::Variadic) != this->vArgType.end(), __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Please declare all optional arguments first..";
    if( TPrint::ErrorIf(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( TPrint::ErrorIf(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        TPrint::Error(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from TCli..");
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgPlaceholder.push_back(description);
    this->vArgType.push_back(ParameterType::Optional);
    this->vArgColor.push_back(TPrint::kNoColor);

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1 && this->lastOptionPos > arg_index) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            TPrint::Error(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str0 = this->argv[arg_index+1];

        TObjArray *array = str0.Tokenize(delimiter);
        if((unsigned int) array->GetEntries() > value.size()) value.resize(array->GetEntries());
        for (int i = 0; i < array->GetEntries(); i++) {

            TString str = ((TObjString *)(array->At(i)))->String();
            value[i] = ROOT::IOPlus::Helpers::InterpretFormula(str);
        }

        delete array;
        array = NULL;

    } else {

        // Set the default value if found
        if(placeholder.size() > value.size()) value.resize(placeholder.size());
        for(int i = 0, N = placeholder.size(); i < N; i++) {

            if( !ROOT::IOPlus::Helpers::IsNaN(placeholder[i]) ) value[i] = placeholder[i];
        }
    }

    if(expected_size != -1 && value.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"" + name + "\"";
        eMessage += ", " + TString::Itoa(value.size(), 10) + " element(s) received";
        TPrint::Error(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    return true;
}

bool TCli::AddOption(TString name, TString description, double &value, TString placeholder)
{
    return AddOption(name, description, value, ROOT::IOPlus::Helpers::InterpretFormula(placeholder));
}

bool TCli::AddOption(TString name, TString description, double &value, double placeholder)
{
    TString eMessage = "Variadic argument already declared and goes last.";
    if( TPrint::ErrorIf(std::find(this->vArgType.begin(), this->vArgType.end(), ParameterType::Variadic) != this->vArgType.end(), __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Please declare all optional arguments first..";
    if( TPrint::ErrorIf(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( TPrint::ErrorIf(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        TPrint::Error(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from TCli..");
        this->kUsage = false;
        return false;
    }


    this->vArgName.push_back(name);
    this->vArgPlaceholder.push_back(description);
    this->vArgType.push_back(ParameterType::Optional);
    this->vArgColor.push_back(TPrint::kNoColor);

    // Set the default value if found
    value = 0;
    if( !ROOT::IOPlus::Helpers::IsInf(placeholder) ) {

        value = placeholder;
        TPrint::Debug(10, __METHOD_NAME__, Form("Arg \"%s\" default value found %f", name.Data(), placeholder));
    }

    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1 && this->lastOptionPos > arg_index) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            TPrint::Error(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
    
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];

        value = ROOT::IOPlus::Helpers::InterpretFormula(str);
        TPrint::Debug(10, __METHOD_NAME__, Form("Arg \"%s\" found and set to \"%s\"", name.Data(), str.Data()));
        return true;
    }

    TPrint::Debug(10, __METHOD_NAME__, Form("Arg \"%s\" set to \"%f\"", name.Data(), value));
    return false;
}

//
// AddOption for bool
bool TCli::AddOption(TString name, TString description, std::vector<bool> &value, std::vector<bool> vReverseLogic, char delimiter, int expected_size)
{
    TString eMessage = "Variadic argument already declared and goes last.";
    if( TPrint::ErrorIf(std::find(this->vArgType.begin(), this->vArgType.end(), ParameterType::Variadic) != this->vArgType.end(), __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Please declare all optional arguments first..";
    if( TPrint::ErrorIf(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( TPrint::ErrorIf(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        TPrint::Error(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from TCli..");
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgPlaceholder.push_back(description);
    this->vArgType.push_back(ParameterType::Optional);
    this->vArgColor.push_back(TPrint::kNoColor);

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1 && this->lastOptionPos > arg_index) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            TPrint::Error(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];

        TObjArray *array = str.Tokenize(delimiter);
        if((unsigned int) array->GetEntries() > value.size()) value.resize(array->GetEntries());
        for (int i = 0; i < array->GetEntries(); i++) {

            const char *s = ((TObjString *)(array->At(i)))->String().Data();

            bool b;
            std::istringstream(s) >> std::boolalpha >> b;

            value[i] = b;
        }

        delete array;
        array = NULL;

    } else {

        // Set the default value if found
        if(vReverseLogic.size() > value.size()) value.resize(vReverseLogic.size());
        for(int i = 0, N = vReverseLogic.size(); i < N; i++) {

            if( !vReverseLogic[i] ) {

                value[i] = true;
                TPrint::Debug(10, __METHOD_NAME__, Form("Arg \"%s\"[%d] reverse logic value found.", name.Data(), i));
            }
        }
    }

    if(expected_size != -1 && value.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"%s\"";
        eMessage += ", " + TString::Itoa(value.size(), 10) + " element(s) received";
        TPrint::Error(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    return true;
}


//
// AddOption for TString
//
bool TCli::AddOption(TString name, TString description, std::vector<TString> &value, TString placeholder_str, char delimiter, int expected_size)
{
    std::vector<TString> placeholder;

    TObjArray *array = placeholder_str.Tokenize(delimiter);
    for (int i = 0; i < array->GetEntries(); i++) {

        placeholder.push_back( ((TObjString *)(array->At(i)))->String() );
    }
    delete array;
    array = NULL;

    return TCli::AddOption(name, description, value, placeholder, delimiter, expected_size);
}

bool TCli::AddOption(TString name, TString description, std::vector<TString> &value, std::vector<TString> placeholder, char delimiter, int expected_size)
{
    TString eMessage = "Variadic argument already declared and goes last.";
    if( TPrint::ErrorIf(std::find(this->vArgType.begin(), this->vArgType.end(), ParameterType::Variadic) != this->vArgType.end(), __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Please declare all optional arguments first..";
    if( TPrint::ErrorIf(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( TPrint::ErrorIf(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        TPrint::Error(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from TCli..");
        this->kUsage = false;
        return false;
    }


    this->vArgName.push_back(name);
    this->vArgPlaceholder.push_back(description);
    this->vArgType.push_back(ParameterType::Optional);
    this->vArgColor.push_back(TPrint::kNoColor);

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1 && this->lastOptionPos > arg_index) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            TPrint::Error(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];

        TObjArray *array = str.Tokenize(delimiter);
        if((unsigned int) array->GetEntries() > value.size()) value.resize(array->GetEntries());
        for (int i = 0; i < array->GetEntries(); i++) {

            value[i] = ((TObjString *)(array->At(i)))->String();
        }

        delete array;
        array = NULL;

    } else {

        // Set the default value if found
        if(placeholder.size() > value.size()) value.resize(placeholder.size());
        for(int i = 0, N = placeholder.size(); i < N; i++) {

            if( !placeholder[i].EqualTo("") ) {

                value[i] = placeholder[i];
                TPrint::Debug(10, __METHOD_NAME__, Form("Arg \"%s\"[%d] default value found %s", name.Data(), i, placeholder[i].Data()));
            }
        }
    }

    if(expected_size != -1 && value.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"%s\"";
        eMessage += ", " + TString::Itoa(value.size(), 10) + " element(s) received";
        TPrint::Error(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    return true;
}


//
// AddOption for TString
//
bool TCli::AddOption(TString name, TString description, std::vector<TFileReader> &value, TString placeholder_str, TClass *cl, char delimiter, int expected_size)
{
    std::vector<TString> placeholder = ROOT::IOPlus::Helpers::Explode(delimiter, placeholder_str);
    return TCli::AddOption(name, description, value, placeholder, cl, delimiter, expected_size);
}

bool TCli::AddOption(TString name, TString description, std::vector<TFileReader> &value, std::vector<TString> placeholder, TClass *cl, char delimiter, int expected_size)
{
    TString eMessage = "Variadic argument already declared and goes last.";
    if( TPrint::ErrorIf(std::find(this->vArgType.begin(), this->vArgType.end(), ParameterType::Variadic) != this->vArgType.end(), __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Please declare all optional arguments first..";
    if( TPrint::ErrorIf(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( TPrint::ErrorIf(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        TPrint::Error(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from TCli..");
        this->kUsage = false;
        return false;
    }


    this->vArgName.push_back(name);
    this->vArgPlaceholder.push_back(description);
    this->vArgType.push_back(ParameterType::Optional);
    this->vArgColor.push_back(TPrint::kNoColor);

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1 && this->lastOptionPos > arg_index) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            TPrint::Error(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];

        TObjArray *array = str.Tokenize(delimiter);
        if((unsigned int) array->GetEntries() > value.size()) value.resize(array->GetEntries());
        for (int i = 0; i < array->GetEntries(); i++) {

            value[i].SetClassName(cl);
            value[i].Init( ((TObjString *) (array->At(i)))->String() );
        }

        delete array;
        array = NULL;

    } else {

        // Set the default value if found
        if(placeholder.size() > value.size()) value.resize(placeholder.size());
        for(int i = 0, N = placeholder.size(); i < N; i++) {

            if( !placeholder[i].EqualTo("") ) {

                value[i].SetClassName(cl);
                value[i].Init( placeholder[i] );
                TPrint::Debug(10, __METHOD_NAME__, Form("Arg \"%s\"[%d] default value found %s", name.Data(), i, placeholder[i].Data()));
            }
        }
    }

    if(expected_size != -1 && value.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"%s\"";
        eMessage += ", " + TString::Itoa(value.size(), 10) + " element(s) received";
        TPrint::Error(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    return true;
}

bool TCli::IsArgAlreadyDefined(TString argname) {

    std::vector<TString> argname0_array;
    for(int i = 0, N = this->vArgName.size(); i < N; i++) {

        std::vector<TString> array = TVarexp(this->vArgName[i]).Tokenize('|');
        for(int j = 0, J = array.size(); j < J; j++) array[j].ToLower();
        
        argname0_array.insert(argname0_array.begin(), array.begin(), array.end());
    }

    TObjArray *argname_array = argname.Tokenize("|");
    for (int i = 0; i < argname_array->GetEntries(); i++) {

        TString optname = TPrint::Trim(((TObjString *)(argname_array->At(i)))->String());
        TString optNameLC = optname;
                optNameLC.ToLower();

        if( find(argname0_array.begin(), argname0_array.end(), optNameLC) != argname0_array.end()) {

            TString eMessage = "Option \""+optname+"\" already added.. You cannot have a duplicated option..";
            TPrint::Error(__METHOD_NAME__, eMessage);
            this->kUsage = false;
            return 1;
        }

    }

    return 0;
}

bool TCli::AddOption(TString name, TString description, TVector2 &value, TString placeholder_str, char delimiter)
{
    int expected_size = 2;
    TVector2 placeholder;

    TObjArray *array = placeholder_str.Tokenize(delimiter);
    if(array->GetEntries() > (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"" + name + "\"";
        eMessage += ", " + TString::Itoa(array->GetEntries(), 10) + " element(s) received";
        TPrint::Error(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    placeholder.SetX( array->GetEntries() > 0 ? ROOT::IOPlus::Helpers::InterpretFormula(((TObjString *)(array->At(0)))->String()) : 0);
    placeholder.SetY( array->GetEntries() > 1 ? ROOT::IOPlus::Helpers::InterpretFormula(((TObjString *)(array->At(1)))->String()) : 0);

    delete array;
    array = NULL;

    return TCli::AddOption(name, description, value, placeholder, delimiter);
}

bool TCli::AddOption(TString name, TString description, TVector2 &value, std::vector<double> placeholder_vector, char delimiter)
{
    int expected_size = 2;
    TVector2 placeholder;

    if(placeholder_vector.size() > (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"" + name + "\"";
        eMessage += ", " + TString::Itoa(placeholder_vector.size(), 10) + " element(s) received";
        TPrint::Error(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    placeholder.SetX( placeholder_vector.size() > 0 ? placeholder_vector[0] : 0);
    placeholder.SetY( placeholder_vector.size() > 1 ? placeholder_vector[1] : 0);

    return TCli::AddOption(name, description, value, placeholder, delimiter);
}

bool TCli::AddOption(TString name, TString description, TVector2 &value, TVector2 placeholder, char delimiter)
{
    TString eMessage = "Variadic argument already declared and goes last.";
    if( TPrint::ErrorIf(std::find(this->vArgType.begin(), this->vArgType.end(), ParameterType::Variadic) != this->vArgType.end(), __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Please declare all optional arguments first..";
    if( TPrint::ErrorIf(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( TPrint::ErrorIf(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        TPrint::Error(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from TCli..");
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgPlaceholder.push_back(description);
    this->vArgType.push_back(ParameterType::Optional);
    this->vArgColor.push_back(TPrint::kNoColor);

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1 && this->lastOptionPos > arg_index) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            TPrint::Error(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str0 = this->argv[arg_index+1];

        TObjArray *array = str0.Tokenize(delimiter);
        value.SetX( array->GetEntries() > 0 ? ROOT::IOPlus::Helpers::InterpretFormula(((TObjString *)(array->At(0)))->String()) : placeholder.X());
        value.SetY( array->GetEntries() > 1 ? ROOT::IOPlus::Helpers::InterpretFormula(((TObjString *)(array->At(1)))->String()) : placeholder.Y());

        delete array;
        array = NULL;

    } else {

        value.SetX( placeholder.X() );
        value.SetY( placeholder.Y() );
    }

    return true;
}

bool TCli::AddOption(TString name, TString description, TVector3 &value, TString placeholder_str, char delimiter)
{
    int expected_size = 3;
    TVector3 placeholder;

    TObjArray *array = placeholder_str.Tokenize(delimiter);
    if(array->GetEntries() > (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"" + name + "\"";
        eMessage += ", " + TString::Itoa(array->GetEntries(), 10) + " element(s) received";
        TPrint::Error(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    placeholder.SetX( array->GetEntries() > 0 ? ROOT::IOPlus::Helpers::InterpretFormula(((TObjString *)(array->At(0)))->String()) : 0);
    placeholder.SetY( array->GetEntries() > 1 ? ROOT::IOPlus::Helpers::InterpretFormula(((TObjString *)(array->At(1)))->String()) : 0);
    placeholder.SetZ( array->GetEntries() > 2 ? ROOT::IOPlus::Helpers::InterpretFormula(((TObjString *)(array->At(2)))->String()) : 0);

    delete array;
    array = NULL;

    return TCli::AddOption(name, description, value, placeholder, delimiter);
}

bool TCli::AddOption(TString name, TString description, TVector3 &value, std::vector<double> placeholder_vector, char delimiter)
{
    int expected_size = 3;
    TVector3 placeholder;

    if(placeholder_vector.size() > (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size, 10) + " element(s) expected for option \"" + name + "\"";
        eMessage += ", " + TString::Itoa(placeholder_vector.size(), 10) + " element(s) received";
        TPrint::Error(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    placeholder.SetX( placeholder_vector.size() > 0 ? placeholder_vector[0] : 0);
    placeholder.SetY( placeholder_vector.size() > 1 ? placeholder_vector[1] : 0);
    placeholder.SetZ( placeholder_vector.size() > 2 ? placeholder_vector[2] : 0);

    return TCli::AddOption(name, description, value, placeholder, delimiter);
}

bool TCli::AddOption(TString name, TString description, TVector3 &value, TVector3 placeholder, char delimiter)
{
    TString eMessage = "Variadic argument already declared and goes last.";
    if( TPrint::ErrorIf(std::find(this->vArgType.begin(), this->vArgType.end(), ParameterType::Variadic) != this->vArgType.end(), __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Please declare all optional arguments first..";
    if( TPrint::ErrorIf(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( TPrint::ErrorIf(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        TPrint::Error(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from TCli..");
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgPlaceholder.push_back(description);
    this->vArgType.push_back(ParameterType::Optional);
    this->vArgColor.push_back(TPrint::kNoColor);

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1 && this->lastOptionPos > arg_index) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            TPrint::Error(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str0 = this->argv[arg_index+1];

        TObjArray *array = str0.Tokenize(delimiter);
        value.SetX( array->GetEntries() > 0 ? ROOT::IOPlus::Helpers::InterpretFormula(((TObjString *)(array->At(0)))->String()) : placeholder.X());
        value.SetY( array->GetEntries() > 1 ? ROOT::IOPlus::Helpers::InterpretFormula(((TObjString *)(array->At(1)))->String()) : placeholder.Y());
        value.SetZ( array->GetEntries() > 2 ? ROOT::IOPlus::Helpers::InterpretFormula(((TObjString *)(array->At(2)))->String()) : placeholder.Z());

        delete array;
        array = NULL;

    } else {

        value.SetX( placeholder.X() );
        value.SetY( placeholder.Y() );
        value.SetZ( placeholder.Z() );
    }

    return true;
}

bool TCli::AddOption(TString name, TString description, TString &value, TString placeholder)
{
    TString eMessage = "Variadic argument already declared and goes last.";
    if( TPrint::ErrorIf(std::find(this->vArgType.begin(), this->vArgType.end(), ParameterType::Variadic) != this->vArgType.end(), __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Please declare all optional arguments first..";
    if( TPrint::ErrorIf(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( TPrint::ErrorIf(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        TPrint::Error(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from TCli..");
        this->kUsage = false;
        return false;
    }


    this->vArgName.push_back(name);
    this->vArgPlaceholder.push_back(description);
    this->vArgType.push_back(ParameterType::Optional);
    this->vArgColor.push_back(TPrint::kNoColor);

    if( !placeholder.EqualTo("") ) {

        value = placeholder;
        TPrint::Debug(10, __METHOD_NAME__, Form("Arg \"%s\" default value found %s", name.Data(), placeholder.Data()));
    }

    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1 && this->lastOptionPos > arg_index) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            TPrint::Error(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];

        value = str;
        TPrint::Debug(10, __METHOD_NAME__, Form("Arg \"%s\" found and set to \"%s\"", name.Data(), str.Data()));

        return true;
    }

    TPrint::Debug(10, __METHOD_NAME__, Form("Arg \"%s\" set to \"%s\"", name.Data(), value.Data()));
    return false;
}

bool TCli::AddOption(TString name, TString description, char &value, char placeholder)
{
    TString value_str;
    TString placeholder_str = (TString) placeholder;

    bool b = TCli::AddOption(name, description, value_str, placeholder_str);
    value = value_str[0];

    return b;
}

bool TCli::AddOption(TString name, TString description, TFileReader &value, TString placeholder, TClass *cl)
{
    TString eMessage = "Variadic argument already declared and goes last.";
    if( TPrint::ErrorIf(std::find(this->vArgType.begin(), this->vArgType.end(), ParameterType::Variadic) != this->vArgType.end(), __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Please declare all optional arguments first..";
    if( TPrint::ErrorIf(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( TPrint::ErrorIf(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgPlaceholder.push_back(description);
    this->vArgType.push_back(ParameterType::Optional);
    this->vArgColor.push_back(TPrint::kNoColor);

    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1 && this->lastOptionPos > arg_index) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            TPrint::Error(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }

        TString str = this->argv[arg_index+1];

        if(TFileReader::Obj(str).EqualTo("/")) {

            TString objname = TFileReader::Obj(placeholder);
            if(!objname.EqualTo("/")) {

                str += ":" + objname;
            }
        }
    
        value.SetClassName(cl);
        value.Init(str);
        return true;
    }

    return true;
}

bool TCli::AddArgument(std::map<TString, double>& value, TString placeholder, char delimiter, char delimiterKey, TCli::ParameterType type)
{
    this->vArgName.push_back("");
    this->vArgPlaceholder.push_back(placeholder);
    this->vArgType.push_back(type);
    this->vArgColor.push_back((type == ParameterType::Mandatory) ? TPrint::kRed : TPrint::kNoColor);
    this->bArgOptionLock = true;

    std::vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(type == ParameterType::Mandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    std::vector<TString> vv = TFileReader::SplitString(str, delimiter);
    for(int i = 0, N = vv.size(); i < N; i++) {

        std::vector<TString> keyvalue = TFileReader::SplitString(vv[i], delimiterKey);
        if(keyvalue.size() != 2) {
            
            TString eMessage  = Form("\"%s\" is an invalid value and cannot be formatted using \"%c\" delimiter for argument #%d", keyvalue[0].Data(), delimiterKey, vArgNotUsed[0]-1);
            TPrint::Error(__METHOD_NAME__, eMessage);

            this->kUsage = false;
            return false;
        }
    
        value.insert({keyvalue[0], ROOT::IOPlus::Helpers::InterpretFormula(keyvalue[1])});
    }


    this->vArgColor.pop_back();
    this->vArgColor.push_back(TPrint::kGreen);
    
    return true;
}

bool TCli::AddOption(TString name, TString description, std::map<TString, double> &value, std::map<TString, double>placeholder, char delimiter, char delimiterKey)
{
    TString eMessage = "Variadic argument already declared and goes last.";
    if( TPrint::ErrorIf(std::find(this->vArgType.begin(), this->vArgType.end(), ParameterType::Variadic) != this->vArgType.end(), __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Please declare all optional arguments first..";
    if( TPrint::ErrorIf(this->bArgOptionLock, __METHOD_NAME__, eMessage)) {

        this->kUsage = false;
        return false;
    }

    eMessage = "Option \""+name+"\" already added.. You cannot have a duplicated option..";
    if( TPrint::ErrorIf(this->IsArgAlreadyDefined(name), __METHOD_NAME__, eMessage) ) {

        TPrint::Error(__METHOD_NAME__, "If you do not find it in your option list. It might be in the default options already loaded from TCli..");
        this->kUsage = false;
        return false;
    }

    this->vArgName.push_back(name);
    this->vArgPlaceholder.push_back(description);
    this->vArgType.push_back(ParameterType::Optional);
    this->vArgColor.push_back(TPrint::kNoColor);

    // Set the proper value if found
    int arg_index = this->GetArgIndex(name);
    if(arg_index != -1 && this->lastOptionPos > arg_index) {

        this->vArgAlreadyUsed.push_back(arg_index);
        this->vArgAlreadyUsed.push_back(arg_index+1);

        if(!this->CheckNArg(arg_index+1)) {

            TPrint::Error(__METHOD_NAME__, "Wrong usage of argument \"%s\"", name.Data());
            this->kUsage = false;
            return false;
        }
        //if(!this->CheckArg(arg_index+1)) return false;
        TString str = this->argv[arg_index+1];
        std::vector<TString> vv = TFileReader::SplitString(str, delimiter);
        for(int i = 0, N = vv.size(); i < N; i++) {

            std::vector<TString> keyvalue = TFileReader::SplitString(vv[i], delimiterKey);
            if(keyvalue.size() != 2) {
                
                TString eMessage  = Form("\"%s\" is an invalid value and cannot be formatted using \"%c\" delimiter for option \"%s\"", keyvalue[0].Data(), delimiterKey, name.Data());
                TPrint::Error(__METHOD_NAME__, eMessage);

                this->kUsage = false;
                return false;
            }

            value.insert({keyvalue[0], ROOT::IOPlus::Helpers::InterpretFormula(keyvalue[1])});
        }

    } else {

        std::map<TString, double>::iterator it;
        for (it = placeholder.begin(); it != placeholder.end(); it++)
            value.insert({it->first, it->second});
    }

    return true;
}

bool TCli::AddOption(TString name, TString description, std::map<TString, double> &value, TString placeholder_str, char delimiter, char delimiterKey)
{
    std::map<TString, double> placeholder;

    std::vector<TString> vv = TFileReader::SplitString(placeholder_str, delimiter);
    for(int i = 0, N = vv.size(); i < N; i++) {

        std::vector<TString> keyvalue = TFileReader::SplitString(vv[i], delimiterKey);
        if(keyvalue.size() != 2) {
            
            TString eMessage  = Form("\"%s\" is an invalid value and cannot be formatted using \"%c\" delimiter for option \"%s\"", keyvalue[0].Data(), delimiterKey, name.Data());
            TPrint::Error(__METHOD_NAME__, eMessage);

            this->kUsage = false;
            return false;
        }

        placeholder.insert({keyvalue[0], ROOT::IOPlus::Helpers::InterpretFormula(keyvalue[1])});
    }
    
    return TCli::AddOption(name, description, value, placeholder, delimiter);
}


bool TCli::AddArgument(std::vector<TString>& v, TString placeholder, char delimiter, int expected_size, TCli::ParameterType type)
{
    this->vArgName.push_back("");
    this->vArgPlaceholder.push_back(placeholder);
    this->vArgType.push_back(type);
    this->vArgColor.push_back((type == ParameterType::Mandatory) ? TPrint::kRed : TPrint::kNoColor);
    this->bArgOptionLock = true;

    std::vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(type == ParameterType::Mandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    std::vector<TString> vv = TFileReader::SplitString(str, delimiter);
    if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size,10) + " std::vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
        eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
        TPrint::Error(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    this->vArgColor.pop_back();
    this->vArgColor.push_back(TPrint::kGreen);

    v.resize(vv.size());
    for(int j = 0; j < vv.size(); j++) v[j] = vv[j];

    return true;
}

bool TCli::AddArgument(std::vector<TFileReader>& v, TString placeholder, TClass *cl, char delimiter, int expected_size, TCli::ParameterType type)
{
    this->vArgName.push_back("");
    this->vArgPlaceholder.push_back(placeholder);
    this->vArgType.push_back(type);
    this->vArgColor.push_back((type == ParameterType::Mandatory) ? TPrint::kRed : TPrint::kNoColor);
    this->bArgOptionLock = true;

    std::vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(type == ParameterType::Mandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    std::vector<TString> vv = TFileReader::SplitString(str, delimiter);

    if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size,10) + " std::vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
        eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
        TPrint::Error(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    this->vArgColor.pop_back();
    this->vArgColor.push_back(TPrint::kGreen);

    v.resize(vv.size());
    for(int j = 0; j < vv.size(); j++) {

        if(TFileReader::Obj(vv[j]).EqualTo("/")) {

            TString objname = TFileReader::Obj(placeholder);
            TString eMessage = "Argument #" + TString::Itoa(vArgNotUsed[0], 10) + " has no objname.. Default value (=\""+objname+"\") will be used";
            TPrint::Debug(10, __METHOD_NAME__, eMessage);

            vv[j] += ":" + objname;
        }

        v[j].SetClassName(cl);
        v[j].Init(vv[j]);
    }

    return true;
}

bool TCli::AddVariadic(std::vector<TString>& v, TString placeholder, int min, int max)
{
    int N = this->GetArgNotUsed().size();
    if(min > max && max > 0) std::swap(min, max);

    do {

        this->vArgName.push_back("");
        this->vArgPlaceholder.push_back(placeholder);
        this->vArgType.push_back(ParameterType::Variadic);
        this->vArgColor.push_back( (max > 0 && N > max) || (N < min) ? TPrint::kOrange : TPrint::kNoColor );
        this->bArgOptionLock = true;

        std::vector<int> vArgNotUsed = this->GetArgNotUsed();
        int nargs = vArgNotUsed.size();
        if(!nargs) break;

        if( !this->CheckArg(vArgNotUsed[0]) ) return false;
        this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);
        
        TString str = this->argv[vArgNotUsed[0]];
        if(str.BeginsWith("-")) {
            this->kUsage = false;
            gPrint->Warning(__METHOD_NAME__, "Invalid variadic argument passed as parameter `%s`", str.Data());
            return false;
        }

        this->vArgPlaceholder[this->vArgPlaceholder.size()-1] = str;
        v.push_back(str);

        this->vArgColor.pop_back();
        this->vArgColor.push_back( TPrint::kGreen );

    } while(this->GetArgNotUsed().size() > 0);

    if(max > 0 && N > max) {
            gPrint->Warning(__METHOD_NAME__, "Too many variadic input provided. (maximum limit set to %d)", max);
            this->kUsage = false;
            return false;
    }

    if(N < min) {
         this->kUsage = false;
         return false;
    }

    return true;
}

bool TCli::AddVariadic(std::vector<TFileReader>& v, TString placeholder, int min, int max, TClass *cl)
{
    int N = this->GetArgNotUsed().size();
    if(min > max && max > 0) std::swap(min, max);

    do {

        this->vArgName.push_back("");
        this->vArgPlaceholder.push_back(placeholder);
        this->vArgType.push_back(ParameterType::Variadic);
        this->vArgColor.push_back( (max > 0 && N > max) || (N < min) ? TPrint::kOrange : TPrint::kNoColor );

        this->bArgOptionLock = true;

        std::vector<int> vArgNotUsed = this->GetArgNotUsed();
        int nargs = vArgNotUsed.size();
        if(!nargs) break;

        if( !this->CheckArg(vArgNotUsed[0]) ) return false;
        this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

        TString str = this->argv[vArgNotUsed[0]];
        v.resize(v.size()+1);
        v.back().SetClassName(cl);
        if(TFileReader::Obj(str).EqualTo("/")) {

            TString objname = TFileReader::Obj(placeholder);
            TString eMessage = "Argument #" + TString::Itoa(vArgNotUsed[0], 10) + " has no object.. Default value (=\""+objname+"\") will be used";
            TPrint::Debug(10, __METHOD_NAME__, eMessage);

            str += ":" + objname;
        }
    
        v.back().Init(str);

        this->vArgColor.pop_back();
        this->vArgColor.push_back( TPrint::kGreen );

    } while(this->GetArgNotUsed().size() > 0);

    if(max > 0 && N > max) {

            gPrint->Warning(__METHOD_NAME__, "Too many variadic input provided. (maximum limit set to %d)", max);
            this->kUsage = false;
            return false;
    }

    if(N < min) {
         this->kUsage = false;
         return false;
    }

    return true;
}

bool TCli::AddArgument(TVector2 &v, TString placeholder, char delimiter, TCli::ParameterType type)
{
    int expected_size = 2;
    
    this->vArgName.push_back("");
    this->vArgPlaceholder.push_back(placeholder);
    this->vArgType.push_back(type);
    this->vArgColor.push_back((type == ParameterType::Mandatory) ? TPrint::kRed : TPrint::kNoColor);
    this->bArgOptionLock = true;

    std::vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(type == ParameterType::Mandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    std::vector<TString> vv = TFileReader::SplitString(str, delimiter);
    if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size,10) + " std::vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
        eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
        TPrint::Error(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    this->vArgColor.pop_back();
    this->vArgColor.push_back(TPrint::kGreen);

    v.SetX(ROOT::IOPlus::Helpers::InterpretFormula(vv[0]));
    v.SetY(ROOT::IOPlus::Helpers::InterpretFormula(vv[1]));
    
    return true;
}

bool TCli::AddArgument(TVector3&v, TString placeholder, char delimiter, TCli::ParameterType type)
{
    int expected_size = 3;
    
    this->vArgName.push_back("");
    this->vArgPlaceholder.push_back(placeholder);
    this->vArgType.push_back(type);
    this->vArgColor.push_back((type == ParameterType::Mandatory) ? TPrint::kRed : TPrint::kNoColor);
    this->bArgOptionLock = true;

    std::vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(type == ParameterType::Mandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
    std::vector<TString> vv = TFileReader::SplitString(str, delimiter);
    if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

        TString eMessage  = TString::Itoa(expected_size,10) + " std::vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
        eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
        TPrint::Error(__METHOD_NAME__, eMessage);

        this->kUsage = false;
        return false;
    }

    this->vArgColor.pop_back();
    this->vArgColor.push_back(TPrint::kGreen);

    v.SetX(ROOT::IOPlus::Helpers::InterpretFormula(vv[0]));
    v.SetY(ROOT::IOPlus::Helpers::InterpretFormula(vv[1]));
    v.SetZ(ROOT::IOPlus::Helpers::InterpretFormula(vv[2]));
    
    return true;
}

bool TCli::AddArgument(TString &value, TString placeholder, TCli::ParameterType type)
{
    this->vArgName.push_back("");
    this->vArgPlaceholder.push_back(placeholder);
    this->vArgType.push_back(type);
    this->vArgColor.push_back((type == ParameterType::Mandatory) ? TPrint::kRed : TPrint::kNoColor);

    this->bArgOptionLock = true;
    std::vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(type == ParameterType::Mandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
            str = str.Strip(TString::kBoth);

    this->vArgColor.pop_back();
    this->vArgColor.push_back(TPrint::kGreen);

    value = str;
    return true;
}

bool TCli::AddArgument(TFileReader &value, TString placeholder, TClass *cl, TCli::ParameterType type)
{
    this->vArgName.push_back("");
    this->vArgPlaceholder.push_back(placeholder);
    this->vArgType.push_back(type);
    this->vArgColor.push_back((type == ParameterType::Mandatory) ? TPrint::kRed : TPrint::kNoColor);

    this->bArgOptionLock = true;
    std::vector<int> vArgNotUsed = this->GetArgNotUsed();
    int nargs = vArgNotUsed.size();
    if(nargs == 0) {

        if(type == ParameterType::Mandatory) this->kUsage = false;
        return false;
    }

    if( !this->CheckArg(vArgNotUsed[0]) ) return false;
    this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

    TString str = this->argv[vArgNotUsed[0]];
            str = str.Strip(TString::kBoth);

    this->vArgColor.pop_back();
    this->vArgColor.push_back(TPrint::kGreen);

    if(TFileReader::Obj(str).EqualTo("/")) {

        TString objname = TFileReader::Obj(placeholder);
        if(!objname.EqualTo("/")) {

            TString eMessage = "Argument #" + TString::Itoa(vArgNotUsed[0], 10) + " has no objname.. Default value (=\""+objname+"\") will be used";
            TPrint::Debug(10, __METHOD_NAME__, eMessage);

            str += ":" + objname;
        }
    }

    TPrint::RedirectTTY(__METHOD_NAME__);

    value.SetClassName(cl);
    value.Init(str);

    TString out, err;
    TPrint::GetTTY(__METHOD_NAME__, &out, &err);
    if(!out.EqualTo("") || !err.EqualTo("")) {
        bufferTTY[0].push_back(out);
        bufferTTY[1].push_back(err);
    }

    return true;
}

void TCli::Print(bool kIncrementTab)
{
    this->kPrintTCliOnDestroy = false;

    TString cli;
    TRegexp regex("^-[^0-9]+");
    for(int i = 1; i < this->argc; i++) {

        TString str = this->argv[i];
        if((i != 0 && (!str.Contains(regex)) || str.Contains(" "))) cli += "\"";

        if(str.Contains(regex)) cli += TPrint::kBlue + str + TPrint::kNoColor;
        else cli += str;

        if((i != 0 && (!str.Contains(regex)) || str.Contains(" "))) cli += "\""+TPrint::kNoColor+" ";
        else cli += " ";
    }

    int index = 0;
    bool bWithArgument = false;

    // When there is a parameter starting with "-" from previous instructions, it must have a HAndboxMsg::kNoColor tag..
    while((index = cli.Index(TPrint::kBlue, index)) != -1) {

        int subindex = cli.Index(TPrint::kBlue, index+2);

        TString substr = cli(index-1, subindex-index+1);
        if(substr.Contains("\"")) bWithArgument = true;

        if(bWithArgument) cli.Replace(index, 0, '\n');
        index += 3;
    }

    // Add some offset after a new line
    if(cli.BeginsWith("\"")) cli = TPrint::kPurple + cli;
    cli.ReplaceAll('\n', "\\\n");
    cli.ReplaceAll("\""+TPrint::kNoColor+" \"", 
                   "\""+TPrint::kNoColor+ " \\\n" + TPrint::kPurple + "\"");
    cli.ReplaceAll("\""+TPrint::kNoColor+" "+TPrint::kBlue+"-",
                   "\""+TPrint::kNoColor+" \\\n"+TPrint::kBlue+"-");
    cli += TPrint::kNoColor;

    // Print the remaining error message in the buffer..
    if(!kIncrementTab) TPrint::SetSingleSkipIncrementTab();
    TPrint::Info(gSystem->BaseName(this->argv[0]), cli);
    if(bWithArgument) std::cout << TPrint::Endl();
}
