#include <iostream>
#include <thread>
#include <fstream>
#include <string>

#include <TTensorD.h>
#include <TTensorF.h>

#ifdef __APPLE__
#include <sys/types.h>
#include <sys/sysctl.h>
#endif

void PrintCPUInfo() {

    // CPU information (platform-dependent)
    #ifdef __linux__
    // On Linux, we can read from /proc/cpuinfo
    std::ifstream cpuinfo("/proc/cpuinfo");
    std::string line;
    while (std::getline(cpuinfo, line)) {
        if (line.find("model name") != std::string::npos) {
            std::cout << line << std::endl;
            break;
        }
    }
    
    cpuinfo.close();
    #elif defined(__APPLE__)
    // For macOS, using sysctl
    char cpu_model[256];
    size_t size = sizeof(cpu_model);
    if (sysctlbyname("machdep.cpu.brand_string", &cpu_model, &size, nullptr, 0) == 0) {
        std::cout << "CPU Model: " << cpu_model << std::endl;
    } else {
        std::cerr << "Failed to get CPU model." << std::endl;
    }
    #endif

    // Get the number of CPU cores
    unsigned int num_cores = std::thread::hardware_concurrency();
    std::cout << "Number of CPU cores: " << num_cores << std::endl;
}


void PrintGPUInfo() {

    // Check MPS availability
    #if defined(__APPLE__)
        if (TTensorF::GetDeviceCount() > 0) std::cout << "MPS is available. (Float32 variables only)" << std::endl;
        else std::cout << "MPS is not available on this platform." << std::endl;
    #else
        if (TTensorD::GetDeviceCount() > 0) std::cout << "Number of CUDA GPUs: " << TTensorD::GetDeviceCount() << std::endl;
        else std::cout << "CUDA is not available on this platform." << std::endl;
    #endif
}

int main() {

    PrintCPUInfo();
    PrintGPUInfo();
    return 0;
}
