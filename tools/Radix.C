#include <TApplication.h>
#include <Riostream.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TLegend.h>
#include <TFile.h>
#include <TKey.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TEntryList.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TClass.h>

#include <TCli.h>
#include <ROOT/IOPlus/Helpers.h>

int main(int argc, char **argv)
{
    // Input variables for the CLI
    int baseInput = 10;
    std::vector<int> baseOutput;
    std::vector<TString> v;

    // Initialize the CLI interface
    TCli& cli = TCli::Instance(&argc, argv);
    cli.SkipHeader();
    cli.AddMessage("This script is used to convert numbers from one base to another.");
    cli.AddOption("--input",  "Input base", baseInput, 10);
    cli.AddOption("--output", "Output base", baseOutput);
    cli.AddVariadic(v, "", 1);  // Takes numbers as arguments

    if(!cli.Usage()) return 1;

    // Default output bases if not provided (binary, decimal, hexadecimal)
    if (baseOutput.size() == 0) baseOutput = {2, 10, 16};

    bool isNaN = false;

    for (int i = 0; i < v.size(); i++) {

        TString number = v[i];
        int number_base = baseInput;

        // Handle prefixes like 0b (binary), 0o (octal), 0x (hexadecimal)
        if (number.BeginsWith("0b")) {
            number_base = 2;
            number = number(2, number.Length());  // Remove "0b"
        } else if (number.BeginsWith("0o")) {
            number_base = 8;
            number = number(2, number.Length());  // Remove "0o"
        } else if (number.BeginsWith("0x")) {
            number_base = 16;
            number = number(2, number.Length());  // Remove "0x"
        } else if (number.BeginsWith("0d")) {
            number_base = 10;
            number = number(2, number.Length());  // Remove "0d"
        } else if (!number.EqualTo(TString::Itoa(number.Atoi(), 10))) {
            std::cerr << "NaN" << std::endl;
            isNaN = true;
            continue;
        }

        number.ToLower();
        number = ROOT::IOPlus::Helpers::StripLeadingZeros(number); 

        // Convert the number to a long integer based on the input base
        long long N = strtoll(number.Data(), nullptr, number_base);

        // Print the number in the specified output bases
        bool first = true;
        for(int base : baseOutput) {

            if (base == number_base) std::cout << TPrint::kGreen;

            int baseGroup;
            TString baseSpacer;
            switch(base) {
                case 2:
                    baseGroup = 4;
                    baseSpacer = " "; 
                    break;

                case 16:
                    baseGroup = 4;
                    baseSpacer = " "; 
                    break;

                case 10: [[fallthrough]];
                default:
                    baseGroup = 3;
                    baseSpacer = "'"; 
            }

            if (number.EqualTo(TString::Itoa(N, number_base))) {
                std::cout << (first ? "" : " = ") << "(" << ROOT::IOPlus::Helpers::FormatNumber(TString::Itoa(N, base), baseGroup, baseSpacer) << ")" << base;
            } else {
                std::cerr << (first ? "" : " = ") << "(NaN)" << base;
                isNaN = true;
            }

            if (base == number_base) std::cout << TPrint::kNoColor;
            first = false;
        }
        std::cout << std::endl;
    }

    return isNaN ? 1 : 0;
}