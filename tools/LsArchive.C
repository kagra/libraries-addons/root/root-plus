#include <Riostream.h>
#include <vector>
#include <TFile.h>
#include <TSystem.h>

#include <TFileReader.h>
#include <TCli.h>
#include <TArchive.h>

int main(int argc, char **argv) 
{
    TString destination;
    std::vector<TString> archivePaths;

    bool bListing;
    TCli& cli = TCli::Instance(&argc, argv);
            cli.AddMessage("This script is used to list the content of archive files..");
            cli.AddOption("--extract", "Extract into subdirectory", destination);
            cli.AddArgument(destination, "filename.tar.gz:/dirname");
            cli.AddVariadic(archivePaths, "filename.tar.gz:/dirname");

    if(!cli.Usage() ) return 1;

    // Add Input
    for(unsigned int i = 0, N = archivePaths.size(); i < N; i++) {
    
        TString aName = TFileReader::FileName(archivePaths[i]);
        TString memberPath = TFileReader::Obj(archivePaths[i]);

        TArchive myArchive(aName);
        if(myArchive.IsOpen()) {

            std::vector<TString> members = myArchive.GetListOfMembers();
            if(members.size() < 1) {
                std::cout << "`" << aName << "` is empty" << std::endl;
                continue;
            }

            std::cout << aName << std::endl;
            for(auto member : members) {
                std::cout << aName << ":./" << member << "" << std::endl;
            }
        }

        if(!destination.EqualTo("")) 
            myArchive.Extract(destination);
    }

    return 0;
}