#include <TApplication.h>
#include <Riostream.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TLegend.h>
#include <TFile.h>
#include <TKey.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TEntryList.h>
#include <TROOT.h>
#include <TFormula.h>
#include <TClass.h>

#include <TCli.h>
#include <ROOT/IOPlus/Helpers.h>

int main(int argc, char **argv)
{
    TString baseName = gSystem->BaseName(TString(argv[0]));
            baseName.ToLower();

    int base;
    std::vector<TString> v;
    TCli& cli = TCli::Instance(&argc, argv);
          cli.SkipHeader();
          cli.AddMessage("This script is used to convert numbers from one base to another..");
          cli.AddOption("--base", "Input base (if cannot be parsed)", base, 10);
          cli.AddVariadic(v, "", 1);

    if(!cli.Usage() ) return 1;

    if (baseName.EqualTo("bin") ) base = 2;
    if (baseName.EqualTo("octal") ) base = 8;
    if (baseName.EqualTo("dec") ) base = 10;
    if (baseName.EqualTo("hex") ) base = 16;

    bool isNaN = false;
    for(int i = 0; i < v.size(); i++) {

        TString number = v[i];
        int number_base = base;
        if (number.BeginsWith("0b")) {
            number_base = 2;
            number = number(2, number.Length());
        } else if (number.BeginsWith("0o")) {
            number_base = 8;
            number = number(2, number.Length());
        } else if (number.BeginsWith("0x")) {
            number_base = 16;
            number = number(2, number.Length());
        } else if (number.BeginsWith("0d")) {
            number_base = 10;
            number = number(2, number.Length());
        } else if (!number.EqualTo(TString::Itoa(v[i].Atoi(), 10))) {
            std::cerr << "NaN" << std::endl;
            isNaN = true;
            continue;
        }

        number.ToLower();
        number = ROOT::IOPlus::Helpers::StripLeadingZeros(number); 
        
        long int N = strtol(number.Data(), nullptr, number_base);
        if(number.EqualTo(TString::Itoa(N,number_base))) std::cout << N << std::endl;
        else {
            std::cerr << "NaN" << std::endl;
            isNaN = true;
        }
    }

    return isNaN;
}