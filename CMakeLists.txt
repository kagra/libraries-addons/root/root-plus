cmake_minimum_required(VERSION 3.20)

# Project information
set(PROJECT_TITLE "ROOT+ Library")
project(RIOPlus 
    VERSION 0.1
    DESCRIPTION "Library providing additional features to ROOT: Tensor calculation & Data streaming."
    HOMEPAGE_URL "https://git.ligo.org/kagra/libraries-addons/root/root-plus"
)

set(LIBRARY ${PROJECT_NAME} CACHE STRING "Library name")
set(LIBRARY_TITLE ${PROJECT_TITLE} CACHE STRING "Library title")

# Additional cmake features
add_compile_options(-Wextra -Wno-sign-compare -Wimplicit-fallthrough)
set(CMAKE_INSTALL_MESSAGE "LAZY")

# Check if git submodule are initialized in case you use custom cmake modules.
file(GLOB CMAKE_MODULE_PATHS "${CMAKE_CURRENT_SOURCE_DIR}/cmake/*")
foreach(MODULE_PATH ${CMAKE_MODULE_PATHS})
    if(IS_DIRECTORY ${MODULE_PATH})
        list(PREPEND CMAKE_MODULE_PATH ${MODULE_PATH})
    endif()
endforeach()

#
# Prepare project architecture
include(ProjectArchitecture)
include(FindPackageStandard)
include(BuildOptions)
include(DisplayMotd)

DISPLAY_MOTD()
MESSAGE_TITLE("${PROJECT_TITLE}")

update_build_options()
check_build_option(BUILD_IMAGE "Docker container won't be built.")
check_build_option(BUILD_DOCUMENTATION "Library documentation won't be built.")
check_build_option(BUILD_TESTS "Tests for this library won't be running.")
check_build_option(BUILD_LIBRARY "Library `${PROJECT_NAME}` will not be built.")
check_build_option(BUILD_EXAMPLES "Examples how-to-use `${PROJECT_NAME}` won't be computed.")
check_build_option(BUILD_TOOLS "Additional binary tools won't be computed.")

#
# Build project
if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/.env AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/cmake/.env.in)
    CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/.env.in ${CMAKE_CURRENT_SOURCE_DIR}/.env @ONLY)
endif()
if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/build AND NOT CMAKE_CURRENT_BINARY_DIR STREQUAL ${CMAKE_CURRENT_SOURCE_DIR}/build)
    FILE(CREATE_LINK ${CMAKE_BINARY_DIR_RELATIVE} ${CMAKE_CURRENT_SOURCE_DIR}/build SYMBOLIC)
endif()

#
# Create docker image (used in GitLab CI/CD for running tests)
if(BUILD_IMAGE AND EXISTS Dockerfile)
    include(DockerContainer)
endif()

#
# Additional restriction/warning feature
include(DisableInSourceBuild)
include(GitBranchWarning)

# Project overview
DUMP_PROJECT()

#
# Create a library
if(BUILD_LIBRARY)

    #
    # Load compiler
    include(StandardCompilerC)
    include(StandardCompilerCXX)

    #
    # Library dependencies
    find_package(ROOT REQUIRED COMPONENTS Gui Matrix)
    include(RootFramework)

    find_package(Torch REQUIRED)
    find_package(Date REQUIRED)
    find_package(Curl REQUIRED)
    find_package(Archive REQUIRED)

    #
    # Extension capability of iostream operator<< to vectors and matrices
    option(STD_IOMANIP_EXTEND "Extension of iomanip adding iostream capability for vectors and matrices" ON)
    if(STD_IOMANIP_EXTEND)
        message(STATUS "Extending `iomanip` capabilities for vectors and matrices.")
    else()
        message(STATUS "Extension for `iomanip` disabled.")
    endif()

    #
    # Extension capability of iostream operator<< to vectors and matrices
    option(STD_BACKTRACE_EXTEND "Enable backtrace and stack printing" ON)
    if(STD_BACKTRACE_EXTEND)
        message(STATUS "Extension for `backtrace` features enabled.")
    else()
        message(STATUS "Extension for `backtrace` features disabled.")
    endif()

    #
    # Extension capability of iostream operator<< to vectors and matrices
    option(STD_TUPLE_EXTEND "Extension of tuple adding tuple2,3,4 aliases" ON)
    if(STD_TUPLE_EXTEND)
        message(STATUS "Extension for `tuple` features enabled")
    else()
        message(STATUS "Extension for `tuple` features disabled.")
    endif()

    #
    # Extension capability of transform methods for APPLE computers (seq, unseq, parallel processing..)
    if(APPLE)
        option(STD_PSTLD_EXTEND "Extension of parallel capabilities using pstld" ON)
        if(STD_PSTLD_EXTEND)
            message(STATUS "Extending `parallel` capabilities using pstld.")
        else()
            message(STATUS "Extension for `parallel` disabled.")
        endif()
    endif()

    #
    # Prepare library including ROOT    
    FILE_SOURCES(SOURCES "src")
    if(NOT Curl_FOUND)
        LIST(FILTER SOURCES EXCLUDE REGEX ".*TCurl\.cc$")
    endif()

    if(NOT Archive_FOUND)
        LIST(FILTER SOURCES EXCLUDE REGEX ".*TArchive\.cc$")
    endif()

    FILE_HEADERS(HEADERS "include" RECURSE 
        EXCLUDE include/lib${LIBRARY}.LinkDef.h
        EXCLUDE ".*/Rioplus\.h$" REGEX
        EXCLUDE ".*/Impl/.*\.h$" REGEX
        EXCLUDE ".*/std/.*\.h$" REGEX
    )
    if(NOT APPLE) # Optional MacOS features
        LIST(FILTER HEADERS EXCLUDE REGEX ".*pstld\.h$")
    endif()

    if (TORCH_C_API)
    
        ROOT_ADD_LIBRARY(${LIBRARY}
            SOURCES ${SOURCES}
            HEADERS ${HEADERS}
            PACKAGES PRIVATE Date Archive Curl
            PACKAGES PUBLIC Torch
        )

    else()

        ROOT_ADD_LIBRARY(${LIBRARY}
            SOURCES ${SOURCES}
            HEADERS ${HEADERS}
            PACKAGES PRIVATE Date Archive Curl
            PACKAGES PRIVATE Torch 
        )

    endif()

    ROOT_INSTALL_LIBRARY(${LIBRARY})
    if(${CMAKE_MAINPROJECT})
        
        add_custom_target(lib DEPENDS ${LIBRARY} COMMENT "Compiling library")

        if(BUILD_TESTS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tests)
            add_subdirectory("tests")
        endif()

        if(BUILD_EXAMPLES AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/examples)
            add_subdirectory("examples")
        endif()

        if(BUILD_TOOLS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tools)
            add_subdirectory("tools")
        endif()
    endif()

endif()

#
# Prepare assets for documentation
if(BUILD_DOCUMENTATION AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/share/doxygen)
    add_subdirectory("share/doxygen")
endif()


#
# Hook Scripts
if(CMAKE_MAINPROJECT)

    # Make sure cmake includes are available
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/cmake 
            DESTINATION share 
            PATTERN "*.in" EXCLUDE 
            PATTERN ".*" EXCLUDE
            PATTERN "README*" EXCLUDE
    )

    # Post install message
    find_file(CMAKE_POST_CONFIG PostInstallConfig.cmake PATHS ${CMAKE_MODULE_PATHS})
    if(CMAKE_POST_CONFIG)
        install(SCRIPT "${CMAKE_POST_CONFIG}")
    endif()

    if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        check_target_dependencies()
    endif()

    # Post cmake message
    include(PostCMakeConfig)

endif()
