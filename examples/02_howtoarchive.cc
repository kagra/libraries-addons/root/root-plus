#include "TArchive.h"
#include "TSystem.h"
#include <iostream>

int main(int argc, char **argv)
{
    TString fname = TString(gSystem->DirName(argv[0])) + "/test.tar.gz"; // Replace with your URL

    // Set cache TTL to 1 hour (3600 seconds)
    TArchive mySafeArchive(fname, "READ");
    if(!mySafeArchive.IsOpen()) {
        std::cerr << "Failed to open `" << fname << "`" << std::endl;
        return 1;
    }

    // Copy existing archive to another place for edition.
    fname = "copy_test.tar.gz";
    mySafeArchive.Write(fname);
    mySafeArchive.Dump();
    mySafeArchive.Close();

    TArchive myArchive(fname, "READ");
    if(!myArchive.IsOpen()) {
        std::cerr << "Failed to open " << fname << std::endl;
        return 1;
    }
    myArchive.Dump();

    // Stream a single file
    std::ifstream myMember = myArchive.OpenMember("./test_01.txt");
    if(myMember.is_open()) {
        std::cout << "--> Open file `./test_01.txt` descriptor: " << &myMember << std::endl;
        myMember.close();
    }
    std::cout << std::endl;

    // Extract a single file
    TString swapFile = "./test_01.txt.swp";
    if (myArchive.ExtractMember("./test_01.txt", swapFile) ) {

        std::ifstream myExtractedMember = myArchive.OpenMember("./test_01.txt");
        if(myExtractedMember.is_open()) {
            std::cout << "--> Open member `./test_01.txt` descriptor: " << &myExtractedMember << std::endl;
            myExtractedMember.close();
        }
        std::cout << std::endl;
    }

    // Remove a single file
    myArchive.Remove("test_01.txt");
    myArchive.Dump();
    myArchive.Add("test_01.copy.txt", swapFile);
    myArchive.Dump();
    myArchive.Commit();
    gSystem->Unlink(swapFile);

    std::cout << std::endl;
    myArchive.Dump();
    myArchive.Write("output.tar.gz");
    myArchive.Dump();

    // Create a text file and add it to the archive
    TString testFile = "test_05.txt";
    FILE *fp = fopen(testFile, "w");
    if (!fp) return 1;
    fputs("Hello, file!\n", fp);
    fclose(fp);

    TString testFileMember = "./test_subdir/" + testFile;
    myArchive.Add(testFileMember, testFile);
    myArchive.Dump();
    myArchive.Commit();
    gSystem->Unlink(testFile);

    myArchive.Remove(testFileMember);
    myArchive.Dump();
    myArchive.Commit();

    // Final archive overview
    std::cout << std::endl;
    myArchive.Dump();

    // Extract it into a test directory..
    myArchive.Extract("./test_directory/");
    myArchive.Close();

    return 0;
}
