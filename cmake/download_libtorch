#!/bin/bash

# Function to detect operating system
detect_os() {
    case "$OSTYPE" in
        linux*)     OS="linux";;
        darwin*)    OS="macos";;
        msys*)      OS="windows";;
        *)          OS="unknown";;
    esac
}

# Function to detect machine architecture
detect_architecture() {
    ARCH=$(uname -m)
    case "$ARCH" in
        x86_64)     ARCH="x64";;
        aarch64)    ARCH="aarch64";;
        arm64)      ARCH="arm64";;
        *)          ARCH="unknown";;
    esac
}

# Function to detect the main CUDA version for PyTorch
map_to_pytorch_version() {
    local detected_version="$1"

    # If the version is greater than or equal to 124, use 124
    if [ "$detected_version" -ge 124 ]; then
        echo "124"
    # If the version is between 121 (inclusive) and 124 (exclusive), use 121
    elif [ "$detected_version" -ge 121 ] && [ "$detected_version" -lt 124 ]; then
        echo "121"
    # If the version is between 118 (inclusive) and 121 (exclusive), use 118
    elif [ "$detected_version" -ge 118 ] && [ "$detected_version" -lt 121 ]; then
        echo "118"
    # If the version is less than 118, use 118
    else
        echo "118"
    fi
}

# Function to get the detected CUDA version from nvcc
detect_cuda_version_from_nvcc() {
    if command -v nvcc &> /dev/null; then
        # Extract major.minor version and remove the dot (e.g., 12.6 -> 126)
	cuda_version=$(nvcc --version | sed -n 's/^.*release \([0-9]\+\.[0-9]\+\).*$/\1/p' | sed 's/\.//')
        # Call function to map detected version to PyTorch supported version
        mapped_version=$(map_to_pytorch_version "$cuda_version")
        echo "$mapped_version"
    else
        echo "cpu"  # Fallback if nvcc is not installed
    fi
}

# Function to construct device version
device_version() {
    local device="$1"

    # Remove dots from device version
    device="${device//./}"

    # Prepend 'cu' if not already included
    if [[ ! "$device" =~ ^cu ]]; then
        device="cu$device"
    fi

    echo "$device"
}

# Function to download LibTorch
download_libtorch() {
    local version="$1"
    local device="$2"
    local url=""

    case "$OS" in
        "linux")
            if [ "$device" == "cpu" ]; then
                url="https://download.pytorch.org/libtorch/cpu/libtorch-cxx11-abi-shared-with-deps-${version}%2Bcpu.zip"
            else
                url="https://download.pytorch.org/libtorch/cu${device}/libtorch-cxx11-abi-shared-with-deps-${version}%2Bcu${device}.zip"
            fi
            ;;
        "macos")
            url="https://download.pytorch.org/libtorch/cpu/libtorch-macos-${ARCH}-${version}.zip"
            ;;
        "windows")
            if [ "$device" == "cpu" ]; then
                url="https://download.pytorch.org/libtorch/cpu/libtorch-win-shared-with-deps-${version}%2Bcpu-${ARCH}.zip"
            else
                device=$(device_version "$device")
                url="https://download.pytorch.org/libtorch/cu${device}/libtorch-win-shared-with-deps-${version}%2Bcu${device}-${ARCH}.zip"
            fi
            ;;
        *)
            echo "Unsupported OS."
            exit 1
            ;;
    esac

    echo "Downloading LibTorch version: $version"
    wget "$url" -O libtorch.zip
    unzip libtorch.zip -d .
    rm libtorch.zip

    # Print commands to set environment variables
    case "$OS" in
        "linux")
            echo "export PATH=\"$PWD/libtorch/bin:\$PATH\""
            echo "export LD_LIBRARY_PATH=\"$PWD/libtorch/lib:\$LD_LIBRARY_PATH\""
            echo "export TORCH_DIR=\"$PWD/libtorch\""
            ;;
        "macos")
            echo "export PATH=\"$PWD/libtorch/bin:\$PATH\""
            echo "export DYLD_LIBRARY_PATH=\"$PWD/libtorch/lib:\$DYLD_LIBRARY_PATH\""
            echo "export TORCH_DIR=\"$PWD/libtorch\""
            ;;
        "windows")
            echo "Environment variable setting not supported on Windows in this script."
            ;;
        *)
            echo "Unsupported OS."
            exit 1
            ;;
    esac
}

# Main script logic
detect_os
detect_architecture
echo "Detected OS: $OS"
echo "Detected Architecture: $ARCH"

# Check if LibTorch version argument is provided
if [ -z "$1" ]; then
    echo "Error: LibTorch version not provided."
    echo "Usage: $0 <libtorch_version> [cuda_version]"
    exit 1
fi

# Detect GPU or use provided CUDA version
cuda_version="$(detect_cuda_version_from_nvcc)"
echo "CUDA Version: $cuda_version"

download_libtorch "$1" "$cuda_version"
echo "LibTorch installation completed."