# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

find_package_standard(
  NAMES libdate-tz
  HEADERS "date/date.h" "date/tz.h"
  PATHS ${LIBDATE} $ENV{LIBDATE} ${DATE} $ENV{DATE}
)

#
# Prefer to use default FindTorch, if available
get_filename_component(FILE_NAME ${CMAKE_CURRENT_LIST_FILE} NAME_WE)
if(${FILE_NAME} MATCHES "^Find(.+)$")

    set(FINDER ${CMAKE_MATCH_1})
    if(NOT ${FINDER}_FOUND)
      find_package_standard(
        NAMES libhowardhinnant-date-tz
        HEADERS "date/date.h" "date/tz.h"
        PATHS ${LIBDATE} $ENV{LIBDATE} ${DATE} $ENV{DATE}
      )
    endif()

endif()