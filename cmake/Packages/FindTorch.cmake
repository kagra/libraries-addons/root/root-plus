# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    find_package(TBB REQUIRED)
endif()

# Load using standard package finder
find_package_standard(
    NAMES torch
    HEADERS "torch/csrc/api/include/torch/torch.h"
    PATHS ${TORCH} $ENV{TORCH} ${LIBTORCH} $ENV{LIBTORCH} ${TORCH_DIR} $ENV{TORCH_DIR} ${Torch_DIR} $ENV{Torch_DIR}
)

#
# Prefer to use default FindTorch, if available
get_filename_component(FILE_NAME ${CMAKE_CURRENT_LIST_FILE} NAME_WE)
if(${FILE_NAME} MATCHES "^Find(.+)$")

    set(FINDER ${CMAKE_MATCH_1})
    if(${FINDER}_FOUND)

        set(${FINDER}_INCLUDE_DIRS "${${FINDER}_INCLUDE_DIRS}" "${${FINDER}_INCLUDE_DIRS}/torch/csrc/api/include")
        unset(${FINDER}_FOUND)

        list(PREPEND CMAKE_PREFIX_PATH "${${FINDER}_DIR}/share/cmake/Torch")
        list(PREPEND CMAKE_PREFIX_PATH "${${FINDER}_DIR}/share/cmake/ATen")
        list(PREPEND CMAKE_PREFIX_PATH "${${FINDER}_DIR}/share/cmake/Caffe2")
        list(PREPEND CMAKE_PREFIX_PATH "${${FINDER}_DIR}/share/cmake/Tensorpipe")

        set(_DIR ${${FINDER}_DIR})
        find_package(${FINDER} CONFIG REQUIRED QUIET)
        if(${FINDER}_FOUND)
            set(${FINDER}_LIBRARIES "${TORCH_LIBRARY}" "${TORCH_LIBRARIES}" "${TBB_LIBRARIES}")
            set(${FINDER}_INCLUDE_DIRS "${TORCH_INCLUDE_DIRS}" "${TBB_INCLUDE_DIRS}")
            set(${FINDER}_DIR "${${FINDER}_DIR}")
        endif()
    endif()
endif()
