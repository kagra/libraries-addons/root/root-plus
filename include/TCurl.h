#pragma once

#include "ROOT/IOPlus/TCurlFile.h"

class TCurl : public TCurlFile {
    
    public:
        using TCurlFile::TCurlFile;

        ~TCurl() {}

    ClassDef(TCurl, 1)
};