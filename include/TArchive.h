#pragma once

#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include <fstream>
#include <vector>

#include "TArchiveFile.h"

using TArchiveBuffer = std::map<TString, TString>;
class TArchive : public TArchiveFile {

    private:
        class Impl;
        Impl *pImpl = NULL;

        TString fOption;
        TString fCurrentMember;
        std::vector<TString> fArchiveMembers;

        TArchiveBuffer fArchiveBuffer;

    protected:
        mutable char *fChecksum = nullptr;
        int GetCurrentIndex();

    public:
        enum class CompressionFormat { UNKNOWN, NONE, GZIP, BZIP2, XZ, LZMA, LZOP, LZ4, ZSTD, ZIP};
        static TString enum2str(CompressionFormat);
        
        TArchive(const char *archiveName, Option_t *option = "", const char *member = "");
        ~TArchive();

        const char *GetName() const { return this->GetArchiveName(); }
        void SetName(const char *name) { this->fArchiveName = name; }

        Option_t *GetOption();
        void SetOption(Option_t *option);
        static CompressionFormat GetCompression(const char *);
        CompressionFormat GetCompression() const;

        Int_t OpenArchive();
        std::ifstream OpenMember(const char *member, bool binaryMode = false);

        TString GetCurrentMember();
        std::ifstream OpenCurrentMember(bool binaryMode = false);
        Int_t SetCurrentMember() { return this->SetCurrentMember(this->fCurrentMember); }
        Int_t SetCurrentMember(const char *member);

        bool IsOpen() const;
        Int_t Open();
        void Close();
        Int_t Reset();

        bool IsCompressed() const;

        Int_t Extract(const char *destination);
        bool ExtractMember(TString memberPath, TString destination);
        std::vector<TString> GetListOfMembers();

        Int_t Write(const char *newArchiveName, const CompressionFormat &format = CompressionFormat::UNKNOWN /* by default, automatic determination using new archive name */);
        Int_t Write(const char *newArchiveName, const CompressionFormat &format, TArchiveBuffer archiveBuffer);
        void Add(TString memberPath, TString fileName);
        void Remove(TString memberPath);
        void Rollback(const char *member = "");
        Int_t Commit();

        TString Next();
        int GetN();

        const char *Checksum() const;
        void Print(Option_t *opt = "") ;
        void Dump();

    ClassDef(TArchive, 1) // TArchive class for downloading files from URLs
};
