#pragma once

#include "ROOT/IOPlus/TTensorT.h"
#include "ROOT/IOPlus/Complex.h"

class TTensorC : public ROOT::IOPlus::TTensorT<complex64> {

    public:
        using ROOT::IOPlus::TTensorT<complex64>::TTensorT;

    ClassDef(TTensorC, 1);  // ROOT Class Definition
};
