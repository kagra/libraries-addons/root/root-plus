/**
 * @file TCli.h
 * @brief Header of the TCli class
 * @author Marco Meyer <marco.meyer@cern.ch>
 * @date 2017-08-11
 * @version 1.0
 *
 * *********************************************
 *
 * @class TCli
 * @brief Description of the TCli class.
 * @details Detailed description of the TCli class, its functionality, and usage.
 */

#pragma once

#include <signal.h>

#include <Riostream.h>
#include <vector>
#include <TH1.h>
#include <TF1.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TApplication.h>
#include <TObject.h>
#include <TLorentzVector.h>
#include <TPRegexp.h>
#include <TBrowser.h>
#include <TCanvas.h>
#include <TVector.h>
#include <TVector2.h>
#include <TVector3.h>
#include <TTime.h>

#include "TTree.h"
#include "TPrint.h"
#include "TFileReader.h"
#include "TVarexp.h"


/*
 * Avoid to start root and create a TBrowser session
 */
class TCliBrowser: public TBrowser {

        public:
        TCliBrowser() : TBrowser() {}
        virtual ~TCliBrowser() { gApplication->Terminate(); }
};

class TCli //: public TApplication (@TODO: comply with TApplication in a proper way?)
{
        public:

                enum class Verbosity {VERBOSITY_QUIET = -1, VERBOSITY_NORMAL = 0, VERBOSITY_VERBOSE = 1, VERBOSITY_VERY_VERBOSE = 10, VERBOSITY_DEBUG = 100};
                static void enum2str() { std::invalid_argument("Invalid argument converting enum to string"); }
                static TString enum2str(Verbosity verbosity) {

                        switch(verbosity) {

                                case Verbosity::VERBOSITY_QUIET: return "Quiet";
                                case Verbosity::VERBOSITY_NORMAL: return "Normal";
                                case Verbosity::VERBOSITY_VERBOSE: return "Verbose";
                                case Verbosity::VERBOSITY_VERY_VERBOSE: return "Very verbose";
                                case Verbosity::VERBOSITY_DEBUG: return "Debugging";
                                default: enum2str();
                        }
                }

                enum class ParameterType { Mandatory, Optional, Variadic};

        private:

                TCli(int *newArgc, char **newArgv, Option_t *newOption) { Init(newArgc, newArgv, newOption); }
                TCli(const TCli&) = delete;
                TCli& operator=(const TCli&) = delete;

        public:
                static bool kNoHeader;
        protected:

                int iarg, argc;
                std::vector<TString> argv;

                bool kUsage;
                bool kPrintTCliOnDestroy;
                TString usage_msg;

                Option_t *option;

                std::vector<TString> vTitle;
                std::vector<int> vTitlePos;

                bool bArgOptionLock;
                std::vector<TString>  vArgName;
                std::vector<TString>  vArgColor;
                std::vector<ParameterType>  vArgType;
                std::vector<TString> vArgPlaceholder;

                std::vector<int>  vArgAlreadyUsed;

                std::vector<TString> bufferTTY[2];
                TString WrongUsageStr;

        public:
                static int iPadTitle;

                static TApplication *fApp;
                static TBrowser *fBrowser;
                static bool bGraphicsLoop;
                static bool bOpenGraphics;
                static int lastOptionPos;

                static void EnableGraphicsHandler();
                static void DisableSignalHandler();
                static void GraphicsHandler(int);

                static void EnableMT(int nThreads = 0);
                static void DisableMT();
                static bool IsParallel(int nThreads = 0);
                static bool HasMT(int nThreads = 0);
                static int  GetMTPoolSize();
                static int  GetNCores();

                static bool IsBatch();
                static bool IsInteractive();

                static bool HasGraphics(Option_t *opt = "");
                static void OpenGraphics(Option_t *opt = "g");
                static void WaitForGraphicsToBeClosed(Option_t *opt = "");
                static void CloseGraphics();

                static const bool kReverseLogic;

                // static const bool kUseDefault;
                // static const bool kNotUseDefault;
                
                // Constructor, destructor
                static TCli& Instance(int *newArgc = 0, char **newArgv = NULL, Option_t *newOption = "") {
                
                        static TCli _Instance(newArgc, newArgv, newOption);
                        if(_Instance.argc == 0) _Instance.Init(newArgc, newArgv, newOption);
                        return _Instance;
                };

                virtual ~TCli() { gPrint->EmptyTrash(); }

                void Init(int *, char **, Option_t *);

                // TCli functions and arg manager
                void AddTitle(TString);
                void AddMessage(TString);
                
                inline void SkipHeader() { this->kNoHeader = true; }

                bool Valid(bool = 0);
                inline bool Usage(bool b = 0) { return Valid(b); }
                void WrongUsage(TString str = "") {

                        if(!str.EqualTo("")) WrongUsageStr += gPrint->kRed + str + gPrint->kNoColor + '\n';
                        kUsage = false;
                }

                bool IsArgAlreadyDefined(TString);

                inline bool Status() {
                        return kUsage;
                }

                bool Help(int = 0);
                bool CheckArg(int);

                std::vector<int> GetArgNotUsed();
                int GetArgIndex(TString);

                template<typename T>
                bool AddArgument(T &value, TString placeholder, ParameterType type = ParameterType::Mandatory)
                {
                        this->vArgName.push_back("");
                        this->vArgPlaceholder.push_back(placeholder);
                        this->vArgType.push_back(type);
                        this->vArgColor.push_back((type == ParameterType::Mandatory) ? gPrint->kRed : gPrint->kNoColor);
                        this->bArgOptionLock = true;

                        std::vector<int> vArgNotUsed = this->GetArgNotUsed();
                        int nargs = vArgNotUsed.size();
                        if(nargs == 0) {

                                if(type == ParameterType::Mandatory) this->kUsage = false;
                                return false;
                        }

                        if( !this->CheckArg(vArgNotUsed[0]) ) return false;
                        this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

                        this->vArgColor.pop_back();
                        this->vArgColor.push_back(gPrint->kGreen);

                        if constexpr (std::is_same<T, bool>::value) {

                                TString str = this->argv[vArgNotUsed[0]];
                                        str.ToLower();
                                        str = str.Strip(TString::kBoth);

                                value = !str.EqualTo("0") && !str.EqualTo("no") && !str.EqualTo("false") && !str.EqualTo("off") && !str.EqualTo("");

                        } else if constexpr (std::is_same<T, int>::value) {
                                value = (int) ROOT::IOPlus::Helpers::InterpretFormula(this->argv[vArgNotUsed[0]]);
                        } else if constexpr (std::is_same<T, float>::value) {
                                value = ROOT::IOPlus::Helpers::InterpretFormula(this->argv[vArgNotUsed[0]]);
                        } else if constexpr (std::is_same<T, double>::value) {
                                value = ROOT::IOPlus::Helpers::InterpretFormula(this->argv[vArgNotUsed[0]]);
                        } else if constexpr (std::is_same<T, const char*>::value) {
                                value = this->argv[vArgNotUsed[0]];
                        }

                        return true;
                }
                
                bool AddArgument(TString&,     TString, TCli::ParameterType = TCli::ParameterType::Mandatory);
                bool AddArgument(TFileReader&, TString, TClass *cl = NULL, TCli::ParameterType = TCli::ParameterType::Mandatory);
                bool AddArgument(TVector2&,    TString, char = ' ', TCli::ParameterType = TCli::ParameterType::Mandatory);
                bool AddArgument(TVector3&,    TString, char = ' ', TCli::ParameterType = TCli::ParameterType::Mandatory);
                
                template<typename T>
                bool AddArgument(std::vector<T>& v, TString placeholder, char delimiter = ' ', int expected_size = -1, ParameterType type = ParameterType::Mandatory)
                {
                        this->vArgName.push_back("");
                        this->vArgPlaceholder.push_back(placeholder);
                        this->vArgType.push_back(type);
                        this->vArgColor.push_back((type == ParameterType::Mandatory) ? gPrint->kRed : gPrint->kNoColor);
                        this->bArgOptionLock = true;

                        std::vector<int> vArgNotUsed = this->GetArgNotUsed();
                        int nargs = vArgNotUsed.size();

                        if(nargs == 0) {

                                if(type == ParameterType::Mandatory) this->kUsage = false;
                                return false;
                        }

                        if(!this->CheckArg(vArgNotUsed[0])) return false;
                        this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

                        TString str = this->argv[vArgNotUsed[0]];
                        std::vector<TString> vv = TFileReader::SplitString(str, delimiter);
                        if(expected_size != -1 && vv.size() != (unsigned int) expected_size) {

                                TString eMessage  = TString::Itoa(expected_size,10) + " std::vector element(s) expected for argument #" + TString::Itoa(vArgNotUsed[0]-1,10);
                                eMessage += ", " + TString::Itoa(vv.size(), 10) + " element(s) received";
                                gPrint->Error(__METHOD_NAME__, eMessage);

                                this->kUsage = false;
                                return false;
                        }

                        this->vArgColor.pop_back();
                        this->vArgColor.push_back(gPrint->kGreen);

                        v.resize(vv.size());
                        for(int j = 0; j < vv.size(); j++) {
                        
                                if constexpr (std::is_same<T, bool>::valu) {

                                        str = TString(vv[j]);
                                        str.ToLower();
                                                str = str.Strip(TString::kBoth);

                                        v[j] = (!str.EqualTo("0")) && (!str.EqualTo("no")) && (!str.EqualTo(""));
                                } else if constexpr (std::is_same<T, int>::valu) {
                                        v[j] = (int) ROOT::IOPlus::Helpers::InterpretFormula(vv[j]);
                                } else if constexpr (std::is_same<T, float>::valu) {
                                        v[j] = ROOT::IOPlus::Helpers::InterpretFormula(vv[j]);
                                } else if constexpr (std::is_same<T, double>::valu) {
                                        v[j] = ROOT::IOPlus::Helpers::InterpretFormula(vv[j]);
                                } else if constexpr (std::is_same<T, const char*>::valu) {
                                        v[j] = vv[j];
                                }
                        }

                        return true;
                }

                bool AddArgument(std::vector<TString>&,     TString, char = ' ', int = -1, TCli::ParameterType = TCli::ParameterType::Mandatory);
                bool AddArgument(std::vector<TFileReader>&, TString, TClass *cl = NULL, char = ' ', int = -1, TCli::ParameterType = TCli::ParameterType::Mandatory);
                bool AddArgument(std::map<TString, double>&, TString, char = ';', char = ':', TCli::ParameterType = TCli::ParameterType::Mandatory);

                template<typename T>
                bool AddVariadic(std::vector<T>& v, TString placeholder, int min = 0, int max = 0)
                {
                        int N = this->GetArgNotUsed().size();
                        if(min > max && max > 0) std::swap(min, max);
                        do {

                                this->vArgName.push_back("");
                                this->vArgPlaceholder.push_back(placeholder);
                                this->vArgType.push_back(ParameterType::Variadic);
                                this->vArgColor.push_back(gPrint->kNoColor);
                                this->bArgOptionLock = true;

                                std::vector<int> vArgNotUsed = this->GetArgNotUsed();
                                int nargs = vArgNotUsed.size();
                                if(!nargs) break;

                                if( !this->CheckArg(vArgNotUsed[0]) ) return false;
                                this->vArgAlreadyUsed.push_back(vArgNotUsed[0]);

                                if constexpr (std::is_same<T, bool>::value) {
                                
                                        TString str = this->argv[vArgNotUsed[0]];
                                                str.ToLower();
                                                str = str.Strip(TString::kBoth);

                                        v.push_back(!str.EqualTo("0") && !str.EqualTo("no") && !str.EqualTo("false") && !str.EqualTo("off") && !str.EqualTo(""));

                                } else if constexpr (std::is_same<T, int>::value) {
                                        v.push_back((int) ROOT::IOPlus::Helpers::InterpretFormula(this->argv[vArgNotUsed[0]]));
                                } else if constexpr (std::is_same<T, double>::value) {
                                        v.push_back(ROOT::IOPlus::Helpers::InterpretFormula(this->argv[vArgNotUsed[0]]));
                                } else if constexpr (std::is_same<T, float>::value) {
                                        v.push_back(ROOT::IOPlus::Helpers::InterpretFormula(this->argv[vArgNotUsed[0]]));
                                } else if constexpr (std::is_same<T, const char*>::value) {
                                        v.push_back(this->argv[vArgNotUsed[0]]);
                                } else {
                                       throw new std::runtime_error("Unexpected variadic template type provided.");
                                }

                                this->vArgColor.pop_back();
                                this->vArgColor.push_back(gPrint->kGreen);

                        } while(this->GetArgNotUsed().size() > 0);

                        if(max > 0 && N > max) {
                                gPrint->Warning(__METHOD_NAME__, "Too many variadic input provided. (expected: %d)", max);
                                this->kUsage = false;
                                return false;
                        }

                        if(N < min) {
                                this->kUsage = false;
                                return false;
                        }

                        return true;
                }

                bool AddVariadic(std::vector<TString>&, TString, int = 0, int = 0);
                bool AddVariadic(std::vector<TFileReader>&, TString, int = 0, int = 0, TClass *cl = NULL);

                bool AddOption(TString, TString, bool &, bool = !TCli::kReverseLogic);
                bool AddOption(TString, TString, std::vector<bool>&, std::vector<bool>, char = ' ', int = -1);

                bool AddOption(TString, TString, char &, char = '\0');

                bool AddOption(TString, TString, int &, double = INFINITY);
                bool AddOption(TString, TString, float &, double = INFINITY);
                bool AddOption(TString, TString, double &, double = INFINITY);

                bool AddOption(TString, TString, int &, TString);
                bool AddOption(TString, TString, float &, TString);
                bool AddOption(TString, TString, double &, TString);
                bool AddOption(TString, TString, TString &, TString = "");
                bool AddOption(TString, TString, TFileReader &, TString = "", TClass *cl = NULL);

                bool AddOption(TString, TString, std::vector<int>&, TString = "", char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<float>&, TString = "", char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<double>&, TString = "", char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<TString>&, std::vector<TString>, char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<TFileReader>&, std::vector<TString>, TClass *cl = NULL, char = ' ', int = -1);

                bool AddOption(TString, TString, std::vector<int>&, std::vector<int>, char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<float>&, std::vector<float>, char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<double>&, std::vector<double>, char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<TString>&, TString = "", char = ' ', int = -1);
                bool AddOption(TString, TString, std::vector<TFileReader>&, TString, TClass *cl = NULL, char = ' ', int = -1);

                bool AddOption(TString, TString, TVector2 &, TVector2   , char = ' ');
                bool AddOption(TString, TString, TVector2 &, std::vector<double>, char = ' ');
                bool AddOption(TString, TString, TVector2 &, TString = "", char = ' ');
                bool AddOption(TString, TString, TVector3 &, TVector3   , char = ' ');
                bool AddOption(TString, TString, TVector3 &, std::vector<double>, char = ' ');
                bool AddOption(TString, TString, TVector3 &, TString = "", char = ' ');

                bool AddOption(TString, TString, std::map<TString, double> &, std::map<TString, double>, char = '\n', char = ':');
                bool AddOption(TString, TString, std::map<TString, double> &, TString = "", char = '\n', char = ':');

                int GetArgc() {
                        return argc;
                }

                int CheckNArg(int argi) {

                        return !(argc <= argi);
                }

                void Print(bool = false);
};
