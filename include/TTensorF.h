#pragma once

#include "ROOT/IOPlus/TTensorT.h"

class TTensorF : public ROOT::IOPlus::TTensorT<float> {

    public:
        using ROOT::IOPlus::TTensorT<float>::TTensorT;

    ClassDef(TTensorF, 1);  // ROOT Class Definition
};