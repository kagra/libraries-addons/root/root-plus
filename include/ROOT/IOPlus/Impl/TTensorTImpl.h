#pragma once

#include "../TTensorT.h"

#ifdef __APPLE__
    #ifdef RAW
    #undef RAW
    #endif
#endif

#include <torch/torch.h>

#include <vector>
#include <tuple>

#include "../std/execution.h"

namespace ROOT { namespace IOPlus {

    template<typename Element>
    class TTensorT<Element>::Impl : public torch::Tensor  {

        friend class TTensorT<Element>;

        protected:

            using torch::Tensor::Tensor;
            Impl(const Impl& impl);
            Impl(const torch::Tensor& tensor);
            Impl(const std::vector<int64_t> &sizes = {}, TTensorDevice device = TTensorDevice::Auto, TTensorLayout layout = TTensorLayout::Stride, const bool &required_grad = false): Impl(at::IntArrayRef(sizes), device, layout, required_grad) {}
            Impl(const at::IntArrayRef &sizes, TTensorDevice device = TTensorDevice::Auto, TTensorLayout layout = TTensorLayout::Stride, const bool &required_grad = false);

            Impl& operator=(const Impl& other);

        public:
            virtual ~Impl() {}

            static const char *version();

            // Function to return torch::ScalarType based on Element type
            static torch::ScalarType dtype();
            static torch::ScalarType dtype_t();

            // Function to return tensor without copying data
            static TTensorT<Element>::Impl from_blob(const std::vector<Element>& data, torch::IntArrayRef sizes = {});

            template<typename T, typename std::enable_if<!std::is_same<T, Element>::value, int>::type = 0>
            static TTensorT<Element>::Impl from_blob(const std::vector<T>& data, torch::IntArrayRef sizes) {
                return torch::from_blob((void *) data.data(), sizes, Impl::dtype_t());
            }

            // Function to determine and return torch::Device based on GPU availability
            static int device_count();

            // Function to determine and return torch::Device based on GPU availability
            static torch::Device get_device(TTensorDevice device = TTensorDevice::Auto);
            static TTensorDevice get_device(torch::Device device);

            // Function to return tensor layout
            static torch::Layout get_layout(TTensorLayout layout);

            // Function to return tensor normalization when computing FFT
            static TString get_norm(const TTensorNorm &norm);

            // Define the set_stride method
            void set_stride(size_t i, size_t stride);
    };
}}
