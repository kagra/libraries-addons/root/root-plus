#pragma once 

#include <curl/curl.h>
#include <sys/stat.h>
#include <memory>

#include <TMD5.h>
#include <TSystem.h>
#include <TUrl.h>

#include "ROOT/IOPlus/TCurlFile.h"
#include "TFileReader.h"
#include "TClock.h"

class TCurlFile::Impl {

    friend class TCurlFile;
    protected:
        
        CURL* curl = NULL;
        
        mutable char *error;
        CURLcode errorCode = CURLE_OK;

        std::shared_ptr<TCurlRequest> request;
        std::shared_ptr<TCurlResponse> response;

        TCurlDict headers;        
        char progressChar;
        int progressWidth;
        const char *progressSuffix;
        const char *progressPrefix;

    private:
        static char        _curl_download_chr;
        static int         _curl_download_width;
        static const char *_curl_download_prefix;
        static const char *_curl_download_suffix;
        
        static curl_slist *_curl_serialize_headers(const TCurlDict &headers, curl_slist *list = NULL);
        static size_t _curl_header_filesize(char *buffer, size_t size, size_t nitems, void *userdata);
        static int    _curl_xferinfo_function(void* clientp, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow);
        static size_t _curl_parse_headers(char* buffer, size_t size, size_t nitems, TCurlDict* headers);
        static time_t _curl_parse_time(const TString& strtime);

        typedef struct {

            const char *uri;
            curl_off_t resume_byte_pos;

            char chr;
            int width;
            const char *prefix;
            const char *suffix;

        } curl_data_t;

        
    public:
        Impl();
        ~Impl();

        CURL* GetHandler();

        const char *GetUrl() const;

        const char *GetError() const;
        CURLcode GetErrorCode() const;

        std::shared_ptr<TCurlResponse> Request(std::shared_ptr<TCurlRequest> request, FILE *f = NULL);
        std::shared_ptr<TCurlResponse> RequestHeader(std::shared_ptr<TCurlRequest> request);
        long GetSize(const char *url);

        TCurlDict GetHeaders() const;
        TString GetHeader(TString name) const;
        void ClearHeaders();
        void AddHeader(TString header, TString value);
        void RemoveHeader(TString header);

        void SetProgressPrefix(const char *);
        void SetProgressSuffix(const char *);
        void SetProgressWidth(int);
        void SetProgressChar(char);
};