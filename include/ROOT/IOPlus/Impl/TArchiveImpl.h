#ifndef TArchiveImpl_H
#define TArchiveImpl_H

#include <TString.h>
#include <TSystem.h>
#include <TMD5.h>

#include <archive.h>
#include <archive_entry.h>

#include "TFileReader.h"
#include "TArchive.h"

class TArchive::Impl {

    friend class TArchive;
    protected:
    
        TString aName;
        int fCurrentPos = 0;
        int fCurrentStatus = ARCHIVE_OK;

        struct archive* fHandler = nullptr;
        struct archive_entry *fCurrentEntry = nullptr;

    public:
        Impl(const char *aName);
        ~Impl();
        
        archive *GetHandler();
        archive_entry *GetEntry();
        
        const char *GetCompression();

        TString GetEntryName(bool prependArchiveName = false);
        std::vector<TString> GetEntryNames(bool prependArchiveName = false);

        Int_t Open(Int_t index = 0);
        Int_t Reset();
        void Close();

        void Seekg(Int_t pos);
        bool Next();
        Int_t Tellg();
        Int_t GetStatus();
        int GetN();

        Int_t Extract(const char *);
        bool ExtractMember(Int_t index, TString destination);
        static Int_t Write(
            const char *aName, 
            const CompressionFormat &format, 
            TArchiveBuffer members
        );
};

#endif