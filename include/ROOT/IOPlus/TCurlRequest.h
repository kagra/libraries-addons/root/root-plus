#pragma once 

#include <Riostream.h>
#include <TString.h>
#include <TSystem.h>
#include <map>

class TCurlFile;

using TCurlDict = std::map<TString, TString>;
enum TCurlMethod {GET, POST, PUT, DELETE};

class TCurlRequest {

    friend class TCurlFile;

    protected:
    
        const char *uri;
        Option_t *options;

        TCurlDict headers;
        TCurlMethod method;
        const char *parameters;

        const char *userAgent = "ReqBin Curl Client/1.0";
        bool ignorePeerValidation = false;
        bool followLocation = false;
        bool noProgress = false;
        bool verbose = false;
        bool noBody = false;
        bool noResume = false;

        int64_t resumeBytePos = 0;
            
    public:

        TCurlRequest(const char *uri, Option_t *options = "", const TCurlDict &headers = {}, const TCurlMethod &method = TCurlMethod::GET, const char *parameters = "");
        virtual ~TCurlRequest();

        // Getters
        const char *GetUri() const;
        Option_t * GetOptions() const;
        TCurlMethod GetMethod() const;
        const char *GetParameters() const;

        TCurlDict GetHeaders() const;
        void ClearHeaders();
        void AddHeader(TString header, TString value);
        void RemoveHeader(TString header);
        
        // CURL options
        void ResumeFromLarge(int64_t resumeBytePos);
        void IgnorePeerValidation(bool ignore = true);
        void NoBody(bool nobody = true);
        void NoResume(bool noresume = true);
        void SetUserAgent(const char *agent);
        void FollowLocation(bool follow = true);
        void HideProgress(bool noProgress = true);
        void Verbose(bool verbose = true);

        friend std::ostream& operator<<(std::ostream& os, const TCurlRequest& request)
        {
            os << "HTTP Request URI: " << request.uri << "\n";
            os << "HTTP Method: ";
            switch (request.method) {
                case GET: os << "GET"; break;
                case POST: os << "POST"; break;
                case PUT: os << "PUT"; break;
                case DELETE: os << "DELETE"; break;
            }
            os << "\n";
            os << "HTTP Parameters: " << (request.parameters ? request.parameters : "") << "\n";
            os << "User Agent: " << request.userAgent << "\n";
            os << "Ignore Peer Validation: " << (request.ignorePeerValidation ? "true" : "false") << "\n";
            os << "Follow Location: " << (request.followLocation ? "true" : "false") << "\n";
            os << "No Progress: " << (request.noProgress ? "true" : "false") << "\n";
            os << "Verbose: " << (request.verbose ? "true" : "false") << "\n";
            os << "No Body: " << (request.noBody ? "true" : "false") << "\n";
            os << "No Resume: " << (request.noResume ? "true" : "false") << "\n";
            os << "Resume Byte Position: " << request.resumeBytePos << "\n";
            os << "HTTP Headers:\n";
            for (const auto& header : request.headers) {
                os << "  " << header.first << ": " << header.second << "\n";
            }
            return os;
        }
};
