#pragma once 

#include <iostream>
#include <vector>
#include <map>

#include <complex>
#include <iomanip>
#include <string.h>

#include <TString.h>
#include <TVectorT.h>
#include <TMatrixT.h>

#include <libRIOPlus.Config.h>
#ifdef STD_IOMANIP_EXTEND

namespace std {

    // Declare global variables as extern
    extern int _limit;
    extern char _delimiter;

    extern int _offset;
    extern int _line_size;

    namespace {

        // Private method
        inline static string spacer(int length, char c = ' ')
        {
                std::string space = "";
                for(int i = 0; i < length; i++) space += c;
                return space;
        }
        
        // Private structure for SetLimitManip
        struct SetLimitManip {
            int __limit;

            explicit SetLimitManip(int limit) : __limit(limit) {}

            friend std::ostream& operator<<(std::ostream& os, const SetLimitManip& manip) {
                _limit = manip.__limit; // Set the global _limit
                return os;
            }
        };

        // Private structure for SetDelimiterManip
        struct SetDelimiterManip {
            char __delimiter;

            explicit SetDelimiterManip(char delimiter) : __delimiter(delimiter) {}

            friend std::ostream& operator<<(std::ostream& os, const SetDelimiterManip& manip) {
                _delimiter = manip.__delimiter; // Set the global _delimiter
                return os;
            }
        };

        // Private structure for SetOffsetManip
        // @TODO: look backward and implement offset by overloading std::endl()
        //        to offset by n spaces..
        struct SetOffsetManip {
            int __offset;

            explicit SetOffsetManip(int offset) : __offset(offset) {}

            friend std::ostream& operator<<(std::ostream& os, const SetOffsetManip& manip) {
                _offset = manip.__offset; // Set the global _offset
                return os;
            }
        };

    } // namespace

    // Function to create and return the custom manipulator SetLimitManip
    SetLimitManip setlimit(const int &limit) { return SetLimitManip(limit); }
    int limit();

    // Function to create and return the custom manipulator SetDelimiterManip
    SetDelimiterManip setdelimiter(const char &delimiter) { return SetDelimiterManip(delimiter); }
    char delimiter();

    // Function to create and return the custom manipulator SetOffsetManip
    SetOffsetManip setoffset(const int &offset) { return SetOffsetManip(offset); }
    int offset();

    // Overload operator<< to use printVector function with a default limit of 0 (no limit)
    template <typename T>
    std::ostream& operator<<(std::ostream& os, const std::complex<T>& elem) {

        std::streamsize currentPrecision = os.precision();
        os << std::setprecision(currentPrecision) << elem.real() << (elem.imag() >= 0 ? "+" : "") << elem.imag() << "i";
        return os;
    }
     
    template <typename T>
    std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec) {

        size_t vecSize = vec.size();
        size_t limit = std::limit();
        if(vecSize - limit < 3) limit = vecSize;

        int w = os.width();
        os.width(0);
        
        os << "[";

        if (vecSize <= limit) {

            for (size_t i = 0; i < vecSize; ++i) {
                if (i > 0) {
                    os << std::delimiter() << " "; 
                }
                os.width(w);
                os << vec[i];
            }

        } else {

            size_t firstHalf = (limit + 1) / 2;
            size_t secondHalf = limit - firstHalf;

            for (size_t i = 0; i < firstHalf; ++i) {
                if (i > 0) {
                    os << std::delimiter() << " "; 
                }
                os.width(w);
                os << vec[i];
            }

            os << std::delimiter() << " ";
            os.width(w);
            os << "...";

            for (size_t i = vecSize - secondHalf; i < vecSize; ++i) {
                os << std::delimiter() << " "; 
                os.width(w);
                os << vec[i];
            }
        }

        os << "]";
        return os;
    }

    // Overload operator<< to use printVectorOfVectors function with a default limit of 0 (no limit)
    template <typename T>
    std::ostream& operator<<(std::ostream& os, const std::vector<std::vector<T>>& vec) {

        int w = os.width();
        os.width(0);  // Reset width

        int offset = std::offset();
        os << std::spacer(offset) << "[";
        for (size_t i = 0; i < vec.size(); ++i) {

            if(i > 0) {
                os << std::spacer(offset) << " ";
            }

            os.width(w);
            os << vec[i];
            if (i != vec.size() - 1) {
                os << std::delimiter();
            }
            if(i < vec.size()-1) os << std::endl;
        }

        os << "]";

        return os;
    }
    
    // Overload operator<< for TVectorT
    template <typename T>
    std::ostream& operator<<(std::ostream& os, const TVectorT<T>& vec) {

        std::vector<T> v(vec.GetMatrixArray(), vec.GetMatrixArray() + vec.GetNoElements());
        os << v;

        return os;
    }

    // Overload operator<< for TMatrixT
    template <typename T>
    std::ostream& operator<<(std::ostream& os, const TMatrixT<T>& M) {
        
        std::vector<std::vector<T>> m(M.GetNrows());
        for (int i = 0; i < M.GetNrows(); ++i) {

            int J = M.GetNcols();
            if(i < M.GetNrows())  {

                const T *array = M.GetMatrixArray();
                m[i] = std::vector<T>(&array[i*J], &array[i*J] + J);
            }
        }
        
        os << m;
        return os;
    }

        
    // Overload operator<< to print dictionaries
    template <typename T>
    std::ostream& operator<<(std::ostream& os, const std::map<T, T>& elem) {

        std::cout << "{";
        if(elem.size()) std::cout << std::endl;
        for (const auto& it : elem) {
            os << "  \"" << it.first << "\": \"" << it.second << "\"" << std::endl;
        }
        std::cout << "}" << std::endl;
        return os;
    }
       

} // namespace std

#endif
