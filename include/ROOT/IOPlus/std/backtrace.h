#pragma once

#include <iostream>
#include <regex>

#include <execinfo.h>      // For backtrace and backtrace_symbols
#include <cxxabi.h>        // For abi::__cxa_demangle
#include <dlfcn.h>         // For Dl_info and dladdr
#include <cstdlib>         // For std::free

namespace std {

        inline void print_backtrace() {

                const int max_frames = 100;
                void* buffer[max_frames];
                int frame_count = backtrace(buffer, max_frames);

                char** symbols = backtrace_symbols(buffer, frame_count);
                if (symbols == nullptr) {
                        std::cerr << "Error: Could not obtain backtrace symbols." << std::endl;
                        return;
                }

                for (int i = frame_count-1; i > 0; --i) {

                        Dl_info info;
                        if (!dladdr(buffer[i], &info) && info.dli_sname) std::cout << symbols[i] << std::endl;
                        else {
                                int status = 0;
                                char* demangled = abi::__cxa_demangle(info.dli_sname, nullptr, nullptr, &status);
                                const char* function_name = (status == 0) ? demangled : info.dli_sname;
                                std::cout << "Stack #" << frame_count-i << " in " << info.dli_fname << std::endl;
                                std::cout << "\t" << function_name << " + " << (char*)buffer[i] - (char*)info.dli_saddr << std::endl;
                                free(demangled);
                        }
                }

                free(symbols);
        }
}