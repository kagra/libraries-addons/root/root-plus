#pragma once

#include "../../Rioplus.h"

#include <TMatrixT.h>
#include <TVectorT.h>
#include <TPrint.h>

#include <stdexcept>
#include <utility>
#include <sstream>
#include <TString.h>
#include <initializer_list>
#include <numeric>

namespace ROOT {
namespace IOPlus {

enum class ExecutionPolicy { Sequential, Parallel, ParallelUnsequenced, Unsequenced};

enum class TTensorType { kChar, kShort, kLong, kInt, kFloat, kDouble, kComplexFloat, kComplexDouble };
enum class TTensorDevice { Auto, CPU, GPU };
enum class TTensorLayout { Stride, Sparse };
enum class TTensorNorm { Backward = 0, Ortho = 1, Forward = 2 };

template<typename Element>
class TTensorT: public TObject {

protected:
    class Impl;
    Impl *pImpl;

    void Attach(Impl *pImpl);
    
    template<typename List>
    std::vector<int64_t> GetShape(const List& list) const
    {
        std::vector<int64_t> shape(1, list.size());

        if constexpr (!std::is_same_v<typename List::value_type, Element>) { // Recursively get inner dimensions
            auto innerShape = GetShape(*list.begin());
            shape.insert(shape.end(), innerShape.begin(), innerShape.end());
        }

        return shape;
    }

    template<typename List>
    void FlattenDataRecursive(const List& list, std::vector<Element>& data) const
    {
        for (const auto& elem : list) {

            if constexpr (std::is_same_v<typename List::value_type, Element>) data.push_back(elem);
            else FlattenDataRecursive(elem, data);
        }
    }

    // Unified method to flatten data from any level of nested initializer_list
    template<typename List>
    std::vector<Element> FlattenData(const List& list) const
    {
        std::vector<Element> data;
        FlattenDataRecursive(list, data);
        return data;
    }

public:

    using T = std::complex_type_t<Element>;
    friend class TTensorT<T>;

    const Impl& GetImpl()    const { return *pImpl; }
          Impl& GetImpl()          { return *pImpl; }
          Impl* GetImplPtr() const { return  pImpl; }

    static const char *Version();

    // Function to return tensor without copying data
    static TTensorT<Element>::Impl FromBlob(const std::vector<Element>& data, std::vector<int64_t> sizes = {})
    {
        return TTensorT<Element>::Impl::from_blob(data, sizes);
    }

    TTensorT();
    TTensorT(const TTensorT& copy);
    TTensorT(const TTensorT::Impl& copy);

    TTensorT(const std::vector<int64_t> &shape, const std::vector<Element> &data = {}, const Element& c = 0);
    TTensorT(const std::vector<int64_t> &shape, void *data, const int64_t &length,  const Element& c = 0);
    TTensorT(const std::vector<Element> &data, const std::vector<int64_t> &shape = {}, const Element& c = 0);

    template<typename... Shape, typename = std::enable_if_t<(std::is_arithmetic_v<Shape> && ...)>>
    TTensorT(Shape... shape) : TTensorT() { this->Resize(std::vector<int64_t>{static_cast<int64_t>(shape)...}); }
    
    // Constructor taking a nested initializer list
    TTensorT(const std::initializer_list<Element>& list);
    TTensorT(const std::initializer_list<std::initializer_list<Element>>& list);
    TTensorT(const std::initializer_list<std::initializer_list<std::initializer_list<Element>>>& list);

    virtual ~TTensorT();

    static TTensorDevice GetDevice();
    static int GetDeviceCount();
    
    static void ManualSeed(const uint64_t &seed);
    TTensorT<Element>& Autograd(bool requires_grad = false);
    TTensorT<Element>& Backward(const TTensorT<Element>& = {});
    TTensorT<Element>& To(TTensorDevice device = TTensorDevice::Auto);
    TTensorT<Element>& To(const TString &device);

    //
    // Factory methods: https://pytorch.org/cppdocs/notes/tensor_creation.html#picking-a-factory-function
    inline static TTensorT<Element> Arange(                      const int64_t &end, const int64_t &step = 1) { return Arange(0, end, step); }
           static TTensorT<Element> Arange(const int64_t &start, const int64_t &end, const int64_t &step = 1);

    template<typename... Shape, typename = std::enable_if_t<(std::is_arithmetic_v<Shape> && ...)>>
    inline static TTensorT<Element> Empty(Shape... shape) { return Empty(std::vector<int64_t>{static_cast<int64_t>(shape)...}); }
    template<typename... Args>
    inline static TTensorT<Element> Empty(const std::initializer_list<Args>&... args) { Empty(std::vector<int64_t>{static_cast<int64_t>(*args.begin())...}); }
           static TTensorT<Element> Empty(std::vector<int64_t>);

    static TTensorT<Element> Eye(const int64_t &n, const int64_t &m = 0);

    inline static TTensorT<Element> Linspace(                      const int64_t &end, const int64_t &step = 1) { return Linspace(0, end, step); }
           static TTensorT<Element> Linspace(const int64_t &start, const int64_t &end, const int64_t &step = 1);

    inline static TTensorT<Element> Logspace(                      const int64_t &end, const int64_t &step = 1, double base = 10.0) { return Logspace(0, end, step, base); }
           static TTensorT<Element> Logspace(const int64_t &start, const int64_t &end, const int64_t &step = 1, double base = 10.0);

    template<typename... Shape, typename = std::enable_if_t<(std::is_arithmetic_v<Shape> && ...)>>
    inline static TTensorT<Element> Full(const Element &c, Shape... shape) { return Full(c, std::vector<int64_t>{static_cast<int64_t>(shape)...}); }
    template<typename... Args>
    inline static TTensorT<Element> Full(const Element &c, const std::initializer_list<Args>&... args) { Full(c, std::vector<int64_t>{static_cast<int64_t>(*args.begin())...}); }
           static TTensorT<Element> Full(const Element &c, std::vector<int64_t> shape);

    template<typename... Shape, typename = std::enable_if_t<(std::is_arithmetic_v<Shape> && ...)>>
    inline static TTensorT<Element> Ones(Shape... shape) { return Ones(std::vector<int64_t>{static_cast<int64_t>(shape)...}); }
    template<typename... Args>
    inline static TTensorT<Element> Ones(const std::initializer_list<Args>&... args) { Ones(std::vector<int64_t>{static_cast<int64_t>(*args.begin())...}); }
    inline static TTensorT<Element> Ones(std::vector<int64_t> shape) { return Full(0, shape); }

    template<typename... Shape, typename = std::enable_if_t<(std::is_arithmetic_v<Shape> && ...)>>
    inline static TTensorT<Element> Rand(Shape... shape) { return Rand(std::vector<int64_t>{static_cast<int64_t>(shape)...}); }
    template<typename... Args>
    inline static TTensorT<Element> Rand(const std::initializer_list<Args>&... args) { Rand(std::vector<int64_t>{static_cast<int64_t>(*args.begin())...}); }
           static TTensorT<Element> Rand(std::vector<int64_t> shape);

    template<typename... Shape, typename = std::enable_if_t<(std::is_arithmetic_v<Shape> && ...)>>
    inline static TTensorT<Element> Randint(const int64_t &high, Shape... shape) { return Randint(0, high, std::vector<int64_t>{static_cast<int64_t>(shape)...}); }
    template<typename... Args>
    inline static TTensorT<Element> Randint(const int64_t &high, const std::initializer_list<Args>&... args) { Randint(0, high, std::vector<int64_t>{static_cast<int64_t>(*args.begin())...}); }
    inline static TTensorT<Element> Randint(const int64_t &high, std::vector<int64_t> shape) { return Randint(0, high, shape); }

    template<typename... Shape, typename = std::enable_if_t<(std::is_arithmetic_v<Shape> && ...)>>
    inline static TTensorT<Element> Randint(const int64_t &low, const int64_t &high, Shape... shape) { return Randint(low, high, std::vector<int64_t>{static_cast<int64_t>(shape)...}); }
    template<typename... Args>
    inline static TTensorT<Element> Randint(const int64_t &low, const int64_t &high, const std::initializer_list<Args>&... args) { Randint(low, high, std::vector<int64_t>{static_cast<int64_t>(*args.begin())...}); }
           static TTensorT<Element> Randint(const int64_t &low, const int64_t &high, std::vector<int64_t> shape);

    template<typename... Shape, typename = std::enable_if_t<(std::is_arithmetic_v<Shape> && ...)>>
    inline static TTensorT<Element> Randn(Shape... shape) { return Randn(std::vector<int64_t>{static_cast<int64_t>(shape)...}); }
    template<typename... Args>
    inline static TTensorT<Element> Randn(const std::initializer_list<Args>&... args) { Randn(std::vector<int64_t>{static_cast<int64_t>(*args.begin())...}); }
           static TTensorT<Element> Randn(std::vector<int64_t> shape);

    static TTensorT<Element> Randperm(const int64_t &n);

    template<typename... Shape, typename = std::enable_if_t<(std::is_arithmetic_v<Shape> && ...)>>
    inline static TTensorT<Element> Zeros(Shape... shape) { return Zeros(std::vector<int64_t>{static_cast<int64_t>(shape)...}); }
    template<typename... Args>
    inline static TTensorT<Element> Zeros(const std::initializer_list<Args>&... args) { Zeros(std::vector<int64_t>{static_cast<int64_t>(*args.begin())...}); }
    inline static TTensorT<Element> Zeros(std::vector<int64_t> shape) { return Full(0, shape); }

    //
    // Methods on shape and tensor structure 
    std::vector<int64_t> GetShape() const;
    TString GetShapeStr() const;

    template<typename... Shape, typename = std::enable_if_t<(std::is_arithmetic_v<Shape> && ...)>>
    TTensorT<Element>& Reshape(Shape... shape) {
        
        std::vector<int64_t> newShape{static_cast<int64_t>(shape)...};
        int64_t newSize = std::accumulate(newShape.begin(), newShape.end(), 1, std::multiplies<int64_t>());
        int64_t oldSize = GetEntries();

        if (newSize != GetEntries()) {
            throw std::invalid_argument("Reshape " + GetShapeStr(newShape) + " operation cannot change the total number of elements in the tensor " + GetShapeStr() + ".");
        }

        return Resize(newShape);
    }

    template<typename... Sizes, typename = std::enable_if_t<(std::is_arithmetic_v<Sizes> && ...)>>
    TTensorT<Element>& Resize(Sizes... sizes) {

        std::vector<int64_t> newSizes{static_cast<int64_t>(sizes)...};
        return Resize(newSizes);
    }

    TTensorT<Element>& Resize(std::vector<int64_t> sizes, const Element &c = 0);

    int64_t GetDimensions() const;
    int64_t GetSize() const { return GetEntries() * sizeof(Element); }
    int64_t GetEntries(int64_t dim) const;
    int64_t GetEntries() const;

    bool IsComplex();
    void Clear();
    void Empty();

    template<typename Callback>
    TTensorT<Element> &Map(Callback&& callback, const ExecutionPolicy &policy = ExecutionPolicy::ParallelUnsequenced)
    {
        auto fn = [&] (const Element& element) -> Element {

            int64_t index = &element - this->begin();            
            if constexpr (std::is_invocable_r_v<Element, Callback, const int64_t&, const Element&>) {
                return callback(index, element);
            } else if constexpr (std::is_invocable_r_v<Element, Callback, const Element&>) {
                return callback(element);
            } else if constexpr (std::is_invocable_r_v<Element, Callback, const int64_t&>) {
                return callback(index);
            } else if constexpr (std::is_invocable_r_v<Element, Callback>) {
                return callback();
            }

            throw std::invalid_argument("Unexpected callback method provided.");
        };

        return Transform(fn, policy);
    }

    template <typename Callback>
    TTensorT<Element> &Filter(Callback&& callback, const ExecutionPolicy &policy = ExecutionPolicy::ParallelUnsequenced)
    {
        auto fn = [&](const Element& element) -> Element {

            int64_t index = &element - this->begin();
            if constexpr (std::is_invocable_r_v<bool, Callback, const int64_t&, const Element&>) {
                if(!callback(index, element)) return 0;
            } else if constexpr (std::is_invocable_r_v<bool, Callback, const Element&>) {
                if(!callback(element)) return 0;
            } else if constexpr (std::is_invocable_r_v<bool, Callback, const int64_t&>) {
                if(!callback(index)) return 0;
            } else if constexpr (std::is_invocable_r_v<bool, Callback>) {
                if(!callback()) return 0;
            }

            return element;
        };

        return Transform(fn, policy);
    }

    TTensorT<Element> &Transform(const std::function<Element(const Element &)> &fn, const ExecutionPolicy &policy = ExecutionPolicy::ParallelUnsequenced);

protected:
    Element *GetDataPtr(int64_t dataIndex = 0) const;
    Element *GetDataPtr(std::vector<int64_t> indices) const { return this->GetDataPtr(this->GetDataIndex(indices)); };
    inline int64_t GetDataPtrOffset(const Element *ptr) const { return this->GetDataPtrOffset(this->GetDataIndices(ptr)); }
    inline int64_t GetDataPtrOffset(std::vector<int64_t> indices) const { return this->GetDataPtrOffset(this->GetDataIndex(indices)); };
           int64_t GetDataPtrOffset(int64_t dataIndex) const;

public:

    const Element &GetData(int64_t dataIndex) const;
          Element &GetData(int64_t dataIndex);
    const Element &GetData(std::vector<int64_t> indices) const { return this->GetData(this->GetDataIndex(indices)); }
          Element &GetData(std::vector<int64_t> indices) { return this->GetData(this->GetDataIndex(indices)); }

    template<typename... Indices, typename = std::enable_if_t<(std::is_arithmetic_v<Indices> && ...)>>
    int64_t GetDataIndex(Indices... indices) const { return this->GetDataIndex(std::vector<int64_t>{static_cast<int64_t>(indices)...}); }
    int64_t GetDataIndex(const std::vector<int64_t>& indices) const;
    int64_t GetDataIndex(const Element* ptr) const;

    inline std::vector<int64_t> GetDataIndices(const Element *ptr) const { return GetDataIndices(this->GetDataIndex(ptr)); }
           std::vector<int64_t> GetDataIndices(int64_t dataIndex) const;

    //
    // Tensor extraction methods
    TTensorT<Element> Clone() const;

    //
    // Tensor operations
    inline TTensorT<Element>  Fill (const Element& c) const { TTensorT<Element> t = *this; return t.Fill_(c); };
           TTensorT<Element>& Fill_(const Element& c);

    inline TTensorT<Element>  FillRandom (const Element &max) const { TTensorT<Element> t = *this; return t.FillRandom_(max); };
           TTensorT<Element>& FillRandom_(const Element &max);

    inline TTensorT<Element>  FillRandom (const Element &min, const Element &max) const { TTensorT<Element> t = *this; return t.FillRandom_(min, max); };
           TTensorT<Element>& FillRandom_(const Element &min, const Element &max);

    inline TTensorT<Element>  FillRandomInt (const int &max) const { TTensorT<Element> t = *this; return t.FillRandomInt_(max); };
           TTensorT<Element>& FillRandomInt_(const int &max);

    inline TTensorT<Element>  FillRandomInt (const int &min, const int &max) const { TTensorT<Element> t = *this; return t.FillRandomInt_(min, max); };
           TTensorT<Element>& FillRandomInt_(const int &min, const int &max);

    inline TTensorT<Element>  Transpose (int64_t dim0, int64_t dim1) const { TTensorT<Element> t = *this; return t.Transpose_(dim0, dim1); };
           TTensorT<Element>& Transpose_(int64_t dim0, int64_t dim1);

    inline TTensorT<Element>  Concatenate (const TTensorT<Element>& B, int64_t dim) const { TTensorT<Element> t = *this; return t.Concatenate_(B, dim); };
           TTensorT<Element>& Concatenate_(const TTensorT<Element>& B, int64_t dim);

    inline TTensorT<Element>  Contraction (const TTensorT<Element>& B) const { TTensorT<Element> t = *this; return t.Contraction_(B); };
           TTensorT<Element>& Contraction_(const TTensorT<Element>& B);

    inline TTensorT<Element>  Permute (const std::vector<int64_t>& perms) const { TTensorT<Element> t = *this; return t.Permute_(perms); };
           TTensorT<Element>& Permute_(const std::vector<int64_t>& perms);

    inline TTensorT<Element>  Cumsum (int64_t dim) const { TTensorT<Element> t = *this; return t.Cumsum_(dim); };
           TTensorT<Element>& Cumsum_(int64_t dim);

    inline TTensorT<Element>  Sum (int64_t dim, std::vector<T> weight = {}) const { TTensorT<Element> t = *this; return t.Sum_(dim, weight); };
           TTensorT<Element>& Sum_(int64_t dim, std::vector<T> weight = {});

    inline TTensorT<Element>  Mean (int64_t dim, std::vector<T> weight = {}) const { TTensorT<Element> t = *this; return t.Mean_(dim, weight); };
           TTensorT<Element>& Mean_(int64_t dim, std::vector<T> weight = {});

    inline TTensorT<Element>  Min () const { TTensorT<Element> t = *this; return t.Min_(); };
           TTensorT<Element>& Min_();

    inline TTensorT<Element>  Max () const { TTensorT<Element> t = *this; return t.Max_(); };
           TTensorT<Element>& Max_();

    template <typename... Tuples>
    std::enable_if_t<(std::is_tuple2_int64<std::tuple<Tuples...>>::value || std::is_tuple3_int64<std::tuple<Tuples...>>::value), TTensorT<Element>>
    Slice(const Tuples&... tuples) const {
        if constexpr (std::is_tuple2_int64<std::tuple<Tuples...>>::value) {
            return Slice(std::make_tuple4<int64_t>(std::vector<std::tuple<int64_t, int64_t>>{tuples...}));
        } else if constexpr (std::is_tuple3_int64<std::tuple<Tuples...>>::value) {
            return Slice(std::make_tuple4<int64_t>(std::vector<std::tuple<int64_t, int64_t, int64_t>>{tuples...}));
        } else {
            throw std::invalid_argument("Slice: Unsupported tuple type");
        }
    }
    inline TTensorT<Element> Slice(int64_t dim, int64_t start, int64_t stop, int64_t step = 1) const { return Slice({std::make_tuple(dim, start, stop, step)}); }
    inline TTensorT<Element> Slice(int64_t dim, int64_t stop) const { return Slice(dim, 0, stop, 1); }
    inline TTensorT<Element> Slice(const std::vector<std::tuple2<int64_t>>& shape) const { return Slice(std::make_tuple4<int64_t>(shape)); }
    inline TTensorT<Element> Slice(const std::vector<std::tuple3<int64_t>>& shape) const { return Slice(std::make_tuple4<int64_t>(shape)); }
           TTensorT<Element> Slice(const std::vector<std::tuple4<int64_t>>& shape) const { TTensorT<Element> t = *this; return t.Slice_(shape); };

    template <typename... Tuples>
    std::enable_if_t<(std::is_tuple2_int64<std::tuple<Tuples...>>::value || std::is_tuple3_int64<std::tuple<Tuples...>>::value), TTensorT<Element>>
    Slice_(const Tuples&... tuples) const {
        if constexpr (std::is_tuple2_int64<std::tuple<Tuples...>>::value) {
            return Slice_(std::make_tuple4<int64_t>(std::vector<std::tuple<int64_t, int64_t>>{tuples...}));
        } else if constexpr (std::is_tuple3_int64<std::tuple<Tuples...>>::value) {
            return Slice_(std::make_tuple4<int64_t>(std::vector<std::tuple<int64_t, int64_t, int64_t>>{tuples...}));
        } else {
            throw std::invalid_argument("Slice: Unsupported tuple type");
        }
    }

    inline TTensorT<Element>& Slice_(int64_t dim, int64_t start, int64_t stop, int64_t step = 1) { return Slice_({std::make_tuple(dim, start, stop, step)}); }
    inline TTensorT<Element>& Slice_(int64_t dim, int64_t stop) { return Slice_(dim, 0, stop, 1); }
    inline TTensorT<Element>& Slice_(const std::vector<std::tuple2<int64_t>>& shape) { return Slice_(std::make_tuple4<int64_t>(shape)); }
    inline TTensorT<Element>& Slice_(const std::vector<std::tuple3<int64_t>>& shape) { return Slice_(std::make_tuple4<int64_t>(shape)); }
           TTensorT<Element>& Slice_(const std::vector<std::tuple4<int64_t>>& shape);

    inline TTensorT<Element>  Squeeze (const int64_t &dim = 0) const { TTensorT<Element> t = *this; return t.Squeeze_(dim); };
           TTensorT<Element>& Squeeze_(const int64_t &dim = 0);

    inline TTensorT<Element>  Unsqueeze (const int64_t &dim = 0) const { TTensorT<Element> t = *this; return t.Unsqueeze_(dim); };
           TTensorT<Element>& Unsqueeze_(const int64_t &dim = 0);

    inline TTensorT<Element>  Solve (const TTensorT<Element> &B, bool Ax = true) const { TTensorT<Element> t = *this; return t.Solve_(B, Ax); }; // Ax = B  
           TTensorT<Element>& Solve_(const TTensorT<Element> &B, bool Ax = true);

    inline TTensorT<Element>  Zeros () const { TTensorT<Element> t = *this; return t.Zeros_(); };
           TTensorT<Element>& Zeros_() { return Fill_(0); }

    inline TTensorT<Element>  Ones () const { TTensorT<Element> t = *this; return t.Ones_(); };
           TTensorT<Element>& Ones_() { return Fill_(1); }

    inline TTensorT<Element>  Tile (const std::vector<int64_t> &dims) const { TTensorT<Element> t = *this; return t.Tile_(dims); };
           TTensorT<Element>& Tile_(const std::vector<int64_t> &dims);

    inline TTensorT<Element>  Inverse () const { TTensorT<Element> t = *this; return t.Inverse_(); };
           TTensorT<Element>& Inverse_();

    inline TTensorT<Element>  Pow (const T& e) const { TTensorT<Element> t = *this; return t.Pow_(e); };
           TTensorT<Element>& Pow_(const T& e);
    inline TTensorT<Element>  Pow (const TTensorT<T>& e) const { TTensorT<Element> t = *this; return t.Pow_(e); };
           TTensorT<Element>& Pow_(const TTensorT<T>& e);

    inline TTensorT<Element>  Exp () const { TTensorT<Element> t = *this; return t.Exp_(); };
           TTensorT<Element>& Exp_();

    inline TTensorT<Element>  Abs () const { TTensorT<Element> t = *this; return t.Abs_(); };
           TTensorT<Element>& Abs_();

    inline TTensorT<Element>   FFT (int n, int64_t dim, const TTensorNorm &norm) const { TTensorT<Element> t = *this; return t.FFT_(n, dim, norm); };
           TTensorT<Element>&  FFT_(int n, int64_t dim, const TTensorNorm &norm);

    inline TTensorT<Element>  iFFT (int n, int64_t dim, const TTensorNorm &norm) const { TTensorT<Element> t = *this; return t.iFFT_(n, dim, norm); };
           TTensorT<Element>& iFFT_(int n, int64_t dim, const TTensorNorm &norm);

    template<typename... Args>
    inline TTensorT<Element>  Stack (int64_t dim, std::initializer_list<const Args &>... args) const { TTensorT<Element> t = *this; return t.Stack_(dim, args...); };
    template<typename... Args>
    inline TTensorT<Element>& Stack_(int64_t dim, std::initializer_list<const Args &>... args) { return this->Stack_(dim, std::vector<TTensorT<Element>>{static_cast<TTensorT<Element>>(*args.begin())...}); }
    inline TTensorT<Element>  Stack (int64_t dim, const std::vector<TTensorT<Element>> &list) const { TTensorT<Element> t = *this; return t.Stack_(dim, list); };
           TTensorT<Element>& Stack_(int64_t dim, const std::vector<TTensorT<Element>> &list);

    inline TTensorT<Element>  Unfold (int64_t dim, int64_t size, int64_t step = 1) const { TTensorT<Element> t = *this; return t.Unfold_(dim, size, step); };
           TTensorT<Element>& Unfold_(int64_t dim, int64_t size, int64_t step = 1);

    TTensorT<Element>& Detach () const { TTensorT<Element> t = *this; return t.Detach_(); };
    TTensorT<Element>& Detach_();

    bool IsContiguous () const;
    inline TTensorT<Element>  Contiguous () const { TTensorT<Element> t = *this; return t.Contiguous(); };
           TTensorT<Element>& Contiguous_();

    Element Trace() const;

    //
    // Accessing index
    const Element& operator[](int64_t dataIndex) const { return this->GetData(dataIndex); }
          Element& operator[](int64_t dataIndex) { return this->GetData(dataIndex); }

    template<typename... Indices, typename = std::enable_if_t<(std::is_arithmetic_v<Indices> && ...)>>
    const Element& operator()(Indices... indices) const { return (*this)(std::vector<int64_t>{static_cast<int64_t>(indices)...}); }
    const Element& operator()(const std::vector<int64_t>& indices) const { return this->GetData(indices); }
    template<typename... Indices, typename = std::enable_if_t<(std::is_arithmetic_v<Indices> && ...)>>
          Element& operator()(Indices... indices) { return (*this)(std::vector<int64_t>{static_cast<int64_t>(indices)...}); }
          Element& operator()(const std::vector<int64_t>& indices) { return this->GetData(indices); }

    // 
    // Assign operations
    TTensorT<Element>& operator=(const TTensorT<Element>& B);
    TTensorT<Element>& operator=(const TTensorT<Element>::Impl& Impl);
    TTensorT<Element>& operator=(const Element& B);

    //
    // Tensor-Tensor operations
    bool operator==(const TTensorT<Element>& t) const;
    bool operator!=(const TTensorT<Element>& t) const { return !(*this == t); }

    TTensorT& operator+=(const TTensorT& B); TTensorT operator+ (const TTensorT& B) const;
    TTensorT& operator-=(const TTensorT& B); TTensorT operator- (const TTensorT& B) const;     
    TTensorT& operator/=(const TTensorT& B); TTensorT operator/ (const TTensorT& B) const;
    TTensorT& operator*=(const TTensorT& B); TTensorT operator* (const TTensorT& B) const;
    TTensorT& operator|=(const TTensorT& B); TTensorT operator| (const TTensorT& B) const;
    TTensorT& operator%=(const TTensorT& B); TTensorT operator% (const TTensorT& B) const;
                                             TTensorT operator~ () const;

    //
    // Tensor-Scalar operations
    TTensorT& operator+=(const Element& e); TTensorT operator+ (const Element& e) const;
    TTensorT& operator-=(const Element& e); TTensorT operator- (const Element& e) const;
    TTensorT& operator/=(const Element& e); TTensorT operator/ (const Element& e) const;
    TTensorT& operator*=(const Element& e); TTensorT operator* (const Element& e) const;

    TTensorT& operator^=(const TTensorT<T>& e); TTensorT operator^ (const TTensorT<T>& e) const;
    TTensorT& operator^=(const T& e); TTensorT operator^ (const T& e) const;

    //
    // Begin and end methods for iterators
    class Iterator {
        public:
            using iterator_category = std::random_access_iterator_tag;
            using value_type = Element;
            using difference_type = std::ptrdiff_t;
            using pointer = Element*;
            using reference = Element&;

        const TTensorT<Element> *container;
        int index;

        Element* ptr;

    public:
        Iterator(const TTensorT<Element> *container, int index)
            : container(container), index(index) { set(index); }

        Element* current() { return ptr; }
        template<typename... Indices, typename = std::enable_if_t<(std::is_arithmetic_v<Indices> && ...)>>
        Iterator& set(Element* _ptr) { return set(container->GetDataPtrOffset(_ptr)); }
        Iterator& set(int index)
        {
            ptr = index >= 0 && index < container->GetEntries() ? container->GetDataPtr(container->GetDataIndices(index)) : nullptr;
            return *this;
        }

        Element& operator[](int index) const {
            if (index < 0 || index >= container->GetEntries()) {
                throw std::out_of_range("Iterator index out of range");
            }
            return *(container->GetDataPtr(container->GetDataIndices(index)));
        }

        Iterator& operator++() {
            set(++index);
            return *this;
        }
        
        Iterator& operator--() {
            set(--index);
            return *this;
        }

        int64_t unreachable() const { return container->GetEntries(); }
        Element &operator*()  const { return *ptr; }
        Element *operator->() const { return ptr; }

        int64_t operator-(const Element *_ptr) const {

            int64_t index0 = container->GetDataIndex(_ptr);
            int64_t index1 = container->GetDataIndex( ptr);
            if(index0 < 0 || index1 < 0) return this->unreachable();

            return index1 - index0;
        }

        Iterator operator+(size_t distance) const { return Iterator(container, index+distance); }
        Iterator operator-(size_t distance) const { return Iterator(container, index-distance); }
        std::ptrdiff_t operator- (const Iterator& other) const { return index - other.index; }

        bool operator==(const Iterator &other) const { return index == other.index && container == other.container; }
        bool operator!=(const Iterator& other) const { return !(*this == other); }

        bool operator< (const Iterator& other) const { return index < other.index; }
        bool operator> (const Iterator& other) const { return index > other.index; }
        bool operator<=(const Iterator& other) const { return index <= other.index; }
        bool operator>=(const Iterator& other) const { return index >= other.index; }
    };

    inline int64_t size() const { return this->GetEntries(); };
    inline Iterator begin() const { return TTensorT<Element>::Iterator(this, 0); }
    inline Iterator end() const { return TTensorT<Element>::Iterator(this, this->GetEntries()); }
    
    template<typename Callback>
    TTensorT<Element>& Apply(Callback callback)
    {
        this->pImpl->apply(callback);
        return *this;
    }

    //
    // Convert to ROOT objects
    template<typename E = Element>
    typename std::enable_if<std::is_same<E, double>::value || std::is_same<E, float>::value, TMatrixT<E>>::type
    GetMatrix(int64_t dim0, int64_t dim1) const { return GetMatrixReal(dim0, dim1); }
    template<typename E = Element>
    typename std::enable_if<std::is_same<E, double>::value || std::is_same<E, float>::value, TMatrixT<E>>::type
    GetMatrixReal(int64_t dim0, int64_t dim1) const;
    template<typename E = Element>
    typename std::enable_if<std::is_same<E, double>::value || std::is_same<E, float>::value, TMatrixT<E>>::type
    GetMatrixImag(int64_t dim0, int64_t dim1) const;

    template<typename E = Element>
    typename std::enable_if<std::is_same<E, double>::value || std::is_same<E, float>::value, TMatrixT<E>>::type
    GetVector(int64_t dim0) const { return GetVectorReal(dim0); }
    template<typename E = Element>
    typename std::enable_if<std::is_same<E, double>::value || std::is_same<E, float>::value, TMatrixT<E>>::type
    GetVectorReal(int64_t dim0) const;
    template<typename E = Element>
    typename std::enable_if<std::is_same<E, double>::value || std::is_same<E, float>::value, TMatrixT<E>>::type
    GetVectorImag(int64_t dim0) const;

protected:

    bool InRange(const std::vector<int64_t>& indices) const;

    inline int64_t GetDataEntries(std::vector<int64_t>) const { return GetEntries(); }
           int64_t GetEntries(std::vector<int64_t>) const;

    TString GetString(const Element& index) const;
    TString GetString(const std::vector<int64_t>& index, TString delimiter = ",", TString leftBracket = "(", TString rightBracket = ")", bool fillZeros = true) const;

    TString GetStridesStr() const;
    TString GetStridesStr(const std::vector<int64_t>& strides) const;
    TString GetShapeStr(const std::vector<int64_t>& shape) const;
    TString GetDataIndicesStr(const std::vector<int64_t>& index) const;

    template<typename... Arg>
    TString GetString(Arg... arg, TString delimiter = ",", TString leftBracket = "(", TString rightBracket = ")") const { return this->GetString(std::vector<int64_t>({arg...}), delimiter, leftBracket, rightBracket); }
    template<typename... Shape, typename = std::enable_if_t<(std::is_arithmetic_v<Shape> && ...)>>
    TString GetShapeStr(Shape... shape) const { return this->GetShapeStr(std::vector<int64_t>{static_cast<int64_t>(shape)...}); }
    template <typename... Indices>
    TString GetDataIndicesStr(Indices... indices) const { return this->GetDataIndicesStr(std::vector<int64_t>{static_cast<int64_t>(indices)...}); }
    template <typename... Strides>
    TString GetStridesStr(Strides... strides) const { return this->GetStridesStr(std::vector<int64_t>{static_cast<int64_t>(strides)...}); }

    std::vector<int64_t> GetStrides() const;
    int64_t GetStride(int64_t dim) const;

    private:
    int PrintHelper(std::ostream& os, std::vector<int64_t> indice = {}) const;
    
    public:
        void Print(Option_t* option = "") const;
        friend std::ostream& operator<<(std::ostream& os, const TTensorT<Element>& tensor) {
            tensor.PrintHelper(os);
            return os;
        }
    
    ClassDef(TTensorT, 1);
};


template<typename Element>
TTensorT<Element>& TTensorT<Element>::Resize(std::vector<int64_t> shape, const Element &c)
{
    std::vector<int64_t> oldShape = this->GetShape();
    this->pImpl->resize_(shape);

    std::vector<int64_t> newShape = this->GetShape();    
    if(oldShape.size() == 0 && newShape.size() > 0) oldShape.push_back(0);

    while(oldShape.size() < newShape.size())
        oldShape.insert(oldShape.begin(), 1);
    while(oldShape.size() > newShape.size())
        newShape.insert(newShape.begin(), 1);

    return this->Map([&] (const int64_t& index, const Element& el) 
    {
        std::vector<int64_t> indices = this->GetDataIndices(index);
        for (int64_t i = 0; i < indices.size(); ++i) {
            if (indices[i] > oldShape[i]-1) return c;
        }

        return el;

    }, ExecutionPolicy::Sequential);
}

template<typename Element>
template<typename E>
typename std::enable_if<std::is_same<E, double>::value || std::is_same<E, float>::value, TMatrixT<E>>::type
TTensorT<Element>::GetMatrixReal(int64_t dim0, int64_t dim1) const {

    if (dim0 < 0 || dim1 < 0 || dim0 >= this->GetDimensions() || dim1 >= this->GetDimensions()) {
        throw std::runtime_error("Invalid dimensions for matrix extraction.");
    }

    // Ensure dim0 and dim1 are not the same
    if (dim0 == dim1) {
        throw std::runtime_error("dim0 and dim1 must be different.");
    }

    // Reduce the tensor along all dimensions except dim0 and dim1
    TTensorT<Element> reduced_tensor = *this;
    for (int64_t dim = this->pImpl->GetDimensions(); dim-- > 0; ) {
        if (dim != dim0 && dim != dim1) {
            reduced_tensor = reduced_tensor.Reduce(dim);
        }
    }

    // Create the matrix for real part
    int64_t nrows = reduced_tensor.sizes[0];
    int64_t ncols = reduced_tensor.sizes[1];
    TMatrixT<Element> matrix(nrows, ncols);

    for (int64_t i = 0; i < nrows; ++i) {
        for (int64_t j = 0; j < ncols; ++j) {
            // Extract real part if element is complex
            if (auto* val = std::get_if<std::complex<Element>>(&reduced_tensor(i, j))) {
                matrix(i, j) = val->real();
            } else {
                matrix(i, j) = std::get<Element>(reduced_tensor(i, j));
            }
        }
    }

    return matrix;
}

template<typename Element>
template<typename E>
typename std::enable_if<std::is_same<E, double>::value || std::is_same<E, float>::value, TMatrixT<E>>::type
TTensorT<Element>::GetMatrixImag(int64_t dim0, int64_t dim1) const {
    if (this->pImpl->GetDimensions() <= 1 || dim0 >= this->pImpl->GetDimensions() || dim1 >= this->pImpl->GetDimensions()) {
        throw std::runtime_error("Invalid dimensions for matrix extraction.");
    }

    // Ensure dim0 and dim1 are not the same
    if (dim0 == dim1) {
        throw std::runtime_error("dim0 and dim1 must be different.");
    }

    // Reduce the tensor along all dimensions except dim0 and dim1
    TTensorT<Element> reduced_tensor = *this;
    for (int64_t dim = this->pImpl->GetDimensions(); dim-- > 0; ) {
        if (dim != dim0 && dim != dim1) {
            reduced_tensor = reduced_tensor.Reduce(dim);
        }
    }

    // Create the matrix for imaginary part
    int64_t nrows = reduced_tensor.sizes[0];
    int64_t ncols = reduced_tensor.sizes[1];
    TMatrixT<Element> matrix(nrows, ncols);

    for (int64_t i = 0; i < nrows; ++i) {
        for (int64_t j = 0; j < ncols; ++j) {
            // Extract imaginary part if element is complex
            if (auto* val = std::get_if<std::complex<Element>>(&reduced_tensor(i, j))) {
                matrix(i, j) = val->imag();
            } else {
                matrix(i, j) = 0.0; // Zero for real elements
            }
        }
    }

    return matrix;
}

template<typename Element>
template<typename E>
typename std::enable_if<std::is_same<E, double>::value || std::is_same<E, float>::value, TMatrixT<E>>::type
TTensorT<Element>::GetVectorReal(int64_t dim0) const {
    if (this->pImpl->GetDimensions() == 0 || dim0 >= this->pImpl->GetDimensions()) {
        throw std::runtime_error("Invalid dimension for vector extraction.");
    }

    // Reduce the tensor along all dimensions except dim0
    TTensorT<Element> reduced_tensor = *this;
    for (int64_t dim = this->pImpl->GetDimensions(); dim-- > 0; ) {
        if (dim != dim0) {
            reduced_tensor = reduced_tensor.Reduce(dim);
        }
    }

    // Create the vector for real part
    int64_t nrows = reduced_tensor.sizes[0];
    TVectorT<Element> vector(nrows);

    for (int64_t i = 0; i < nrows; ++i) {
        // Extract real part if element is complex
        if (auto* val = std::get_if<std::complex<Element>>(&reduced_tensor(i))) {
            vector(i) = val->real();
        } else {
            vector(i) = std::get<Element>(reduced_tensor(i));
        }
    }

    return vector;
}

template<typename Element>
template<typename E>
typename std::enable_if<std::is_same<E, double>::value || std::is_same<E, float>::value, TMatrixT<E>>::type
TTensorT<Element>::GetVectorImag(int64_t dim0) const {

    if (this->pImpl->GetDimensions() == 0 || dim0 >= this->pImpl->GetDimensions()) {
        throw std::runtime_error("Invalid dimension for vector extraction.");
    }

    // Reduce the tensor along all dimensions except dim0
    TTensorT<Element> reduced_tensor = *this;
    for (int64_t dim = this->pImpl->GetDimensions(); dim-- > 0; ) {
        if (dim != dim0) {
            reduced_tensor = reduced_tensor.Reduce(dim);
        }
    }

    // Create the vector for imaginary part
    int64_t nrows = reduced_tensor.sizes[0];
    TVectorT<Element> vector(nrows);

    for (int64_t i = 0; i < nrows; ++i) {
        // Extract imaginary part if element is complex
        if (auto* val = std::get_if<std::complex<Element>>(&reduced_tensor(i))) {
            vector(i) = val->imag();
        } else {
            vector(i) = 0.0; // Zero for real elements
        }
    }

    return vector;
}

template <typename Element>
int64_t operator-(const Element *ptr, const typename ROOT::IOPlus::TTensorT<Element>::Iterator &it) { 
    int64_t index = (it - ptr);
    return index == it.unreachable() ? index : -index;
}

}} // namespace ROOT::IOPlus
