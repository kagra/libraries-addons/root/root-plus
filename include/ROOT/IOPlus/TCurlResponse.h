#pragma once

#include "TCurlRequest.h"

class TCurlFile;

class TCurlResponse {

    friend class TCurlFile;

    protected:

        TString content;
        int status;
        TCurlDict headers;

        long statusCode = 200;

    public:
        TCurlResponse(const char *content = "", int statusCode = 200, TCurlDict headers = {});
        virtual ~TCurlResponse();

        // Getters
        const char *GetContent() const;
        int GetStatusCode() const;
        TCurlDict GetHeaders() const;
        TString GetHeader(TString name) const;

        bool Empty() const;

        friend std::ostream& operator<<(std::ostream& os, const TCurlResponse& response) {
            os << "HTTP Response Status: " << response.statusCode << "\n";
            os << "HTTP Content: " << (response.Empty() ? "[empty]" : response.content.Data()) << "\n";
            os << "HTTP Headers:\n";
            for (const auto& header : response.headers) {
                os << "  " << header.first << ": " << header.second << "\n";
            }
            return os;
        }
};
