#pragma once

#include <Riostream.h>

#include "libRIOPlus.Config.h"
#include "ROOT/IOPlus/Helpers.h"
#include "ROOT/IOPlus/Complex.h"

#ifdef STD_IOMANIP_EXTEND
    #include "ROOT/IOPlus/std/iomanip.h"
#endif

#ifdef STD_BACKTRACE_EXTEND
    #include "ROOT/IOPlus/std/backtrace.h"
#endif

#ifdef STD_TUPLE_EXTEND
    #include "ROOT/IOPlus/std/tuple.h"
#endif
