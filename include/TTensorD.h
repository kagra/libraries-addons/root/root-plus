#pragma once

#include "ROOT/IOPlus/TTensorT.h"

class TTensorD : public ROOT::IOPlus::TTensorT<double> {

    public:
        using ROOT::IOPlus::TTensorT<double>::TTensorT;

    ClassDef(TTensorD, 1);  // ROOT Class Definition
};
