#pragma once

#include <Riostream.h>
#include <signal.h>
#include "TPServerSocket.h"
#include "TMonitor.h"
#include "TMessage.h"

#include <functional>
#include "TSocket.h"

#include "TFileReader.h"
#include "TPrint.h"
#include "TClock.h"

class TObjectStreamer: public TPServerSocket
{
        protected:

                static bool bReady;

                Long_t timeout    = 20; // in ms
                double lastUpdate = 0; // in ms
                Long_t ticks = 0;

                const char *token = "";

                int nClients = 0;
                int maxClients = -1;

                TMonitor *fMonitor;   // socket monitor
                TList    *fSockets;   // list of open spy sockets
                TList    *fObjects;

                int SocketHandler();
                static void ErrorHandler(int level, Bool_t abort, const char *location, const char *msg);
                
                int Initialize(const char *);
                int Open(TPServerSocket*);
                int Close(TSocket*);

        public:

                const int FAILURE = -1;
                const int PASSIVE =  0;
                const int SUCCESS =  1;

                TObjectStreamer (const char *service, const char *token = "", int maxClients = -1, Long_t timeout = 20, Bool_t reuse=kTRUE, Int_t backlog=kDefaultBacklog, Int_t tcpwindowsize=-1): TPServerSocket(service, reuse, backlog, tcpwindowsize), maxClients(maxClients), timeout(timeout) { this->Initialize(token); }
                TObjectStreamer (Int_t port, const char *token = "", int maxClients = -1, Long_t timeout = 20, Bool_t reuse=kTRUE, Int_t backlog=kDefaultBacklog, Int_t tcpwindowsize=-1): TObjectStreamer(gSystem->GetServiceByPort(port), token, maxClients, timeout, reuse, backlog, tcpwindowsize) { }
                
                ~TObjectStreamer() {

                        if(fSockets) {
                                fSockets->Clear("nodelete");
                                delete fSockets;
                        }

                        if(fObjects) {
                                fObjects->Clear("nodelete");
                                delete fObjects;
                        }

                        if(fMonitor) {
                                
                                delete fMonitor;
                        }
                }

                inline void SetTimeout(Long_t timeout) { this->timeout = timeout; }
                inline void SetMaxClients(Long_t maxClients) { this->maxClients = maxClients; }
                inline void SetToken(const char *token) { this->token = token; }

                void Add(TObject *object);
                void Remove(TObject *object);

                TObject *FindObject(TObject *object);
                TObject *FindObject(TString  objName);

                void DeleteIfFound(TObject *object);
                void DeleteIfFound(TString  objName);

                static void EnableSignalHandler();
                static void DisableSignalHandler();

                static void HandlerCTRL_C(int);

                bool IsReady();
                
                void Print(Option_t *opt);
                int Listen();

                Long_t LastUpdate();
                Long_t Ticks();

        ClassDef(TObjectStreamer, 1);
};
