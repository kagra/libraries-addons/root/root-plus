/**
 * @file TClock.h
 * @brief Header of the TClock class
 * @author Marco Meyer <marco.meyer@cern.ch>
 * @date 2017-08-11
 * @version 1.0
 *
 * *********************************************
 *
 * @class TClock
 * @brief Description of the TClock class.
 * @details Detailed description of the TClock class, its functionality, and usage.
 */

#pragma once

#include "ROOT/IOPlus/Helpers.h"
#include <TMath.h>
#include <TH1.h>
#include <TExec.h>

#include <map>
#include <regex>
#include <variant>
#include <TString.h>

#include <TPrint.h>
#include <THLimitsFinder.h>

enum class Clock { TAI, GPS, Unix };
namespace ClockShortcuts {
    constexpr Clock TAI = Clock::TAI;
    constexpr Clock GPS = Clock::GPS;
    constexpr Clock Unix = Clock::Unix;
}

using namespace ClockShortcuts;

class TClockAbstract
{
       public :

              virtual ~TClockAbstract() { }
              
              virtual TString GetName() = 0;

              virtual TString Format(const char *format, double timestamp, const char *timezone = nullptr, int precision = 0) = 0;
              virtual TString Format(Double_t timestamp, const Option_t* option = "", int precision = 0) = 0;
              
              virtual TH1    *FormatTimeAxis(TH1 *h, const Option_t* option = "", const char *timezone = nullptr, int precision = 0, int axis = 0) = 0;

              virtual double Parse(const char *datetime, const char *format = nullptr) = 0;
              virtual inline double Now() { return this->Timestamp(); };

              virtual double Timestamp() = 0;
              virtual int GetLeapSeconds(double timestamp) = 0;
              virtual bool IsLeapSecond(double timestamp) = 0;
              virtual double ConvertTo(double timestamp, Clock clock) = 0;
};

template <Clock T1 = Clock::Unix>
class TClock : public TClockAbstract
{
       protected:

              // https://www.timeanddate.com/time/leapseconds.html
              // https://aviation.stackexchange.com/questions/90839/what-are-satellite-time-gps-time-and-utc-time
              static const int unixTimeGPS;
              static const int gpsOffsetTAI;
              static const std::vector<int> leapSeconds;
              static const std::map<TString, TString> timezoneOffsets;

       public:
              
              TString GetName() override;
              static TClockAbstract *Get();

              TString Format(const char *format, double timestamp, const char *timezone = nullptr, int precision = 0) override;
              TString Format(Double_t value, const Option_t* option = "", int precision = 0) override;

              static TString GuessUnit(double t0, double t1, int N);
              TH1        *FormatTimeAxis(TH1 *h,            const Option_t* option = "", const char *timezone = nullptr, int precision = 0, int axis = 0) override;
              static void FormatTimeAxis(const char* hname, const Option_t* option = "", const char *timezone = nullptr, int precision = 0, int axis = 0);

              inline TString UTC() { return UTC(Now()); }
                     TString UTC(const char *datetime, const char *format = nullptr, int precision = 0) { return Format(nullptr, Parse(datetime, format), "UTC", precision); }
                     TString UTC(int TimeS, int TimeN, int precision = 0) { return Format(nullptr, Timestamp(TimeS, TimeN), "UTC", precision); }
                     TString UTC(double timestamp, int precision = 0) { return Format(nullptr, timestamp, "UTC", precision); }

              inline TString Hour   (double timestamp, const char *timezone = nullptr) { return Format("%H", timestamp, timezone); }
              inline TString Minute (double timestamp, const char *timezone = nullptr) { return Format("%M", timestamp, timezone); }
              inline TString Second (double timestamp, const char *timezone = nullptr, int precision = 0) { return Format("%S", timestamp, timezone, precision); }

              inline TString Day  (double timestamp, const char *timezone = nullptr) { return Format("%d", timestamp, timezone); }
              inline TString Month(double timestamp, const char *timezone = nullptr) { return Format("%m", timestamp, timezone); }
              inline TString Year (double timestamp, const char *timezone = nullptr) { return Format("%Y", timestamp, timezone); }

              inline TString Date    (double timestamp, const char *timezone = nullptr) { return Format("%Y-%m-%d", timestamp, timezone); }
              inline TString DateTime(double timestamp, const char *timezone = nullptr, int precision = 0) { return Format("%Y-%m-%d %H:%M:%S", timestamp, timezone, precision); }
              inline TString Time    (double timestamp, const char *timezone = nullptr, int precision = 0) { return Format("%H:%M:%S", timestamp, timezone, precision); }
              inline TString Timezone(double timestamp, const char *timezone = nullptr) { return Format("%Z",       timestamp, timezone); }
              
              inline TString Local(const char *datetime, const char *format = nullptr, int precision = 0) { return Format(nullptr, Parse(datetime, format), LocalTz(), precision); }
              inline TString Local(int TimeS, int TimeN, int precision = 0) { return Format(nullptr, Timestamp(TimeS, TimeN), LocalTz(), precision); }
              inline TString Local(double timestamp = NAN, int precision = 0) { return Format(nullptr, TMath::IsNaN(timestamp) ? Now() : timestamp, LocalTz(), precision); }
                     TString LocalTz();

              double Parse(const char *datetime, const char *format = nullptr) override;
              double ParseTimeOffset(TString timeOffset); // +XX:YY.ZZZZ

              inline int TimeS(double timestamp) { return static_cast<int>(timestamp); }
              inline int TimeN(double timestamp) { return static_cast<int>(std::round(1e9 * (timestamp - int(timestamp)))); }
              int GMT();

              inline double Timestamp(int TimeS, int TimeN) { return TimeS + 1e-9*TimeN; }
                     double Timestamp(const char *datetime, const char *format = nullptr);
                     double Timestamp() override;

              static int GetLeapSecondsImpl(double timestamp);
                     int GetLeapSeconds(double timestamp) override { return TClock<T1>::GetLeapSecondsImpl(timestamp); };
              static bool IsLeapSecondImpl(double timestamp);
                     bool IsLeapSecond(double timestamp) override { return TClock<T1>::IsLeapSecondImpl(timestamp); }

              double ConvertTo(double timestamp, Clock targetClock) override {
                     switch (targetClock) {
                            case Clock::Unix:
                                   return ConvertTo<Clock::Unix>(timestamp);
                            case Clock::GPS:
                                   return ConvertTo<Clock::GPS>(timestamp);
                            case Clock::TAI:
                                   return ConvertTo<Clock::TAI>(timestamp);
                     default:
                            throw std::invalid_argument("Unknown target clock");
                     }
              }

              template<Clock T2>
              static double ConvertTo(double timestamp)
              {
                     using Clock1 = std::integral_constant<Clock, T1>;
                     using Clock2 = std::integral_constant<Clock, T2>;

                     using ClockGPS = std::integral_constant<Clock, Clock::GPS>;
                     using ClockTAI = std::integral_constant<Clock, Clock::TAI>;
                     using ClockUnix = std::integral_constant<Clock, Clock::Unix>;

                     // No conversion needed if clocks are the same
                     if (std::is_same<Clock1, Clock2>::value) return timestamp;
                     
                     // GPS to TAI and reverse
                     if (std::is_same<Clock1, ClockGPS>::value && std::is_same<Clock2, ClockTAI>::value) return timestamp + gpsOffsetTAI;
                     if (std::is_same<Clock1, ClockTAI>::value && std::is_same<Clock2, ClockGPS>::value) return timestamp - gpsOffsetTAI;

                     // Unix to ...
                     if (std::is_same<Clock1, ClockUnix>::value) {

                            if(std::is_same<Clock2, ClockGPS>::value)
                                   return timestamp - unixTimeGPS + TClock<T1>::GetLeapSecondsImpl(timestamp) + (TClock<T1>::IsLeapSecondImpl(timestamp) ? 0.5 : 0);              
                            if(std::is_same<Clock2, ClockTAI>::value)
                                   return TClock<GPS>::ConvertTo<T2>(TClock<T1>::ConvertTo<GPS>(timestamp));
                     }

                     // ... to Unix
                     if (std::is_same<Clock2, ClockUnix>::value) {

                            if(std::is_same<Clock1, ClockGPS>::value)
                                   return timestamp + unixTimeGPS - TClock<T1>::GetLeapSecondsImpl(timestamp) - (TClock<T1>::IsLeapSecondImpl(timestamp) ? timestamp-int(timestamp)-0.5 : 0);
                            if(std::is_same<Clock1, ClockTAI>::value)
                                   return TClock<GPS>::ConvertTo<T2>(TClock<T1>::ConvertTo<GPS>(timestamp));
                     }

                     throw std::invalid_argument("Unsupported clock conversion. Please implement.");
              }

       ClassDef(TClock,1);
};

R__EXTERN TClock<Clock::Unix> *gClock;
R__EXTERN TClock<Clock::Unix> *gClockUnix;
R__EXTERN TClock<Clock::GPS>  *gClockGPS;
R__EXTERN TClock<Clock::TAI>  *gClockTAI;