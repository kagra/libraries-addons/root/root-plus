#! /bin/bash

# This file is used by docker-compose.yml as container warmup.

[[ $_ != $0 ]] && REALPATH=`dirname $(readlink -f ${BASH_SOURCE[0]})` || REALPATH=`dirname $(readlink -f $0)`
cd $REALPATH

# Build conda environment
MOTD=0 make env
ln -sf /mnt/local/ $HOME

source .bashrc

echo "Container warmup complete. (read-only project directory: $PWD)"
exec "$@"