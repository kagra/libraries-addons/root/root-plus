#include <Rioplus.h>

#include <sstream>
#include <string>

#include <gtest/gtest.h>

TEST(IOManip, VectorDouble) {
    std::vector<double> vec = {1.23456, 2.34567, 3.45678, 4.56789, 5.67890, 6.78901, 7.89012};
    std::stringstream ss;
    int width = 10;
    int precision = 4;

    // Set custom precision and width
    ss << std::fixed << std::setprecision(precision) << std::setw(width);
    ss << vec;

    ASSERT_EQ(ss.str(), "[    1.2346,     2.3457,     3.4568,     4.5679,     5.6789,     6.7890,     7.8901]");
}

TEST(IOManip, LongVectorInt) {
    std::vector<double> vec = {0,1,2,3,4,5,6,7,8,9};
    std::stringstream ss;
    int width = 10;
    int precision = 4;

    // Set custom precision and width
    ss << std::fixed << std::setprecision(precision) << std::setw(width) << std::setlimit(5);
    ss << vec;

    ASSERT_EQ(ss.str(), "[    0.0000,     1.0000,     2.0000,        ...,     8.0000,     9.0000]");
}

TEST(IOManip, AlmostLongVectorInt) {
    std::vector<double> vec = {0,1,2,3,4,5,6,7,8,9};
    std::stringstream ss;
    int width = 10;
    int precision = 4;

    // Set custom precision and width
    ss << std::fixed << std::setprecision(precision) << std::setw(width) << std::setlimit(8);
    ss << vec;

    ASSERT_EQ(ss.str(), "[    0.0000,     1.0000,     2.0000,     3.0000,     4.0000,     5.0000,     6.0000,     7.0000,     8.0000,     9.0000]");
}

TEST(IOManip, VectorComplexDouble) {
    std::vector<std::complex<double>> complexVec = {{1.1, 2.2}, {3.3, 4.4}, {5.5, 6.6}};
    std::stringstream ss;
    int width = 10;
    int precision = 4;

    // Set custom precision and width
    ss << std::fixed << std::setprecision(precision) << std::setw(width);
    ss << complexVec;

    ASSERT_EQ(ss.str(), "[    1.1000+2.2000i,     3.3000+4.4000i,     5.5000+6.6000i]");
}

TEST(IOManip, VectorOfVectorsDouble) {
    std::vector<std::vector<double>> vecOfVecs = {{1.1, 2.2, 3.3}, {4.4, 5.5, 6.6}, {7.7, 8.8, 9.9}};
    std::stringstream ss;
    int width = 10;
    int precision = 4;
    int offset = 4;

    std::setoffset(offset);

    // Set custom precision and width
    ss << std::fixed << std::setprecision(precision) << std::setw(width);
    ss << vecOfVecs;

    ASSERT_EQ(ss.str(), "[[    1.1000,     2.2000,     3.3000],\n [    4.4000,     5.5000,     6.6000],\n [    7.7000,     8.8000,     9.9000]]");
}


TEST(IOManip, VectorOfComplexVectors) {
    std::vector<std::vector<std::complex<double>>> complexVecOfVecs = {
        {{1.1, 2.2}, {3.3, 4.4}}, 
        {{5.5, 6.6}, {7.7, 8.8}}
    };
    std::stringstream ss;
    int width = 10;
    int precision = 4;
    int offset = 4;

    std::setoffset(offset);

    // Set custom precision and width
    ss << std::fixed << std::setprecision(precision) << std::setw(width);
    ss << complexVecOfVecs;

    ASSERT_EQ(ss.str(), "[[    1.1000+2.2000i,     3.3000+4.4000i],\n [    5.5000+6.6000i,     7.7000+8.8000i]]");
}


TEST(IOManip, TVectorT) {
    TVectorT<double> tVector(5);
    for (int i = 0; i < tVector.GetNoElements(); ++i) {
        tVector[i] = i * 1.1;
    }
    std::stringstream ss;
    int width = 10;
    int precision = 4;

    // Set custom precision and width
    ss << std::fixed << std::setprecision(precision) << std::setw(width);
    ss << tVector;

    ASSERT_EQ(ss.str(), "[    0.0000,     1.1000,     2.2000,     3.3000,     4.4000]");
}

TEST(IOManip, TMatrixT) {
    TMatrixT<double> tMatrix(2, 3);
    int counter = 0;
    for (int i = 0; i < tMatrix.GetNrows(); ++i) {
        for (int j = 0; j < tMatrix.GetNcols(); ++j) {
            tMatrix[i][j] = counter * 1.1;
            ++counter;
        }
    }
    std::stringstream ss;
    int width = 10;
    int precision = 4;
    int offset = 4;

    std::setoffset(offset);

    // Set custom precision and width
    ss << std::fixed << std::setprecision(precision) << std::setw(width);
    ss << tMatrix;

    ASSERT_EQ(ss.str(), "[[    0.0000,     1.1000,     2.2000],\n [    3.3000,     4.4000,     5.5000]]");
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
