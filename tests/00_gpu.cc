
#include <torch/torch.h>
#include <iostream>
#include <complex>
#include <gtest/gtest.h>

TEST(CudaTest, GpuN)
{
    // GPU Test
    if (torch::cuda::is_available()) {

        int numGPUs = torch::cuda::device_count();
        if (numGPUs) {

            std::cout << "CUDA available. GPU count: " << numGPUs << "; CPU count: " << torch::get_num_threads() << std::endl;
            return;
        }
    }

    std::cout << "No CUDA available. GPU count: " << 0 << "; CPU count: " << torch::get_num_threads() << std::endl;
}

// Test fixture for basic tensor operations
TEST(CudaTest, TensorDeclaration) {
    
    // Integer tensors
    auto char_tensor = torch::empty({3, 3}, torch::kInt8);  // 32-bit integer tensor
    auto short_tensor = torch::empty({3, 3}, torch::kInt16);  // 32-bit integer tensor
    auto int_tensor = torch::empty({3, 3}, torch::kInt32);  // 32-bit integer tensor
    auto long_tensor = torch::empty({3, 3}, torch::kInt64); // 64-bit integer tensor

    // Floating point tensors
    auto float_tensor = torch::empty({3, 3}, torch::kFloat32);  // 32-bit float tensor
    auto double_tensor = torch::empty({3, 3}, torch::kFloat64); // 64-bit float tensor

    // Complex tensors
    auto complex_float_tensor = torch::empty({3, 3}, torch::kComplexFloat);  // Single-precision complex tensor
    auto complex_double_tensor = torch::empty({3, 3}, torch::kComplexDouble); // Double-precision complex tensor

    // Create a tensor (example initialization)
    torch::Tensor tensor = torch::randn({2, 3});

    // Access data pointer
    float* data_ptr = tensor.data_ptr<float>();

    // Get tensor sizes
    auto sizes = tensor.sizes();
    std::cout << "Tensor size: [" << sizes[0] << ", " << sizes[1] << "]" << std::endl;

    // Access and print tensor data
    std::cout << "Tensor data:" << std::endl;
    for (int i = 0; i < tensor.numel(); ++i) {
        std::cout << data_ptr[i] << " ";
    }
    std::cout << std::endl;
}

TEST(CudaTest, TensorStrides)
{
    auto tensor = torch::ones({3, 3}, torch::kFloat64);

    EXPECT_EQ(tensor.sizes(), std::vector<int64_t>({3,3}));
    EXPECT_EQ(tensor.strides(), std::vector<int64_t>({3,1}));
}

TEST(CudaTest, TensorDataAddr)
{
    torch::Tensor tensor = torch::ones({5}, torch::kComplexDouble);     // Char tensor
    
    // // Extract data pointer, shape, and stride
    std::vector<int64_t> shape(tensor.sizes().begin(), tensor.sizes().end());
    std::vector<std::complex<double>> data2(reinterpret_cast<std::complex<double>*>(((std::complex<double>*) tensor.data_ptr())),
                                            reinterpret_cast<std::complex<double>*>((((std::complex<double>*) tensor.data_ptr()) + tensor.numel())));

    // Print vector elements as a check
    // std::cout << "Vector elements:" << std::endl;
    // for (const auto& elem : data) {
    //     std::cout << elem << " ";
    // }
    // std::cout << std::endl;

    // // Construct TTensorT<Element> object
    // TTensorT<std::complex<double>> result(shape);
    std::complex<double>* data = (std::complex<double>*) tensor.data_ptr();
                          data[0] = std::complex<double>(0,1);

    std::cout << "Tensor Data Addr" << std::endl;
    std::cout << data[0] << std::endl;
    std::cout << data2[0] << std::endl;
}

TEST(CudaTest, TensorType) {
    
// Example tensor creation
    torch::Tensor float_tensor = torch::ones({5}, torch::kFloat32); // Float tensor
    torch::Tensor long_tensor = torch::ones({5}, torch::kInt64);    // Long tensor
    torch::Tensor char_tensor = torch::ones({5}, torch::kInt8);     // Char tensor
    torch::Tensor complex32_tensor = torch::ones({5}, torch::kComplexFloat);     // Char tensor
    torch::Tensor complex64_tensor = torch::ones({5}, torch::kComplexDouble);     // Char tensor
    
    complex64_tensor += complex64_tensor;
    // Accessing data pointers
    auto float_ptr = (float*) float_tensor.data_ptr();
    auto long_ptr = (long*) long_tensor.data_ptr();
    auto char_ptr = (char*) char_tensor.data_ptr();
    auto complex32_ptr = (std::complex<float>*) complex32_tensor.data_ptr();
    auto complex64_ptr = (std::complex<double>*) complex64_tensor.data_ptr();

    // Printing tensor data
    std::cout << "Float Tensor:" << std::endl;
    for (int i = 0; i < float_tensor.numel(); ++i) {
        std::cout << float_ptr[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "Long Tensor:" << std::endl;
    for (int i = 0; i < long_tensor.numel(); ++i) {
        std::cout << long_ptr[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "Char Tensor:" << std::endl;
    for (int i = 0; i < char_tensor.numel(); ++i) {
        std::cout << static_cast<int>(char_ptr[i]) << " "; // Cast to int for proper printing of char values
    }
    std::cout << std::endl;

    std::cout << "Complex32 Tensor:" << std::endl;
    for (int i = 0; i < complex32_tensor.numel(); ++i) {
        std::cout << complex32_ptr[i] << " "; // Cast to int for proper printing of char values
    }
    std::cout << std::endl;

    std::cout << "Complex64 Tensor:" << std::endl;
    for (int i = 0; i < complex64_tensor.numel(); ++i) {
        std::cout << complex64_ptr[i] << " "; // Cast to int for proper printing of char values
    }
    std::cout << std::endl;

}

TEST(CudaTest, TensorBlob) {
    // Example data buffer (float values)
    std::vector<float> data = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
    std::vector<int64_t> shape({2, 3});
    std::vector<int64_t> strides({3, 1});

    // Specify tensor sizes and strides
    at::IntArrayRef shapeRef(shape);
    at::IntArrayRef strideRef{strides};  // Row-major order (for contiguous data)

    // Create tensor from the data buffer
    auto tensor = torch::from_blob(&data[0], shapeRef, strideRef, at::kFloat);

    // Print the created tensor
    std::cout << "Tensor created:\n" << tensor << std::endl;
}

std::vector<int64_t> GetStrides(std::vector<int64_t> shape)
{
    std::vector<int64_t> strides(shape.size());
    if (shape.empty()) {
        return strides;
    }

    strides.back() = 1; // The last dimension has stride 1
    for (int i = shape.size() - 2; i >= 0; --i) {
        strides[i] = strides[i + 1] * shape[i + 1];
    }
    return strides;
}

TEST(CudaTest, TensorSubtraction)
{
    // Example data buffer (float values)
    std::vector<float> data1 = {1.0, 2.0, 3.0, 4.0, 5.0, 6};
    std::vector<float> data2 = {6.0, 5.0, 4.0, 3.0, 5.0, 6};

    // Shape and strides for the tensors
    std::vector<int64_t> shape({2, 2});

    // Specify tensor sizes and strides
    at::IntArrayRef shapeRef(shape);

    // Create tensors from the data buffers
    auto tensor1 = torch::from_blob(data1.data(), shapeRef, torch::kFloat);
    auto tensor2 = torch::from_blob(data2.data(), shapeRef, torch::kFloat);

    // Print the created tensors
    std::cout << "Tensor 1:\n" << tensor1 << std::endl;
    std::cout << "Tensor 2:\n" << tensor2 << std::endl;

    // Subtract the two tensors
    auto result = tensor1 - tensor2;

    // Print the result tensor
    std::cout << "Result of subtraction (Tensor 1 - Tensor 2):\n" << result << std::endl;
    // EXPECT_EQ(1,0);
}

int main(int argc,char **argv) {

    // Initialize Google Test framework
    ::testing::InitGoogleTest(&argc,argv);

    // Run all tests
    return RUN_ALL_TESTS();
}