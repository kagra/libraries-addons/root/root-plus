#include <TClock.h>
#include <gtest/gtest.h>

#include "date/tz.h"
#include <iostream>
#include <vector>

#include <unordered_map>
#include <string>

TEST(Clock, ParseTimeOffset)
{
    // Define the time offset strings
    std::string offset_strs[] = {"+03:00:15.123456", "+03:00", "+030015", "+0300", "+03"};
    double result_strs[] = {10815.123456, 10800, 10815, 10800, 10800};

    for (int i = 0; i < 5; i ++ ) {

        std::string offset_str = offset_strs[i];
        double result_str = result_strs[i];
        
        try {

            // Parse the time offset string
            double offset_seconds = gClock->ParseTimeOffset(offset_str);

            // Output the parsed offset in seconds
            ASSERT_EQ(offset_seconds, result_str);

        } catch (const std::invalid_argument& e) {
            std::cerr << "Error: " << e.what() << std::endl;
        }
    }
}

TEST(Clock, ParseAndFormat) 
{
    TString datetime = "2022-04-01 11:17:00 CST";
    
    double microtimestamp = gClock->Parse(datetime);
    ASSERT_EQ(microtimestamp, 1648833420.000000);

    ASSERT_EQ(gClock->Format(nullptr, microtimestamp, "GMT+1", 6), "2022-04-01 18:17:00.000000 GMT+1");
    ASSERT_EQ(gClock->Format(nullptr, microtimestamp, "GMT+2", 6), "2022-04-01 19:17:00.000000 GMT+2");
    ASSERT_EQ(gClock->Format(nullptr, microtimestamp, "UTC", 6), "2022-04-01 17:17:00.000000 UTC");
    ASSERT_EQ(gClock->Format(nullptr, microtimestamp, "JST", 6), "2022-04-02 02:17:00.000000 JST");

    TString time = gClock->Time(microtimestamp, "CST", 6);
    TString date = gClock->Date(microtimestamp, "CST");
    TString zone = gClock->Timezone(microtimestamp, "CST");
    ASSERT_EQ(date+" "+time+" "+zone, "2022-04-01 11:17:00.000000 CST");
}

TEST(Clock, GPS) 
{
    ASSERT_EQ(gClockGPS->Parse("2022-04-01 11:17:00 CDT"), 1332865038);
}

TEST(Clock, TAI) 
{
    ASSERT_EQ(gClockTAI->Parse("2022-04-01 11:17:00 CDT"), 1332865057);
}

int main(int argc,char **argv) {

    // Initialize Google Test framework
    ::testing::InitGoogleTest(&argc,argv);

    // Run all tests
    return RUN_ALL_TESTS();
}
