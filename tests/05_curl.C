#include "gtest/gtest.h"
#include "TCurl.h"

// Test for GET method with caching
TEST(TCurlFileTest, GetWithCache) {

    TCurlFile *curlFile = new TCurlFile("_fetch_test.html", "RECREATE", 3600, "default_pool");
               curlFile->Print();
    
    // Make first request
    const char *url = "http://www.example.com";
    curlFile->Evict(url);
    
    // First fetch should not hit the cache
    ASSERT_FALSE(curlFile->Hit());

    // Second fetch should hit the cache
    ASSERT_TRUE(curlFile->Fetch(url));
    ASSERT_TRUE(curlFile->Hit());
}

// // Test for POST method
// TEST_F(TCurlFileTest, PostRequest) {
//     const char *url = "http://www.example.com/post";
//     const char *postData = "name=John&age=30";

//     // POST request
//     ASSERT_FALSE(curlFile->Hit(url));
//     ASSERT_TRUE(curlFile->Send(url, TCurlRequest::POST, postData));
//     ASSERT_TRUE(curlFile->Hit(url));
// }

// // Test for PUT method
// TEST_F(TCurlFileTest, PutRequest) {
//     const char *url = "http://www.example.com/put";
//     const char *putData = "name=John&age=31";

//     // PUT request
//     ASSERT_FALSE(curlFile->Hit(url));
//     ASSERT_TRUE(curlFile->Send(url, TCurlRequest::PUT, putData));
//     ASSERT_TRUE(curlFile->Hit(url));
// }

// // Test for DELETE method
// TEST_F(TCurlFileTest, DeleteRequest) {
//     const char *url = "http://www.example.com/delete";

//     // DELETE request
//     ASSERT_FALSE(curlFile->Hit(url));
//     ASSERT_TRUE(curlFile->Send(url, TCurlRequest::DELETE));
//     ASSERT_TRUE(curlFile->Hit(url));
// }

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
