
#include <gtest/gtest.h>
#include <Rioplus.h>

using namespace std;

//
// Define test fixture with a type parameter T
template <typename T>
class NumberTOperatorsTest : public ::testing::Test {
protected:

    // Test variables
    NumberT<T> a;
    NumberT<T> b;
    std::complex<T> c;
    T d;

    void SetUp() override {
        // Initialize test data if needed
        a = T(5);
        std::cout << a << std::endl;
        b = std::complex<T>(10,5);
        c = std::complex<T>(3, 1);
        d = T(0); // Initialize d as T
    }
};

// Define the types of tests to run (e.g., int and double)
typedef ::testing::Types<int, float, double> MyTypes;
TYPED_TEST_SUITE(NumberTOperatorsTest, MyTypes);

// Test cases
TYPED_TEST(NumberTOperatorsTest, OperatorPlusT) {
    NumberT<TypeParam> result = this->a + TypeParam(3);
    EXPECT_EQ(std::get<TypeParam>(result), TypeParam(8));
}

TYPED_TEST(NumberTOperatorsTest, OperatorPlusComplex) {
    NumberT<TypeParam> result = this->a + this->c;
    EXPECT_TRUE(std::get_if<std::complex<TypeParam>>(&result) != nullptr);
    EXPECT_EQ(std::get<std::complex<TypeParam>>(result), std::complex<TypeParam>(8, 1));
}

TYPED_TEST(NumberTOperatorsTest, OperatorPlusCommutative) {
    NumberT<TypeParam> result = this->c + this->b;
    EXPECT_TRUE(std::get_if<std::complex<TypeParam>>(&result) != nullptr);
    EXPECT_EQ(std::get<std::complex<TypeParam>>(result), std::complex<TypeParam>(13, 6));
}

TYPED_TEST(NumberTOperatorsTest, OperatorPlusNumberT) {
    NumberT<TypeParam> result = this->a + this->b;
    EXPECT_TRUE(std::get_if<std::complex<TypeParam>>(&result) != nullptr);
    EXPECT_EQ(std::get<std::complex<TypeParam>>(result), std::complex<TypeParam>(15, 5));
}

TYPED_TEST(NumberTOperatorsTest, OperatorPlusEqualT) {
    this->d += TypeParam(2);
    EXPECT_EQ(this->d, TypeParam(2));
}

TYPED_TEST(NumberTOperatorsTest, OperatorPlusEqualComplex) {
    this->b += this->c;    
    EXPECT_EQ(std::get<std::complex<TypeParam>>(this->b), std::complex<TypeParam>(13, 6));
}

TYPED_TEST(NumberTOperatorsTest, OperatorPlusEqualCommutative) {
    this->c += this->b;
    EXPECT_EQ(this->c, std::complex<TypeParam>(3, 1));
}

TYPED_TEST(NumberTOperatorsTest, OperatorPlusEqualNumberT) {
    this->a += this->b;
    EXPECT_EQ(std::get<std::complex<TypeParam>>(this->a), std::complex<TypeParam>(15, 5));
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
