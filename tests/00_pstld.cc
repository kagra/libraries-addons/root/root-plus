#include <Rioplus.h>
#include <ROOT/IOPlus/std/execution.h>

const size_t N = 100000; // Size of the vector
// const size_t N = 1000000; // Size of the vector
const int iterations = 100;

// Precise timing information, might be not so accurate on ARM architecture
template<typename Func>
void chrono(Func lambda) {
    auto total_time = std::chrono::duration<double>::zero();
    auto best_time = std::chrono::duration<double>::max();
    auto worst_time = std::chrono::duration<double>::min();

    for (int i = 0; i < iterations; ++i) {
        auto start = std::chrono::high_resolution_clock::now();
        lambda();

        auto end = std::chrono::high_resolution_clock::now();
        auto elapsed = end - start;
        total_time += elapsed;

        if (elapsed < best_time) best_time = elapsed;
        if (elapsed > worst_time) worst_time = elapsed;
    }

    std::cout << "<t> = " << total_time.count() / 100.0 << "s; ";
    std::cout << "tmin = " << best_time.count() << "s; ";
    std::cout << "tmax = " << worst_time.count() << "s" << std::flush;
}

int main()
{
    std::vector<int> vec(N);
    std::vector<int> result(N);

    // Fill the vector with random values
    std::generate(vec.begin(), vec.end(), []() { return rand() % 10000; });

    // Perform each sorting operation 100 times and calculate average time
    std::cout << "Running benchmark over " << iterations << " iteration(s) with vector size of " << N << std::fixed << std::endl;

    std::cout << "(std::sort)" << std::endl;
    chrono([&vec] { std::sort(std::execution::seq, vec.begin(), vec.end()); });
    std::cout << "\t(sequential processing)" << std::endl;
    chrono([&vec] { std::sort(std::execution::par, vec.begin(), vec.end()); });
    std::cout << "\t(parallel processing)" << std::endl;
    chrono([&vec] { std::sort(std::execution::par_unseq, vec.begin(), vec.end()); });
    std::cout << "\t(parallel processing + SIMD instructions)" << std::endl;

    std::cout << "(std::reduce)" << std::endl;
    chrono([&vec] { std::reduce(std::execution::seq, vec.begin(), vec.end()); });
    std::cout << "\t(sequential processing)" << std::endl;
    chrono([&vec] { std::reduce(std::execution::par, vec.begin(), vec.end()); });
    std::cout << "\t(parallel processing)" << std::endl;
    chrono([&vec] { std::reduce(std::execution::par_unseq, vec.begin(), vec.end()); });
    std::cout << "\t(parallel processing + SIMD instructions)" << std::endl;

    std::cout << "(std::transform)" << std::endl;
    chrono([&vec, &result] { std::transform(std::execution::seq, vec.begin(), vec.end(), result.begin(), [](int x) { return pow(x, 2); }); });
    std::cout << "\t(sequential processing)" << std::endl;
    chrono([&vec, &result] { std::transform(std::execution::par, vec.begin(), vec.end(), result.begin(), [](int x) { return pow(x, 2); }); });
    std::cout << "\t(parallel processing)" << std::endl;
    chrono([&vec, &result] { std::transform(std::execution::par_unseq, vec.begin(), vec.end(), result.begin(), [](int x) { return pow(x, 2); }); });
    std::cout << "\t(parallel processing + SIMD instructions)" << std::endl;

    return 0;
}
